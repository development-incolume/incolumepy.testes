def tabuada_multiplicar(num):
    for i in range(1, 11):
        print("%2.f x %2.f = %2.f" % (num, i, num * i))
    print()


def tabuada_somar(num):
    for i in range(1, 11):
        print("{} + {:2} = {:2}".format(num, i, num * i))
    print()


if __name__ == '__main__':
    tabuada_multiplicar(6)
    tabuada_somar(5)
