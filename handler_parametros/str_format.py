import collections

Person = collections.namedtuple('Person', 'first last')


class Pessoa():
    def __init__(self, first, last, x=10):
        self.first = first
        self.last = last
        self.x = x

    def __str__(self):
        return '{a[first]} {a[last]}'.format(a=self.__dict__)

    def __repr__(self):
        return '{a[last]}, {a[first]}'.format(a=self.__dict__)

    def ostr(self):
        return '{first} {last}'.format(**self.__dict__)


def my_getitem_getattr():
    p = Person('Ricardo', 'Brito')
    r = Pessoa('Ricardo', 'Brito')

    # print(p.__dict__)
    print(r.__dict__)
    print('{a[first]} {a[last]}'.format(a=r.__dict__))
    print('{0!s}'.format(r))
    print('{0!r}'.format(r))
    print(r.ostr())


if __name__ == '__main__':
    my_getitem_getattr()
