def paramObrig(x=int, y=int):
    assert isinstance(x, int)
    assert isinstance(y, int)
    return x, y


def paramObrig01(x=int, y=int):
    if not (isinstance(x, int) and isinstance(y, int)):
        raise AssertionError('Parametros devem ser numeros inteiros')
    return x, y


def paramObrig02(x=int, y=int):
    if not (isinstance(x, int) and isinstance(y, int)):
        raise TypeError('Parametros devem ser numeros inteiros')
    return x, y


def paramObrig03(x, y):
    x, y = int(x), int(y)
    return x, y


if __name__ == '__main__':
    print(paramObrig(1, 2))
    try:
        print(paramObrig('1', '2'))
    except Exception as e:
        print('assert: não replica msg erro')

    print(paramObrig01(1, 2))
    try:
        print(paramObrig01('1', '2'))
    except AssertionError as e:
        print(e)

    print(paramObrig02(1, 2))
    try:
        print(paramObrig02(1, '2'))
    except TypeError as e:
        print(e)

    print(paramObrig03(1, 2))
    print(paramObrig03(1, '2'))
    try:
        print(paramObrig03(1, 'a'))
    except ValueError as e:
        print(e)
