names = ['ada', 'ana', 'eliana', 'ricardo']
cores = ['azul', 'branco', 'vermelho', 'verde', 'amarelo']

for i in range(min(len(names), len(cores))):
    print('{} --> {}'.format(names[i], cores[i]))

print()
for i in zip(names, cores):
    print(i)

print()
for nome, cor in zip(names, cores):
    print('{} --> {}'.format(nome, cor))

print()
for i in zip(names, cores):
    print('{} --> {}'.format(*i))
