echo "################################################################"
echo ""
echo " --------------------------- FRONTEND ------------------------- "
echo ""

echo ""
date
echo ""

echo "conexoes estabelecidas"
netstat -tupan | grep ESTABLISHED | wc -l
echo ""

echo "################################################################"
echo "conexoes em espera"
netstat -tupan | grep TIME_WAIT | wc -l
echo ""

echo "################################################################"
echo "load do server"
cat /proc/loadavg
echo ""

echo "################################################################"
echo "uso de memoria"
free -m -t

