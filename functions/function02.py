def l_ascii2char():
    return [(i, chr(i)) for i in range(257)]

if __name__ == '__main__':
    print(l_ascii2char())
    print(l_ascii2char()[165])