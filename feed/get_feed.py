#!/usr/bin/python
# -*- coding: utf-8 -*-

import codecs

import feedparser

''' \
http://stackoverflow.com/questions/38346619/how-to-handle-utf-8-text-with-python-3
'''


def jp():
    url = "http://feeds.bbci.co.uk/japanese/rss.xml"
    feeds = feedparser.parse(url)
    title = feeds['feed'].get('title')

    print(title)

    # file = codecs.open(filename='title.txt', mode='w', encoding='utf-8')
    file = codecs.open("test.txt", "w", "utf-8")
    file.write(title)
    file.close()


def ch():
    url = 'newsrss.bbc.co.uk/rss/chinese/simp/news/rss.xml'
    feeds = feedparser.parse(url)
    title = feeds['feed'].get('title')

    print(title)

    with codecs.open('test.txt', 'a', 'utf-8') as file:
        file.write(title)

def my():
    url = 'http://brito.blog.incolume.com.br/feeds/posts/default'
    feeds = feedparser.parse(url)
    # print(feeds)
    title = feeds['feed'].get('title')
    print(title)

    with codecs.open(filename='myblog.txt', mode='w', encoding='utf-8') as file:
        file.write(title)


def title_feed(url, filename, mode='a', encoding='utf-8'):
    feeds = feedparser.parse(url)
    # print(feeds)
    title = feeds['feed'].get('title')
    print(title)
    data = "%s: %s\n" % (title, url)
    with codecs.open(filename, mode, encoding) as file:
        file.write(data)


def test():
    jp()
    my()


def main():
    # test()
    title_feed("http://feeds.bbci.co.uk/japanese/rss.xml", 'feeds.txt', 'w')
    title_feed('http://feeds.bbci.co.uk/zhongwen/simp/rss.xml', 'feeds.txt')
    title_feed('http://brito.blog.incolume.com.br/feeds/posts/default', 'feeds.txt')

if __name__ == '__main__':
    pass
    main()
