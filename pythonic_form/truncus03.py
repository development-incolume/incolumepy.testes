import sys
from contextlib import contextmanager

fname = __file__.replace('.py', '.txt')

with open(fname, 'w') as file:
    oldstout = sys.stdout
    sys.stdout = file
    try:
        help(pow)
    finally:
        sys.stdout = oldstout


@contextmanager
def redirect_stdout(file):
    oldstout = sys.stdout
    sys.stdout = file
    try:
        yield file
    finally:
        sys.stdout = oldstout

with open(fname, 'a') as file:
    with redirect_stdout(file):
        help(help)

