d = {'ricardo': 'blue', 'ana': 'yellow', 'ada': 'green', 'eliana': 'purple'}

for k in d:
    print(k)

for k in d.keys():
    print(k)

for k in d.keys():
    if k.startswith('r'):
        print(d[k])



