from functools import wraps
from functools import lru_cache


# Sem cache
def fibo(n):
    if n < 2:
        return n
    return fibo(n - 1) + fibo(n - 2)


# Cache personalizado via decorador
def memoize(func):
    memo = {}

    @wraps(func)
    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            rv = func(*args)
            memo[args] = rv
            return rv
    return wrapper


@memoize
def fibonacci(n):
    if n < 2:
        return n
    return fibonacci(n - 1) + fibonacci(n - 2)


@lru_cache(maxsize=32)
def fib(n):
    if n < 2:
        return n
    return fib(n-1) + fib(n-2)

if __name__ == '__main__':
    print([fib(n) for n in range(10)])
    # Output: [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]
    print([fibonacci(n) for n in range(10)])
    print([fibo(n) for n in range(10)])
    print(fib.cache_clear())  # limpar cache
