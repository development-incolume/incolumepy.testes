import os
from contextlib import contextmanager, suppress

# not pythonic
try:
    os.remove('teste.txt')
except OSError as e:
    # print(e)
    pass

# pythonic
@contextmanager
def ignored(*exceptions):
    try:
        yield
    except exceptions as e:
        # print(e)
        pass

with ignored(OSError):
    os.remove('teste.txt')

# pythonic com modulos
with suppress(OSError):
    os.remove('somefile.tmp')

with suppress(FileNotFoundError):
    os.remove('someotherfile.tmp')


