d = {'ricardo': 'blue', 'ana': 'yellow', 'ada': 'green', 'eliana': 'purple'}

cores = [x for x in d.values()] * 3
cores.pop(0)
cores.pop(-4)
cores.pop()

print(cores)

#not pythonic
result = {}
for color in cores:
    if color not in result:
        result[color] = 0
    result[color] += 1

print(result)

# pythonic
result = {}
for cor in cores:
    result[cor] = result.get(cor, 0) +1
print(result)


print(result.get('gray', 0))
print(result.get('blue', 0))