def find0(seq, target):
    found = None
    for i, value in enumerate(seq):
        if value == target:
            found = True
            break
    if not found:
        return -1
    return 1


def find(seq, target):
    '''
    Pythonic
    :param seq:
    :param target:
    :return:
    '''

    for i, value in enumerate(seq):
        if value == target:
            break
    else:
        return -1
    return 1


def ofind0(seq, target):
    for i in seq:
        if i == target:
            break
    else:
        return False
    return True


def ofind1(seq, target):
    if isinstance(seq, dict):
        for k, v in seq.items():
            if k == target or v == target:
                break
        else:
            return False
    else:
        for i in seq:
            if i == target:
                break
        else:
            return False
    return True


def ofind(seq, target):
    if isinstance(seq, dict):
        for k, v in seq.items():
            if k == target or v == target:
                break
        else:
            return False
    else:
        for i in seq:
            if i == target:
                break
        else:
            return False
    return True

d = {1: 'um', 2: 'dois', 3: 'três', 4: 'quatro', 5: 'cinco', 9: 'nove'}
l = [1, 2, 3, 6, 9]
t1 = tuple(x for x in d)
t2 = tuple(x for x in d.values())

print(find0(l, 2))
print(find0(l, 7))

print(find(l, 0))
print(find(l, 9))
print(ofind(d, 9))
print(ofind(d, 'nove'))
print(ofind(l, 9))
print(ofind(l, 7))
print(ofind(l, 'cinco'))
print(t1, t2)
print(ofind(t1, 5))
print(ofind(t2, 'cinco'))
