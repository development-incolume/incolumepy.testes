d = {'ricardo': 'blue', 'ana': 'yellow', 'ada': 'green', 'eliana': 'purple'}

e = {k: d[k] for k in d if not k.startswith('r')}

print(d)
print(e)

#no pythonic
for k in d:
    print(k, '-->', d[k])


print('\n#pythonic')
for k, v in d.items():
    print('{} --> {}'.format(k,v))

