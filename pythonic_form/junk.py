d = {1: 'um', 2: 'dois', 3: 'três', 4: 'quatro', 5: 'cinco', 9: 'nove'}
t = tuple(d)
l = [x for x in d.values()]
m = [x for x in d]

lt = [(x, y) for x, y in d.items()]
ltr = [(y, x) for x, y in d.items()]
dr = {y: x for x, y in d.items()}

for i in range(3):
    print(i)
else:
    print('a')


print(d)
print(t)
print(l)
print(m)
print(lt)
print(ltr)
print(dr)
