names = ['ana', 'ada', 'raymond', 'raquel', 'eliana', 'leni', 'ricardo']

d = {}
for name in names:
    key = len(name)
    if key not in d:
        d[key] = []
    d[key].append(name)
print(d)

e={}
for name in names:
    key = len(name)
    e.setdefault(key,[]).append(name)
print(e)

from collections import defaultdict
f = defaultdict(list)
for name in names:
    f[len(name)].append(name)
print(dict(f))