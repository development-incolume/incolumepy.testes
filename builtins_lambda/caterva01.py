import operator
from string import ascii_lowercase


def somar0(x, y):
    return x + y


somar1 = (lambda x, y: x + y)

print(somar0(1, 2))
print(somar1(1, 2))


# 3

def soma(*args):
    return sum(args)


soma1 = lambda *args: sum(args)
print()
print(soma(1, 2, 3, 4, 5, 6, 7, 8, 9))
print(soma1(1, 2, 3, 4, 5, 6, 7, 8, 9))
# 45

print()
d = [(y, x) for x, y in enumerate(reversed(ascii_lowercase), 1)]
print(d)
print(sorted(d, key=lambda x: x[0]))
# [('z', 1), ('y', 2), ('x', 3), ('w', 4), ('v', 5), ('u', 6), ('t', 7), ('s', 8), ('r', 9), ('q', 10), ('p', 11), ('o', 12), ('n', 13), ('m', 14), ('l', 15), ('k', 16), ('j', 17), ('i', 18), ('h', 19), ('g', 20), ('f', 21), ('e', 22), ('d', 23), ('c', 24), ('b', 25), ('a', 26)]
# [('a', 26), ('b', 25), ('c', 24), ('d', 23), ('e', 22), ('f', 21), ('g', 20), ('h', 19), ('i', 18), ('j', 17), ('k', 16), ('l', 15), ('m', 14), ('n', 13), ('o', 12), ('p', 11), ('q', 10), ('r', 9), ('s', 8), ('t', 7), ('u', 6), ('v', 5), ('w', 4), ('x', 3), ('y', 2), ('z', 1)]

print()
e = {y: x for x, y in enumerate(reversed(ascii_lowercase), 1)}
print(e)
print(sorted(d, key=lambda x: x[0]))
print(sorted(d, key=lambda x: x[1]))
# {'m': 14, 's': 8, 't': 7, 'e': 22, 'o': 12, 'q': 10, 'l': 15, 'd': 23, 'v': 5, 'r': 9, 'i': 18, 'w': 4, 'h': 19, 'f': 21, 'j': 17, 'c': 24, 'y': 2, 'u': 6, 'p': 11, 'k': 16, 'a': 26, 'g': 20, 'n': 13, 'z': 1, 'x': 3, 'b': 25}
# [('a', 26), ('b', 25), ('c', 24), ('d', 23), ('e', 22), ('f', 21), ('g', 20), ('h', 19), ('i', 18), ('j', 17), ('k', 16), ('l', 15), ('m', 14), ('n', 13), ('o', 12), ('p', 11), ('q', 10), ('r', 9), ('s', 8), ('t', 7), ('u', 6), ('v', 5), ('w', 4), ('x', 3), ('y', 2), ('z', 1)]
# [('z', 1), ('y', 2), ('x', 3), ('w', 4), ('v', 5), ('u', 6), ('t', 7), ('s', 8), ('r', 9), ('q', 10), ('p', 11), ('o', 12), ('n', 13), ('m', 14), ('l', 15), ('k', 16), ('j', 17), ('i', 18), ('h', 19), ('g', 20), ('f', 21), ('e', 22), ('d', 23), ('c', 24), ('b', 25), ('a', 26)]


print('\n operator no lugar de lambda')
x = {y: x for x, y in enumerate(reversed(ascii_lowercase), 1)}

print(x)
print(sorted(x.items(), key=operator.itemgetter(1)))
print(sorted(x.items(), key=operator.itemgetter(0)))

print('\nmax')
m = lambda x, y: x if x > y else y
print(m(3, 5))
