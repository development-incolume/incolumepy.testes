Date:   Sun Aug 23 18:06:23 2015 -0300
    Excluir temporários do pacote final
Date:   Sun Aug 23 15:46:52 2015 -0300
    primeiro pacote hospedado em https://testpypi.python.org/pypi
Date:   Sun Aug 23 15:04:45 2015 -0300
    aplicado o wheel universal para Python 2 e 3
Date:   Sun Aug 23 15:03:09 2015 -0300
    Na documentação https://packaging.python.org/en/latest/distributing.html, para pacotes wheel, twine é requisito necessário
Date:   Sun Aug 23 15:01:26 2015 -0300
    python setup.py bdist_wheel --universal; configuração para wheel universal definida no setup.cfg
Date:   Sun Aug 23 12:55:52 2015 -0300
    Variável PACKAGE em desuso
Date:   Sun Aug 23 12:54:15 2015 -0300
    retiradas as variáveis NAMESPACE e SUBPACKAGE, substituído por solução Pythônica
Date:   Sun Aug 23 12:35:20 2015 -0300
    captura do namespace substituido pelo split()
Date:   Sun Aug 23 10:18:52 2015 -0300
    adicionado metodo read(*rnames)
Date:   Sun Aug 23 09:54:56 2015 -0300
    tratamento de InportError para namespace
Date:   Sun Aug 23 09:34:57 2015 -0300
    alteração em alguns procedimentos
Date:   Sat Aug 22 23:10:17 2015 -0300
    alteração em alguns procedimentos
Date:   Sat Aug 22 18:08:56 2015 -0300
    corrigido bug de instalação para pacote.tar.gz
Date:   Sat Aug 22 17:51:14 2015 -0300
    update version
Date:   Sat Aug 22 17:48:06 2015 -0300
    incluir docsi/
Date:   Sat Aug 22 17:41:41 2015 -0300
    incremento version
Date:   Sat Aug 22 13:50:11 2015 -0300
    update setup.py
Date:   Sat Aug 22 12:58:58 2015 -0300
    gera apenas os pacotes .egg e .whl
Date:   Sat Aug 22 12:57:15 2015 -0300
    update version
Date:   Sat Aug 22 12:38:13 2015 -0300
    O pytest deverá ser instalado com o pip"
Date:   Sat Aug 22 12:35:55 2015 -0300
    Alteração da forma de capturar o NameSpace do pacote
Date:   Sat Aug 22 12:35:10 2015 -0300
    Arquivos desnecessário
Date:   Sat Aug 22 12:30:14 2015 -0300
    ajust setup.py
Date:   Sat Aug 22 12:09:48 2015 -0300
    Dependência para ambiente Python
Date:   Sat Aug 22 12:01:40 2015 -0300
    gitignore update
Date:   Sat Aug 22 11:53:03 2015 -0300
    update
Date:   Sat Aug 22 11:45:43 2015 -0300
    gitignore update
Date:   Sat Aug 22 11:26:04 2015 -0300
    pytest adicionado
Date:   Sat Aug 22 11:19:11 2015 -0300
    tentativa de remover os arquivos .py dos pacotes de distribuição
Date:   Sat Aug 22 11:08:14 2015 -0300
    update gitignore
Date:   Sat Aug 22 11:07:01 2015 -0300
    tag_build e script de execução
Date:   Sat Aug 22 10:18:58 2015 -0300
    init do incolumepy.teste
Date:   Sat Aug 22 10:17:24 2015 -0300
    declaração de namespace para leitura correta do pacote
Date:   Sat Aug 22 09:54:06 2015 -0300
    Contribuidores e mudanças acrescentados
Date:   Sat Aug 22 09:39:15 2015 -0300
    update manifest
Date:   Sat Aug 22 09:39:01 2015 -0300
    epoch()
Date:   Sat Aug 22 09:33:04 2015 -0300
    add manifest.in
Date:   Sat Aug 22 09:30:35 2015 -0300
    gitignore
Date:   Sat Aug 22 09:29:43 2015 -0300
    versão zero do teste
Date:   Sat Aug 22 12:04:40 2015 +0000
    reademe
