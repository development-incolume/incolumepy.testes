import csv
from collections import defaultdict

class Listas():
    '''
    Sort defaultdict
    '''
    csv_file = '../../../data/cidades_brasil.csv'

    def uso020(self):
        '''
        Ordenaçao pelo Estado com mais cidades
        :return:
        '''
        cities_by_state = defaultdict(list)

        # transfoma CSV em tupla
        with open(self.csv_file) as filein:
            cidades = [tuple(line) for line in csv.reader(filein)]

        for city in sorted(cidades):
            print(city)

        pass


if __name__ == '__main__':
    Listas().uso020()
