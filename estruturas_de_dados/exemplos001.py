import csv
from collections import defaultdict

class Listas():
    '''
    Sort defaultdict
    '''
    csv_file = '../../../data/cidades_brasil.csv'
    def uso020(self):
        '''
        Ordenaçao pelo Estado com mais cidades
        :return:
        '''
        cities_by_state = defaultdict(int)

        # transfoma CSV em tupla
        with open(self.csv_file) as filein:
            cidades = [tuple(line) for line in csv.reader(filein)]

        # conta as cidades de cada estado
        for city, state in cidades:
            cities_by_state[state] += 1

        # ordena os estados pela quantidade de cidades
        for state, i in sorted(cities_by_state.items(), key = lambda x: (x[1], x[0]), reverse=True):
            print(state, i)

        pass


if __name__ == '__main__':
    Listas().uso020()
