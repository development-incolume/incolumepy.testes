class Fila:
    _fila = []

    @property
    def fila(self):
        try:
            return self._fila.pop(0)
        except:
            return None

    @fila.setter
    def fila(self, x):
        self._fila.append(x)

    def __str__(self):
        return str(self._fila)


if __name__ == '__main__':
    f = Fila()
    print(f.fila)
    f.fila = 1
    f.fila = 23
    f.fila = 50
    f.fila = 10
    print(f.fila, f)
    print(f.fila, f)
    f.fila = 11
    print(f.fila, f)
