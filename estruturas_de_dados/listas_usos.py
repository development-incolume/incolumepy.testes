import csv # uso018
from builtins import classmethod
from collections import defaultdict


class Listas:
    a = ['red', 'green', 'blue', 'yellow']
    b = ['raymond', 'rachel', 'matthew']
    d = {'matthew': 'yellow', 'rachel': 'green', 'raymond': 'red', 'brito': 'blue'}
    colors = ['red', 'green', 'red', 'blue', 'green', 'red', 'yellow']
    names = ['raymond', 'rachel', 'matthew', 'roger',
         'betty', 'melissa', 'judith', 'charlie', 'ricardo',
         'ana', 'ada', 'eliana', 'leni']

    csv_file = '../../../data/cidades_brasil.csv'

    def uso001(self):
        '''verso e reverso'''
        for i in self.a:
            print(i)
        print('-'*50)
        for i in reversed(self.a):
            print(i)
        pass

    def uso002(self):
        '''
        enumeraçao de listas
        :return:
        '''
        for i, cor in enumerate(self.a):
            print(i, cor)
        print('-'*50)
        for i, cor in enumerate(self.a,1):
            print(i, cor)
        pass

    def uso003(self):
        '''
        ordenaçao
        :return:
        '''
        for cor in sorted(self.a):
            print (cor)
        pass

    def uso004(self):
        '''
        ordenaçao reversa
        :return:
        '''
        for cor in sorted(self.a, reverse=True):
            print (cor)

        pass

    def uso005(self):
        '''
        concatenaçao de valores oriundos de duas listas
        :return:
        '''
        for i, j in zip(self.a, self.b):
            print(i, j)
        pass

    def uso006(self):
        '''
        ordenaçao por quantia de caracteres(chave) normal e reversa
        :return:
        '''
        for i in sorted(self.a, key=len):
            print(i)
        print('-'*50)
        for i in sorted(self.a, key=len, reverse=True):
            print(i)
        pass

    def uso007(self):
        '''
        percorrer itens de dicionarios
        :return:
        '''
        for i, j in self.d.items():
            print(i,j)
        pass

    def uso008(self):
        '''
        construir um dicionario dinamicamente, a partir de listas
        :return:
        '''
        d = dict(zip(self.a, self.b))
        print(d)
        print('-'*50)
        for i in d.items():
            print(i)
        print('-'*50)
        for i, j in d.items():
            print(i,j)
        pass

    def uso009(self):
        '''
        Contagem de elementos em uma lista
        :return:
        '''
        d ={}
        for cor in self.colors:
            d[cor] = d.get(cor,0) + 1
        print(d)
        return d
        pass

    def uso010(self):
        '''
        agrupamento pelo tamanho do nome
        :return:
        '''
        d = defaultdict(list)
        for name in self.names:
            key = len(name)
            d[key].append(name)

        for i in d.items():
            print(i)
        pass

    def uso011(self):
        '''
        agrupamento pelo 2º campo da tupla
        :return:
        '''
        s = [('yellow', 1), ('blue', 2), ('yellow', 3), ('blue', 4), ('red', 1)]
        d = defaultdict(list)
        for k, v in s:
            d[k].append(v)
        sorted(d.items())

        for i in d.items():
            print(i)
        pass

    def uso012(self):
        '''
        retirada de item de um dicionario
        :return:
        '''
        d = {'matthew': 'blue', 'rachel': 'green', 'raymond': 'red'}
        while d:
            key, value = d.popitem()
            print(key, value)
        print(d)
        return d
        pass

    def uso013(self):
        '''
        concatenaçao de valores de lista
        :return:
        '''
        s = ' '.join(self.names)
        print(s)
        return s
        pass

    def uso014(self):
        '''
        sumarizaçao da quantidade de caracteres em strings
        :return:
        '''
        d = defaultdict(int)
        s = 'mississippi'
        print(s)
        for k in s:
            d[k] += 1

        print(type(d.items()))
        print(d.items())

        for i in d.items():
            print(i)

        return d.items()
        pass

    def uso015(self):
        '''
        configuraçao default para dicionarios
        :return:
        '''
        sorvete = defaultdict(lambda : 'Balnilha')
        sorvete['Eliana'] = 'Chocolate'
        sorvete['Ada'] = 'Morango'
        sorvete['Ana'] = 'Creme'

        print(sorvete.items())
        print(sorvete['Ricardo'])
        sorvete['Rodrigo']
        sorvete['Leni']
        print(sorvete.items())
        pass

    def uso016(self):
        '''
        agrupamento de cidades por estado
        :return:
        '''
        food_list = 'spam spam meat spam spam spam spam eggs spam meat'.split()
        food_count = defaultdict(int) # default value of int is 0
        for food in food_list:
            food_count[food] += 1 # increment element's value by 1

        print(food_list, food_count.items())

    def uso017(self):
        city_list = [('TX','Austin'), ('TX','Houston'), ('NY','Albany'), ('NY', 'Syracuse'),
                     ('NY', 'Buffalo'), ('NY', 'Rochester'), ('TX', 'Dallas'),
                     ('CA','Sacramento'), ('CA', 'Palo Alto'), ('GA', 'Atlanta')]
        cities_by_state = defaultdict(list)

        for state, city in city_list:
            cities_by_state[state].append(city)

        print(cities_by_state.items())
        for state, cities in cities_by_state.items():
            print (state, ', '.join(cities))

        pass

    def uso018(self):
        '''
        agrupamento de cidades por estado em ordem alfabetica,
        tanto por estado quanto por cidades.
        :return:
        '''
        cities_by_state = defaultdict(list)

        with open(self.csv_file) as filein:
            cidades = [tuple(line) for line in csv.reader(filein)]
        print(cidades)
        print('-'*50)

        for city, state in cidades:
            cities_by_state[state].append(city)

        print(cities_by_state.items())
        print('-'*50)

        for state, cities in sorted(cities_by_state.items()):
            print (state, ', '.join(cities))

        pass
    def uso019(self):
        '''
        quantidade de cidades por estado
        :return:
        '''
        cities_by_state = defaultdict(int)

        with open(self.csv_file) as filein:
            cidades = [tuple(line) for line in csv.reader(filein)]

        for city, state in cidades:
            cities_by_state[state] += 1
        print(cities_by_state)

        for city, i in sorted(cities_by_state.items()):
            print(city, i)
        pass

    def uso020(self):
        '''
        Ordenaçao pelo Estado com mais cidades
        :return:
        '''
        cities_by_state = defaultdict(int)

        with open(self.csv_file) as filein:
            cidades = [tuple(line) for line in csv.reader(filein)]

        for city, state in cidades:
            cities_by_state[state] += 1

        li_ord = sorted(cities_by_state.items(), key = lambda x: (x[1], x[0]))

        for city, i in li_ord:
            print(city, i)

        pass

    def uso021(self):
        '''
        Ordenaçao reversa pelo Estado com mais cidades
        :return:
        '''
        cities_by_state = defaultdict(int)

        with open(self.csv_file) as filein:
            cidades = [tuple(line) for line in csv.reader(filein)]

        for city, state in cidades:
            cities_by_state[state] += 1

        for city, i in sorted(cities_by_state.items(), key = lambda x: (x[1], x[0]), reverse=True):
            print(city, i)

        pass
    @classmethod
    def Main(self):
        try:
            print('uso001 '+'='*50)
            Listas().uso001()
            print('uso002 '+'='*50)
            Listas().uso002()
            print('uso003 '+'='*50)
            Listas().uso003()
            print('uso004 '+'='*50)
            Listas().uso004()
            print('uso005 '+'='*50)
            Listas().uso005()
            print('uso006 '+'='*50)
            Listas().uso006()
            print('uso007 '+'='*50)
            Listas().uso007()
            print('uso008 '+'='*50)
            Listas().uso008()
            print('uso009 '+'='*50)
            Listas().uso009()
            print('uso010 '+'='*50)
            Listas().uso010()
            print('uso011 '+'='*50)
            Listas().uso011()
            print('uso012 '+'='*50)
            Listas().uso012()
            print('uso013 '+'='*50)
            Listas().uso013()
            print('uso014 '+'='*50)
            Listas().uso014()
            print('uso015 '+'='*50)
            Listas().uso015()
            print('uso016 '+'='*50)
            Listas().uso016()
            print('uso017 '+'='*50)
            Listas().uso017()

            print('uso018 '+'='*50)
            Listas().uso018()

            print('uso019 '+'='*50)
            Listas().uso019()

            print('uso020 '+'='*50)
            Listas().uso020()
            print('uso021 '+'='*50)
            Listas().uso021()
        except Exception as e:
            #raise (e)
            print(e)



        pass



if __name__ == '__main__':
    Listas.Main()
