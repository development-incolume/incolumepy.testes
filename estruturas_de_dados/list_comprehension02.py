'''
map
'''

numeros = [x for x in range(1,11)]
final = map((lambda x: x ** 2),numeros)
print(final)

a = list(map(lambda x: x** 2, [1,2,3]))
print(a)

q = list(map(lambda x: x**2 ,[x for x in range(1,11)]))
print(q)
