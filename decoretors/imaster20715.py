# https://imasters.com.br/artigo/20715/py/dicas-truques-e-hacks-de-python-parte-4/?trace=1519021197&source=single

def decorator01(func):
    '''
    incrementa o retorno em 1
    :param func:
    :return:
    '''
    return lambda: func() + 1


def decorator02(func):
    '''
    print the return
    :param func:
    :return:
    '''

    def print_func():
        print(func())

    return print_func


def function01():
    return 41


@decorator01
def function02():
    return 41


@decorator02
@decorator01
def function03():
    return 41


if __name__ == '__main__':
    print(function01())
    print(function02())
    function03()
