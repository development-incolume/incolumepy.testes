# Autor: professor Fernado Masanori
from functools import wraps


def hitcchiker(func):
    @wraps(func)
    def wrapper(n): return 42

    return wrapper


@hitcchiker
def fat(n):
    if n < 2: return 1
    return n * fat(n - 1)


@hitcchiker
def fib(n):
    if n < 2:   return 1
    return fib(n - 1) * fib(n - 2)


if __name__ == '__main__':
    print(fat(6))
    print(fib(6))
