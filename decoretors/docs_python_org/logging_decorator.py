import logging
from contextlib import ContextDecorator

logging.basicConfig(level=logging.INFO)


class track_entry_and_exit(ContextDecorator):
    def __init__(self, name):
        self.name = name

    def __enter__(self):
        logging.info('Entering: %s', self.name)

    def __exit__(self, exc_type, exc, exc_tb):
        logging.info('Exiting: %s', self.name)


# Instances of this class can be used as both a context manager:
with track_entry_and_exit('widget loader'):
    print('Some time consuming activity goes here')


# And also as a function decorator:
@track_entry_and_exit('widget loader')
def activity():
    print('Some time consuming activity goes here')


print()
activity()
