import logging
from contextlib import ContextDecorator
from os.path import splitext

logfile = '{}.txt'.format(splitext(__file__)[0])
fname = __file__.replace('.py', '.{}'.format('log'))
#print(logfile, fname)

logging.basicConfig(filename=fname, level=logging.INFO,
                    format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')


class logging_it(ContextDecorator):
    def __init__(self, name, *args, **kwargs):
        self.name = name
        self.args = args
        self.kwargs = kwargs

    def __enter__(self):
        logging.info('Entering ({})'.format(self.name))
        logging.debug('Entering ({})'.format(self.name))

    def __exit__(self, *exc):
        logging.info('Exiting ({})'.format(self.name))
        logging.debug('Entering ({})'.format(self.name))

    def notify(self):
        return self.name


# Instances of this class can be used as both a context manager:
with logging_it('with loader'):
    print('Some time consuming activity goes here')


# And also as a function decorator:
@logging_it('decorator loader')
def activity():
    print('Some time consuming activity goes here')


print()
activity()
