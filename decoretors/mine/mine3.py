from functools import wraps
from os.path import splitext

def recordum(logfile='{}.log'.format(splitext(__file__)[0])):
    def decorador(func):
        @wraps(func)
        def envolucro(*args, **kwargs):
            log_str = '\n\n{} executada.'.format(func.__qualname__)
            print(log_str)
            with open(logfile, 'a') as file:
                file.writelines(log_str)
                file.writelines('\nargs: {}'.format(args))
                file.writelines('\nkwargs: {}'.format(kwargs))
                file.write('\nresultado: {}'.format(None))
        return envolucro
    return decorador


@recordum()
def square(x):
    return (x**2)

if __name__ == '__main__':
    square(1, x=2)