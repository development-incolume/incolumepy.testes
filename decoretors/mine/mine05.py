import logging
from functools import wraps

logfile = __file__.replace('.py', '.log')
print(logfile)

log = logging.basicConfig(filename=logfile,
                    level=logging.INFO,
                    format='%(asctime)s;%(levelname)s;%(name)s;%(message)s')


def recordum(func):
    log.addHandler(logging.FileHandler('0'+logfile))
    @wraps(func)
    def envolucro(*args, **kwargs):
        log_str = '{}({}) executada, {}, {}'.format(
            func.__qualname__,
            func.__name__,
            args,
            kwargs
        )
        print(log_str)
        logging.info(log_str)
        return func(*args, **kwargs)
    return envolucro

def logit(level='INFO'):
    log.addHandler(logging.FileHandler('1' + logfile))
    def decorador(func):
        @wraps(func)
        def envolucro(*args, **kwargs):
            log_str = '{}({}) executada, {}, {}'.format(
                func.__qualname__,
                func.__name__,
                args,
                kwargs
            )
            print(log_str)
            logging.info(log_str)
            return func(*args, **kwargs)
        return envolucro
    return decorador


@recordum
def truncus():
    return 'qualquer coisa.'
@logit()
def chest():
    return 'qualquer coisa.'


truncus()
chest