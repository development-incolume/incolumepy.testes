from os.path import splitext


class Logit(object):
    def __init__(self, logfile='{}.log'.format(splitext(__file__)[0])):
        self.logfile = logfile

    def __call__(self, func):
        log_string = "{} was called".format(func.__qualname__)
        print(log_string)
        # Open the logfile and append
        with open(self.logfile, 'a') as opened_file:
            # Now we log to the specified logfile
            opened_file.write(log_string + '\n')
        # Now, send a notification
        self.notify()

    def notify(self):
        # logit only logs, no more
        pass


@Logit()
def myfunc1():
    pass

@Logit
def myfunc2():
    pass

if __name__ == '__main__':
    myfunc1()
    myfunc2()
