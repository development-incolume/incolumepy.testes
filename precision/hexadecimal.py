def f_hex01():
    return 0x11


def f_hex02():
    return 0x12


def int2hex(i):
    return hex(i)


def hex2int(x):
    return int(x)


if __name__ == '__main__':
    print(f_hex01())
    print(f_hex02())
    print(int2hex(10))
    print(hex2int(0xf))
