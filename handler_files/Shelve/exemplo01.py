import shelve


def encode_shelve():
    user = shelve.open('data.txt')
    user['nickname'] = 'stummjr'
    user['city'] = 'Blumenau'
    user['twitter'] = 'stummjr'
    print(user)
    # {'city': 'Blumenau', 'twitter': 'stummjr', 'nickname': 'stummjr'}
    user.close()


def decode_shelve(file='data.txt'):
    user = shelve.open(file)
    print(user)
    # {'city': 'Blumenau', 'twitter': 'stummjr', 'nickname': 'stummjr'}
    user['blog'] = 'pythonhelp.wordpress.com'
    user.close()


if __name__ == '__main__':
    encode_shelve()
    decode_shelve()
