with open('b.txt') as file:
    content = file.readlines()
print(type(content))
print(content)

print()
with open('b.txt') as file:
    content = [x.strip() for x in file.readlines()]
print(type(content))
print(content)

print()
with open('b.txt') as file:
    content = file.readline()
print(type(content))
print(content)

print()
with open('b.txt') as file:
    content = file.read()
print(type(content))
print(content)

print()
with open('b.txt') as file:
    content = file.readable()
print(type(content))
print(content)
