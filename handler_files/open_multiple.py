def multiples():
    try:
        with open('a.txt', 'w') as a, open('b.txt', 'w') as b:
            a.write('arquivo A\n')
            b.write('arquivo B\n')
    except IOError as e:
        print('Operation failed: %s' % e.strerror)


def copy(fileOrigem, fileDestino):
    try:
        with open(fileOrigem) as a, open(fileDestino, 'a') as b:
            b.write(a.read())
        return True
    except IOError as e:
        print(e.strerror)
        return False


if __name__ == '__main__':
    multiples()
    copy('a.txt', 'b.txt')
