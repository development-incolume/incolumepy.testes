from testing_json1 import *
from testing_json2 import *
from testing_json3 import *
from testing_json4 import *
from testing_json5 import *
#from testing_json6 import *

def menu():
    createjson1('jsonfiles/json1.json')
    testing_json2('jsonfiles/json2.json')
    testing_json3('jsonfiles/json3.json')
    testing_json4('jsonfiles/json4.json')
    #testing_json5('jsonfile/json5.json', DEBUG=True)
    testing_json5('jsonfile/json6.json', DEBUG=False)
    testing_json5('jsonfiles/json7.json', dir='./', DEBUG=False)
    testing_json5('jsonfiles/json8.json', dir='/tmp', DEBUG=False)
    pass


if __name__ == '__main__':
    menu()