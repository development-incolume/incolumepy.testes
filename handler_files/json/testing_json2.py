import subprocess
import json
from datetime import datetime, date, time


def testing_json2(filename, dir='jsonfiles', DEBUG = False):
    tree = dict()
    tree[dir] = list()
    header = ["Pemissões", "link", "user", "group", "size", "date", "name"]
    row = []
    output = subprocess.getoutput('ls --full-time %s' % dir)
    if DEBUG: print(output)
    var = [n for n in output.split('\n')]
    if DEBUG: print(var)

    for i in range(len(var)):
        if i > 0:
            row.append(var[i].strip().split())

    for r in row:
        r[6] = r[6].split('.')[0]
        r[5] = r[5] + " " + r[6]
        r[6] = r[8]
        del r[8]
        del r[7]

    if DEBUG:  print(len(row))
    for i in range(len(row)):
        # print(i, row[i])
        d = {}
        for j in range(len(row[i])):
            d[header[j]] = row[i][j]
        tree[dir].append(d)

    if DEBUG: print(tree)

    with open(filename, 'w') as f:
        f.write(str(tree))

    if DEBUG: print(tree)


if __name__ == '__main__':
    pass