__author__ = '@britodfbr'
import os


def path01a(path='fake_dir'):
    try:
        os.makedirs(path, mode=0o777, exist_ok=True)
    except FileExistsError as e:
        print(e)
    except:
        raise
    pass


if __name__ == '__main__':
    path01a()
    path01a('true_dir')
