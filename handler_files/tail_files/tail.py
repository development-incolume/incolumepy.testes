# https://pt.stackoverflow.com/questions/84254/ler-as-%c3%baltimas-5-linhas-de-um-arquivo-atrav%c3%a9s-do-python?rq=1

def tail(f, n):
    '''Arquivos grandes sem ocupar memória toda'''
    assert n >= 0
    pos, lines = n + 1, []
    while len(lines) <= n:
        try:
            f.seek(-pos, 2)
        except IOError:
            f.seek(0)
            break
        finally:
            lines = list(f)
        pos *= 2
    return lines[-n:]


def tail_file(file_name, number):
    lines = []
    with open(file_name) as f:
        for line in f:
            lines.append(line.rstrip())
            if len(lines) > number:
                lines.pop(0)
    return lines


def my_tail0(filename, lines):
    with open(filename) as file:
        return file.readlines()[-lines:]


def my_tail1(filename, lines):
    with open(filename) as file:
        return [x.rstrip() for x in file.readlines()[-lines:]]


def my_tail2(filename, lines):
    with open(filename) as file:
        for item in [x.rstrip() for x in file.readlines()[-lines:]]:
            yield item


def my_tail3(filename, lines):
    with open(filename) as file:
        lista = [x.rstrip() for x in file.readlines()[-lines:]]
        while lista:
            yield lista.pop()


def my_tail4(filename, lines):
    with open(filename) as file:
        try:
            for item in [x.rstrip() for x in file.readlines()[-lines:]]:
                yield item
        except:
            assert StopIteration


def my_tail5(filename, lines):
    with open(filename) as file:
        lista = [x.rstrip() for x in file.readlines()[-lines:]]
        while lista:
            try:
                yield lista.pop()
            except:
                break


def my_tail(filename, lines):
    with open(filename) as file:
        try:
            for item in [x.rstrip() for x in file.readlines()[-lines:]]:
                yield item
        except:
            raise StopIteration


if __name__ == '__main__':
    print(tail(open('tail.py'), 5))
    print(tail_file('tail.py', 5))
    print(my_tail0('tail.py', 5))

    l = [x for x in 'ricardo brito do nascimento']
    print(l)
    print(l[-5:])
    print(my_tail1('tail.py', 5))

    t = my_tail('tail.py', 5)
    print(t)
    print(list(t))

    print()
    print('=' * 20)
    q = my_tail('tail.py', 5)
    print('q>>', q)
    for i in q:
        print(next(q))
