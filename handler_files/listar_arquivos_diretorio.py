import os
from glob import glob


def encontrar_arq0(arquivo, caminho='/home/brito/Documentos'):
    '''
    Utiliza o os.listdir
    :param arquivo: Nome do arquivo a ser encontrado
    :param caminho: Path onde procurar
    :return: Boolean
    '''
    for arq in os.listdir(caminho):
        if arquivo == arq:
            return True
    return False


def encontrar_arq1(arquivo, caminho='/home/brito/Documentos'):
    '''
    Utiliza o os.walk
    :param arquivo: Nome do arquivo a ser encontrado
    :param caminho: Path onde procurar
    :return: Boolean
    '''
    for base, path, arquivos in os.walk(caminho):
        for arq in arquivos:
            if arquivo == arq:
                return True
    return False


def encontrar_arq(arquivo, ext='*.*', caminho='/home/brito/Documentos'):
    '''
    Utiliza o glob.glob
    Para restringir extensões ext='*.txt' exibir somente arquivos .txt
    :param arquivo: Nome do arquivo a ser encontrado
    :param caminho: Path onde procurar
    :return: Boolean
    '''
    for arq in glob('{}/{}'.format(caminho, ext)):
        if arquivo in os.path.basename(arq):
            return True
    return False


if __name__ == '__main__':
    print(encontrar_arq0('cAMILA _ANA.pdf'))
    print(encontrar_arq1('cAMILA _ANA.pdf'))
    print(encontrar_arq('cAMILA _ANA.pdf'))
