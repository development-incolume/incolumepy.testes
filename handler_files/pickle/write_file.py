import pickle
from collections import namedtuple


def write_pickle(obj, fileout='data.pickle'):
    with open(fileout, 'wb') as f:
        # Pickle the 'data' dictionary using the highest protocol available.
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


if __name__ == '__main__':
    Obj = namedtuple('obj', 'title url')
    a = Obj('blog', 'brito.blog.incolume.com.br')
    write_pickle(a)
    pass
