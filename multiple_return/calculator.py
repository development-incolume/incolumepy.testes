def calculator(a, b, op):
    return {
        '+': a + b,
        '-': a - b,
        '*': a * b,
        '/': a / b,
        '//': a // b,
        '%': a % b,
    }.get(op)


if __name__ == '__main__':
    print(calculator(7, 3, '+'))
    print(calculator(7, 3, '-'))
    print(calculator(7, 3, '*'))
    print(calculator(7, 3, '/'))
    print(calculator(7, 3, '//'))
    print(calculator(7, 3, '%'))
