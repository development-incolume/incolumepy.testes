from concurrent.futures import ThreadPoolExecutor, as_completed
from incolumepy.testes.miscelanea.Fibonacci2 import fibonacci

numeros = [10, 20, 25]

with ThreadPoolExecutor(max_workers=5) as executor:
    fibSubmit = {executor.submit(fibonacci, n,): n for n in numeros}

    for future in as_completed(fibSubmit):
        try:
            n, f = future.result()
        except Exception as exc:
            print("Erro! {0}".format(exc))
        else:
            print("{0}: {1}".format(n, f))