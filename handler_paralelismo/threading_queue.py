import Queue, threading
from incolumepy.testes.miscelanea.Fibonacci2 import fibonacci

q = Queue.Queue()

numeros = [10, 20, 25]

for n in numeros:
    t = threading.Thread(target=fibonacci, args = (n,))
    t.daemon = True
    t.start()

while not q.empty():
    n, f = q.get()
    print ("{0}: {1}".format(n, f))