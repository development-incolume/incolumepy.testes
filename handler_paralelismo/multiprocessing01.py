from multiprocessing import Pool
from incolumepy.testes.miscelanea.Fibonacci2 import fibonacci

if __name__ == '__main__':
    try:
        p = Pool()
        print (p.map(fibonacci, [10, 15, 20]))
    finally:
        p.close()


