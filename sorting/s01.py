naipes = 'Paus Copas Espadas Ouros'.split()
valores = 'A 2 3 4 5 6 7 8 9 10 J Q K'.split()

baralho = [(x, y) for x in naipes for y in valores]

print(naipes)
print(valores)
print(baralho)

baralho.sort()
print(baralho)
