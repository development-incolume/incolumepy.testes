import time


def monitorar(logfile='file.log'):
    with open(logfile) as file:
        while True:
            linha = file.readline().strip()
            if linha:
                yield linha
            else:
                time.sleep(.5)


if __name__ == '__main__':
    logfile = 'file.log'
    # for item, linha in enumerate(monitorar(logfile)):
        # print(item, linha)
        # print('%s: %s' % (item, linha))
    # print('{:6d}: {}'.format(item, linha))

    for i in monitorar('aquitava.log'):
        print(i)
