import time


def monitorar(path):
    with open(path, 'r') as arq:
        while True:
            nova_linha = arq.readline()
            nova_linha = nova_linha.replace('\n', '')
            if nova_linha:
                yield nova_linha
            else:
                time.sleep(1.0)


if __name__ == '__main__':
    caminho_arquivo = 'file.log'
    for idx, linha in enumerate(monitorar(caminho_arquivo)):
        print("{:5d}: {}".format(idx, linha))
