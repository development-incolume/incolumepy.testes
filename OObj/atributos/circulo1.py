from incolumepy.geometria.figura_geometrica import FiguraGeometrica


class Circulo(FiguraGeometrica):
    '''
    >>> c = Circulo()
    >>> c.__dict__
    {'raio': None}
    >>> c.raio = 1
    >>> c.raio
    1
    >>> c.lado = 2
    Traceback (most recent call last):
      File "/usr/lib/python3.5/doctest.py", line 1321, in __run
        compileflags, 1), test.globs)
      File "<doctest __main__.Circulo[4]>", line 1, in <module>
        c.lado = 2
      File "/home/brito/projetos/incolumepy.geometria/incolumepy/geometria/circulo1.py", line 33, in __setattr__
        raise AttributeError('Atributo "{}" não permitido.'.format(key))
    AttributeError: Atributo "lado" não permitido.
    >>> c.lado
    Traceback (most recent call last):
      File "/usr/lib/python3.5/doctest.py", line 1321, in __run
        compileflags, 1), test.globs)
      File "<doctest __main__.Circulo[5]>", line 1, in <module>
        c.lado
      File "/home/brito/projetos/incolumepy.geometria/incolumepy/geometria/circulo1.py", line 46, in __getattr__
        raise AttributeError('Atributo "{}" inexistente.'.format(item))
    AttributeError: Atributo "lado" inexistente.
    '''
    __dict__ = {'raio': None}

    def __init__(self, **kwargs):
        super()
        for key in kwargs.keys():
            if key in self.__dict__:
                setattr(self, key, kwargs[key])

    def __setattr__(self, key, value):
        if key not in self.__dict__:
            raise AttributeError('Atributo "{}" não permitido.'.format(key))
        self.__dict__[key] = value

    def __getattr__(self, item):
        if item not in self.__dict__:
            raise AttributeError('Atributo "{}" inexistente.'.format(item))
        return self.__dict__[item]


if __name__ == '__main__':
    import doctest

    doctest.testmod()
