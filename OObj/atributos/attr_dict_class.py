class Classe:
    def __init__(self):
        self.x = None
        self.y = None
        self.z = None

    def __getattr__(self, item):
        if item in self.__dict__:
            return self.__dict__[item]
        else:
            raise AttributeError('Error Atribute {}'.format(item))

    def __setattr__(self, key, value):
        if key in self.__dict__:
            self.__dict__[key] = value
        else:
            raise AttributeError('Error Atribute {}'.format(key))


if __name__ == '__main__':
    c = Classe()

    print(c.__dict__)
    c.x = 0
    c.y = 0
    c.z = 0
    print(c.__dict__)
    c.w = 1
    print(c.__dict__)
