class Foo:
    '''
    classdocs
    >>> f = Foo()
    >>> f.nome = 'nome'
    >>> f.nome
    'nome'
    >>> f._id = 1
    >>> f._id
    1
    >>> f.foo = Foo
    >>> f.foo
    <class 'attr_dinamicos_classes.Foo'>
    '''
    _id = None
    name = None

    def __init__(self, **attrs):
        if attrs:
            for key, value in attrs.items():
                setattr(self, key, value)

    def __getattr__(self, key):
        if key in self.__dict__:
            return self.__dict__[key]
        return None

    def __setattr__(self, k, v):
        if hasattr(self, k):
            super().__setattr__(k, v)
            return True
        return False

    @staticmethod
    def uso():
        f = Foo()
        print(f)
        print(f._id)
        print(f.name)
        print(f.foo)

        print()
        g = Foo(_id=0, name='asdf', foo=None)
        print(g)
        print(g._id)
        print(g.name)
        print(g.foo)


if __name__ == '__main__':
    import doctest

    doctest.testmod()

    Foo.uso()
