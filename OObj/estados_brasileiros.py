import sys, pprint

class Estados:
    '''
    Controle de acesso aos metodos e atributos de Classe
    Estado e Capitais Brasileiros: http://www.estadosecapitaisdobrasil.com/
    '''
    __dict__ = {'estado':None}
    capitais ={
        'AC': ('Acre', 'Rio Branco', 'Norte'),
        'AL': ('Alagoas', 'Maceió ', 'Nordeste'),
        'AP': ('Amapá', 'Macapá ', 'Norte'),
        'AM': ('Amazonas', 'Manaus ', 'Norte'),
        'BA': ('Bahia', 'Salvador ', 'Nordeste'),
        'CE': ('Ceará', 'Fortaleza ', 'Nordeste'),
        'DF': ('Distrito Federal', 'Brasília ', 'Centro-Oeste'),
        'ES': ('Espírito Santo', 'Vitória ', 'Sudeste'),
        'GO': ('Goiás', 'Goiânia ', 'Centro-Oeste'),
        'MA': ('Maranhão', 'São Luís ', 'Nordeste'),
        'MT': ('Mato Grosso', 'Cuiabá ', 'Centro-Oeste'),
        'MS': ('Mato Grosso do Sul', 'Campo Grande ', 'Centro-Oeste'),
        'MG': ('Minas Gerais', 'Belo Horizonte ', 'Sudeste'),
        'PA': ('Pará', 'Belém ', 'Norte'),
        'PB': ('Paraíba', 'João Pessoa ', 'Nordeste'),
        'PR': ('Paraná', 'Curitiba ', 'Sul'),
        'PE': ('Pernambuco', 'Recife ', 'Nordeste'),
        'PI': ('Piauí', 'Teresina ', 'Nordeste'),
        'RJ': ('Rio de Janeiro', 'Rio de Janeiro ', 'Sudeste'),
        'RN': ('Rio Grande do Norte', 'Natal ', 'Nordeste'),
        'RS': ('Rio Grande do Sul', 'Porto Alegre ', 'Sul'),
        'RO': ('Rondônia', 'Porto Velho ', 'Norte'),
        'RR': ('Roraima', 'Boa Vista ', 'Norte'),
        'SC': ('Santa Catarina', 'Florianópolis ', 'Sul'),
        'SP': ('São Paulo', 'São Paulo ', 'Sudeste'),
        'SE': ('Sergipe', 'Aracaju ', 'Nordeste'),
        'TO': ('Tocantins', 'Palmas ', 'Norte'),
    }

    def __init__(self, **kwargs):
        for key in kwargs:
            self.__dict__[key] = kwargs[key]

    def __setattr__(self, key, value):
        if key in self.__dict__:
            self.__dict__[key] = value
        else:
            raise AttributeError('Atributo "{}" não permitido'.format(key))

    def __getattr__(self, item):
        if item in self.__dict__:
            return self.__dict__[item]
        else:
            raise AttributeError('Atribuito "%s" inexistente"' % item)

#    def __repr__(self):
#        result = ''
#        for var in self.__dict__:
#            if var == 'estado':
#                result += "'Estado': '%s', 'Capital': '%s', " \
#                "'Região': '%s'," % (a.capitais[self.estado][0],
#                                a.capitais[self.estado][1],
#                                a.capitais[self.estado][2])
#            else:
#                result += "'%s': '%s'," % (var, self.__dict__[var])
#        #print('#3>>',type(result))
#        return result

    def __repr__(self):
        result = {}
        for var in self.__dict__:
            if var == 'estado':
                result['estado'] = a.capitais[self.estado][0]
                result['capital'] = a.capitais[self.estado][1]
                result['região'] = a.capitais[self.estado][2]
            else:
                result[var] = self.__dict__[var]
        #print('#3>>',type(result))
        return str(result)


if __name__ == '__main__':
    a = Estados(path='/tmp', file='teste.txt')
    print(a.__dict__)

    print('---')
    try:
        a.test = print
        print(a)
    except:
        print(sys.exc_info())
    pass

    print('---')
    print(a.capitais)

    print('---')
    print("Estado: %s, Capital: %s, Região: %s" % (a.capitais['AP'][0], a.capitais['AP'][1], a.capitais['AP'][2]))

    print('---')
    try:
        print(a)
    except:
        print(sys.exc_info())

    print('---')
    ap = Estados(estado='AP')
    print(ap)
    df = Estados(estado='DF')
    print(df)

    print('---')
    print('>>>', ap)
    print('>>>', df)
