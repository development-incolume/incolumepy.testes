import sys
class Menu:
    '''

    '''
    takeaction = dict()
    sair_options = ['0', 'sair', 's', 'exit', 'e']
    # Uses the get method to allow us to specify a default

    def __init__(self):
        #takeaction['0'] = sair
        self.set_actions('0', self.sair)
        #takeaction['1'] = somar
        #takeaction['2'] = subtrair
        #takeaction['3'] = multiplicar
        #takeaction['4'] = dividir
        #takeaction['5'] = resto
        #takeaction['6'] = potenciar

    def errhandler(self):
        return ("Esta entrada não foi reconhecida!")

    def sair(self, option):
        if str(option).lower() in self.sair_options:
            return True

    def somar(self, x, y):
        return x + y

    def subtrair(self, x, y):
        return x - y

    def multiplicar(self, x, y):
        return x * y

    def dividir(self, x, y):
        try:
            return x // y
        except:
            return sys.exc_info()[1]

    def potenciar(self, x, y):
        return x ** y

    def resto(self, x, y):
        return x % y

    def set_actions(self, flag, method):
        self.takeaction[flag] = method

    def Main(self):
        while True:
            itens_menu = '''
            0 - Sair
            1 - soma
            2 - subtração
            3 - multiplicação
            4 - divisão
            5 - resto divisão
            6 - potencia

            '''
            print(itens_menu)
            option = input("Escolhe entre as opções acima: ")
            #if cls.sair(cls, option): sys.exit(0)
            print('%s\n' % self.takeaction.get(option, self.errhandler()))
if __name__ == '__main__':
    a = Menu()
    a.Main()

    pass
