from incolumepy.testes.OObj.herança_example2.eletronico import Eletronico
from incolumepy.testes.OObj.herança_example2.som import Som
from incolumepy.testes.OObj.herança_example2.tv import Tv
from incolumepy.testes.OObj.polimorfismo.canino import Canino


class Pertences:
    def __init__(self):
        self.pertences = list()

    def addPertences(self, *args):
        try:

            for i in args:
                assert isinstance(i, Eletronico)
                self.pertences.append(i)
            return True
        except AssertionError as e:
            raise TypeError('Somente Objetos da Class Eletronico')
        except:
            raise
        return False


if __name__ == '__main__':
    pertences = Pertences()
    lesse = Canino('Lesse', 3, .8)
    tv1 = Tv('lg', 32)
    tv2 = Tv('lg', 50)
    som = Som('Sony')
    pertences.addPertences(tv1, tv2)
    pertences.addPertences(lesse)
    print(pertences.__dict__)
