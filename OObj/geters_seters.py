class Square(object):
    '''Access the members directly'''
    def __init__(self, length):
        self.length = length

class NewSquare(object):
    '''Use built-in property decorator'''
    def __init__(self, length):
        self.length = length

    @property
    def length(self):
        print('..getter..')
        return self._length


    @length.setter
    def length(self, value):
        print('..setter..')
        self._length = value

    @length.deleter
    def length(self):
        print('..deleter..')
        del self._length

    @classmethod
    def print_class_name(cls):
        print("class name: {0}".format(cls))
        print("Hello, I am {0}!".format(cls))

if __name__ == '__main__':
    r = Square(5)
    print(r.length)
    r.length = 6

    print('*' * 20)
    r = NewSquare(5)
    print('-' * 20)
    print(r.length)  # automatically calls getter
    print('-' * 20)
    r.length = 6  # automatically calls setter
    r.print_class_name()
