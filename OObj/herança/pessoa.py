from incolumepy.testes.OObj.herança.ipessoa import IPessoa


class Pessoa(IPessoa):
    '''Pessoa implementa a interface IPessoa'''

    def __init__(self, nomecompleto):
        super(Pessoa, self).__init__(nomecompleto)

    def __str__(self):
        return self.fullname()


if __name__ == '__main__':
    p = Pessoa('Eliana Ferreira dos Santos Brito')
    print(p.__dict__)
