import os
import sys

sys.path.append(os.path.dirname(__file__))
print(sys.path)

from cidadao import Cidadao


class Funcionario(Cidadao):
    def __init__(self, nomecompleto, cpf, domain):
        super(Funcionario, self).__init__(nomecompleto)
        self.cpf = cpf
        self.domain = domain
        self.email = self.getEmail()

    def getEmail(self):
        return '%s.%s@%s' % (self.__dict__['nome'].lower(),
                             self.__dict__['sobrenome'].lower(),
                             self.__dict__['domain'])

    def __str__(self):
        return '(%(nome)s %(sobrenome)s, %(cpf)s, %(email)s)' % self.__dict__

    def __repr__(self):
        return '({f[cpf]}, {f[email]})'.format(f=self.__dict__)


if __name__ == '__main__':
    f = Funcionario('Ada Catarina dos Santos Brito', '123.123.123-12', 'incolume.com.br')
    print(f.__dict__)
    print(f)
    print('{0!s}'.format(f))
    print('{0!r}'.format(f))
