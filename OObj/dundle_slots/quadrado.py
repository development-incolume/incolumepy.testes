from incolumepy.testes.OObj.dundle_slots.formas_geometricas import FormaGeometrica


class Quadrado(FormaGeometrica):
    __slots__ = ('lado')

    def get_area(self):
        return self.lado ** 2

    def get_perimetro(self):
        return self.lado * 4


if __name__ == '__main__':
    q = Quadrado()
    q.lado = 2
    print(q.get_area(), q.get_perimetro())

    q.raio = 2
