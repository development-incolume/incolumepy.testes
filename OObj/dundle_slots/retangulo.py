from incolumepy.testes.OObj.dundle_slots.formas_geometricas import FormaGeometrica


class Retangulo(FormaGeometrica):
    __slots__ = ('__base', '__altura')

    def __init__(self):
        super()

    @property
    def base(self):
        return self.__base

    @base.setter
    def base(self, x):
        self.__base = float(x)

    @property
    def altura(self):
        return self.__altura

    @altura.setter
    def altura(self, x):
        self.__altura = float(x)

    def get_area(self):
        return (self.base * self.altura)

    def get_perimetro(self):
        return (self.base + self.altura) * 2


if __name__ == '__main__':
    r = Retangulo()
    r.altura = 2
    r.base = 2
    print(r.base, r.altura)
    print(r.get_perimetro())
    print(r.get_area())
