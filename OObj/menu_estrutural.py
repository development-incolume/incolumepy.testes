#!/bin/env python
#coding: utf-8
from os import *

def pwd():
	msg(getcwd())

def msg(msg):
	print (msg)

def tipo(var):
	print (type(var))

def somar(x,y):
	return x+y

def subtrair(x,y):
	return x-y

def multiplicar(x,y):
	return x*y

def dividir(x,y):
	return x/y

def powXY(x,y):
	return x**y

def pow10(x):
	return pow(10,x)

def quad(x,y=2):
	return pow(x,y)

def cubo(x,y=3):
	return pow(x,y)

def fibonacci(i):
	a,b,cont = 0,1,1
	dict1={}
  
	while cont <= i:
		dict1[cont]=b
		a,b = b,a+b
		cont+=1
	return dict1
  

def maiorFibonacci():
	fim=int(input("Ultimo termo da sequência será menor que: "))
	dict1 = fibonacci(fim)
	termo=elememento=0

	for term,elem in enumerate(dict1.values()):
		#print term,elem
		if elem < fim:
			termo=term+1

	print ('Sequência de Fibonacci menor que %s: %s' % (fim,dict1[termo]))
	print ('o termo de menor valor que antecede %s é (%d: %d)'% (fim,termo, dict1[termo]))
	
def termoFibonacci():
	fim=int(input("Qual termo? "))
	dict1 = fibonacci(fim)
	
	print ('O termo %s = %s' % (fim,dict1[len(dict1)]))
		
def getValor(s="Informe o valor: "): 
	return int(input(s))

def apresentaResultado(s='sem valor',resultado=0.0): print ('O resutado da operação %s é %.2f' % (s, resultado))

def errhandler ():
   print ("Your input has not been recognised")

def menu():
	sair="-1"
	while sair:
		print ("""
		1) somar
		2) subtrair
		3) multiplicar
		4) dividir
		5) quadrado
		6) cubo
		7) potencia
		8) potencia de 10
		9) fibonacci < x
		10) termo de fibonacci
		0) sair
		""")
		sair=str(input("Informe sua escolha: "))
		if sair == '0': break
		elif sair == '1': apresentaResultado('somar',somar(getValor('X: '),getValor('Y: ')))
		elif sair == '2': apresentaResultado('subtrair',subtrair(getValor(),getValor()))
		elif sair == '3': apresentaResultado('multiplicar', multiplicar(getValor(),getValor()))
		elif sair == '4': apresentaResultado('dividir',dividir(getValor(),getValor()))
		elif sair == '5': apresentaResultado('quadrado',quad(getValor()))
		elif sair == '6': apresentaResultado('cubo',cubo(getValor()))
		elif sair == '7': apresentaResultado('potência',powXY(getValor(),getValor()))
		elif sair == '8': apresentaResultado('potência de 10',pow10(getValor()))
		elif sair == '9': print (maiorFibonacci())
		elif sair == '10': print (termoFibonacci())
		else:
			print('Escolha somente as opções da primos')
		print ('\n')
menu()
#maiorFibonacci()
#termoFibonacci()
