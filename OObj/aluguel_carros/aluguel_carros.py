'''
Exercício Python 015:
Escreva um programa que pergunte a quantidade de Km percorridos
por um carro alugado e a quantidade de dias pelos quais ele foi
alugado.

Calcule o preço a pagar, sabendo que o carro custa R$60
por dia e R$0,15 por Km rodado.
'''


def aluguel_carros(dias, km):
    return (60 * int(dias)) + (.15 * float(km))


class AluguelCarros:
    __slots__ = ('__dias', '__km', '__diaria_valor', '__km_valor')

    def __init__(self, diaria_valor=0, km_valor=0):
        self.__diaria_valor = diaria_valor
        self.__km_valor = km_valor

    @property
    def diaria_valor(self):
        return self.__diaria_valor

    @diaria_valor.setter
    def diaria_valor(self, x):
        self.__diaria_valor = float(x)

    @property
    def km_valor(self):
        self.__km_valor

    @km_valor.setter
    def km_valor(self, x):
        self.__km_valor = float(x)

    @property
    def dias(self):
        return self.__dias

    @dias.setter
    def dias(self, x):
        self.__dias = int(x)
        if self.__dias > 181:
            raise ValueError('Aluguel superior a 6 Meses')

    @property
    def km(self):
        return self.__km

    @km.setter
    def km(self, x):
        self.__km = float(x)

    def value(self):
        return (self.__diaria_valor *
                self.__dias) + (self.__km_valor * self.__km)

    @staticmethod
    def main():
        a = AluguelCarros()
        a.diaria_valor = 60
        a.km_valor = .15
        a.dias = input('Quantos dias? ')
        a.km = input('Quantos quilomentros? ')
        return a.value()


if __name__ == '__main__':
    dias = input('quantos dias?')
    km = input('quantos quilomentos?')
    print(aluguel_carros(dias, km))

    print()
    print(AluguelCarros.main())
