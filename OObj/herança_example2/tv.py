from incolumepy.testes.OObj.herança_example2.eletronico import Eletronico


class Tv(Eletronico):
    def __init__(self, model, pol, name='TV'):
        super(Tv, self).__init__(name, model)
        self.pol = pol
        self.canal = 2

    def canalUp(self):
        self.canal += 1

    def canalDown(self):
        self.canal -= 1

    def status(self):
        return '%(name)s[%(model)s]: %(stat)s; canal: %(canal)s' % (self.__dict__)


if __name__ == '__main__':
    tv = Tv('lg123', 53)
    print(tv.ligado_stat())
    print(tv.ligaDesliga())
    print(tv.ligado_stat())
    print(tv.status())
    print(tv.__dict__)
