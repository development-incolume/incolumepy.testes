import abc


class IEletronico(object, metaclass=abc.ABCMeta):
    ligado = False
    name = ''
    stat = ''

    def __init__(self, name):
        self.name = name

    @abc.abstractclassmethod
    def __str__(self):
        raise NotImplementedError(NotImplemented)

    @abc.abstractclassmethod
    def status(self):
        raise NotImplementedError(NotImplemented)

    def ligado_stat(self):
        if self.ligado:
            self.stat = 'Ligado'
        else:
            self.stat = 'Desligado'
        return self.ligado

    def ligaDesliga(self):
        self.ligado = not self.ligado
        return self.ligado_stat()
