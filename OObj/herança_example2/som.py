from incolumepy.testes.OObj.herança_example2.eletronico import Eletronico


class Som(Eletronico):
    def __init__(self, model, volMax=100, name='MiniSystem'):
        super(Som, self).__init__(name, model)
        self.vol = 5
        self.volMax = volMax

    def volUp(self):
        if self.vol < self.volMax:
            self.vol += 1
            return True
        print('Volume no maximo')
        return False

    def volLess(self):
        if self.vol > 0:
            self.vol -= 1
            return True
        print('Volume no minimo')
        return False


if __name__ == '__main__':
    som = Som('sony')
    print(som)
    print(som.__dict__)
    print(som.status())
