class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

if __name__ == '__main__':
    a = AttrDict()
    print(a.__dict__)

    b = AttrDict(b='b')
    print(b.__dict__)

    c = AttrDict({1:1, 2:1, 3:2, 4:3, 5: 5, 6: 8})
    print(c.__dict__)
