import sys
class myClass:
    __dict__ = {}
    __dict__['var1'] = None
    __dict__['var2'] = None

    def __init__0(self, var1=0, var2=0):
        self.var1 = var1
        self.var2 = var2


    def __getattr__(self, item):
        if item in self.__dict__:
            return self.__dict__[item]
        else:
            raise AttributeError('Paramentro ou atributo "%s" inexistente.' % item)

    def __setattr__(self, key, value):
        if key in self.__dict__:
            self.__dict__[key] = value
        else:
            raise AttributeError('Parametro ou metodo "%s" não permitido' % key)

if __name__ == '__main__':
    a = myClass()
    # b = myClass(var1=True, var2=False)

    print(a.__dict__)
    try:
        a.var1 = 2
        a.var3 = None
    except:
        print(sys.exc_info()[1])
    print(a.__dict__)
    pass