# https://www.youtube.com/watch?v=sPiWg5jSoZI

class Stock:
    def __init__(self, name, price):
        self.name = name
        self.price = price


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Address:
    def __init__(self, hostname, port):
        self.port = port
        self.hostname = hostname


# Com metaprogramação
class Structure:
    _fields = []

    def __init__(self, *args):
        for name, val in zip(self._fields, args):
            setattr(self, name, val)


class new_Stock(Structure):
    _fields = ['self', 'name', 'price']


class new_Point(Structure):
    _fields = ['x', 'y']


class new_Address(Structure):
    _fields = ['hostname', 'port']


if __name__ == '__main__':
    c = new_Point
    c.novo = ''
    print(c.__dict__)
