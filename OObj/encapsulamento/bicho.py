__author__ = 'Luciano Ramalho'


class Bicho:
    '''
    O equivalente do "method_missing  implementado no python utilizando o
    metodo __getattr__ que é invocado quando um atributo é acessado.

    Exemplo a implementação:

    >>> b = Bicho()
    >>> b.latir(3)
    'au! au! au!'

Estes metodos não existem.
    >>> b.piar()
    'piu!'
    >>> print(b.piar(3))
    piu! piu! piu!
    >>> print(b.urrar())
    urru!
    >>> print(b.urrar(2))
    urru! urru!
    '''

    def latir(self, vezes=1):
        return ('au! ' * vezes).strip()

    def fazer_barulho(self, som, vezes):
        barulho = (som + '!').replace('ar', 'u')
        return ((barulho + ' ') * vezes).strip()

    def __getattr__(self, item):
        def sonar(vezes=1):
            return self.fazer_barulho(item, vezes)

        return sonar

    @staticmethod
    def principal():
        b = Bicho()
        print(b.piar())
        print(b.urrar())
        print(b.cuachar())
        print(b.uivar())
        print(b.latir(3))


if __name__ == '__main__':
    import doctest

    doctest.testmod()
    Bicho.principal()
