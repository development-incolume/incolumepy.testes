from incolumepy.testes.OObj.polimorfismo.iMamifero import IMamifero


class Felino:
    pass


if __name__ == '__main__':
    f = Felino()

    print(isinstance(f, IMamifero))
    print(isinstance(f, Felino))
