from incolumepy.testes.OObj.polimorfismo.mamifero import Mamifero
from incolumepy.testes.OObj.polimorfismo.iMamifero import IMamifero


class Humano(Mamifero):
    def __init__(self, nome, peso, altura):
        super(Humano, self).__init__(nome, peso, altura)



if __name__ == '__main__':
    h = Humano('Ricardo', 84, 1.78)
    print(h)
    print(issubclass(type, IMamifero))
    print(issubclass(Humano, IMamifero))
