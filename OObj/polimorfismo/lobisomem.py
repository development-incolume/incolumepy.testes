from incolumepy.testes.OObj.polimorfismo.canino import Canino
from incolumepy.testes.OObj.polimorfismo.humano import Humano

class Lobisomem(Humano, Canino):
    def __init__(self, nome, peso, altura):
        super(Lobisomem, self).__init__(nome, peso, altura)




if __name__ == '__main__':
    l = Lobisomem
    print(l)
    print(l.latir())