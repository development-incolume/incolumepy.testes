from incolumepy.testes.OObj.polimorfismo.mamifero import Mamifero

class Canino(Mamifero):
    def __init__(self, nome, peso, altura, patas=4):
        super(Canino, self).__init__(nome, peso, altura)
        self.patas = patas

    def latir(self):
        return "Au, au!"

    def __str__(self):
        return 'Nome: %(nome)s; Peso: %(peso)3.2f; ' \
               'Altura: %(altura).2f; patas: %(patas)s' % self.__dict__



if __name__ == '__main__':
    c = Canino('rex', 3, .3)
    print(c)

    print(isinstance(c, Canino))
    print(isinstance(c, Mamifero))
