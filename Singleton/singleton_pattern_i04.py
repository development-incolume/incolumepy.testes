'''
Design Pattern Sigleton
implementação #4
'''


class Singleton(object):
    __instance = None

    def __new__(self, val):
        '''
        crie e entrega a mesma instancia para todas as requisições
        :param val:
        :return:
        '''
        if Singleton.__instance is None:
            Singleton.__instance = object.__new__(self)
        Singleton.__instance.val = val
        return Singleton.__instance


def test_0():
    a = Singleton('a')
    b = Singleton('b')
    assert (a is b) == True
    assert (id(a) == id(b)) == True


if __name__ == '__main__':
    pass
    a = Singleton('a')
    b = Singleton('b')
    print(id(a), id(b))
    print(a is b)
