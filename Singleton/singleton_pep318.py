def singleton(cls):
    instances = {}

    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]

    return getinstance


@singleton
class MyClass:
    pass


if __name__ == '__main__':
    a = MyClass()
    b = MyClass()

    print(id(a), id(b))
    print(a is b)
