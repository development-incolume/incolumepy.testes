'''
Design Pattern Sigleton
implementação #5
'''
class SingletonDecorator:
    def __init__(self, cls):
        self.cls = cls
        self.instance = None

    def __call__(self, *args, **kwds):
        if self.instance == None:
            self.instance = self.cls(*args, **kwds)
        return self.instance

@SingletonDecorator
class Singleton: pass

def test_0():
    a = Singleton('a')
    b = Singleton('b')
    assert (a is b) == True
    assert (id(a) == id(b)) == True


if __name__ == '__main__':
    x = Singleton()
    y = Singleton()
    z = Singleton()
    x.val = 'sausage'
    y.val = 'eggs'
    z.val = 'spam'
    print(x.val)
    print(y.val)
    print(z.val)
    print(x is y is z)