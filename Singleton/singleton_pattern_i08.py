def singletonDecorator(cls):
    instances = {}

    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]

    return getinstance


@singletonDecorator
class Singleton:
    value = None

    def __init__(self, value):
        self.value = value


if __name__ == '__main__':
    a = Singleton('')
    b = Singleton('')
    c = Singleton(1)

    print(a)
    print(b)
    print(c)
