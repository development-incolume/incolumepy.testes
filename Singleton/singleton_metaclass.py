class Singleton(type):
    def __init__(self, name, bases, dict):
        super(Singleton, self).__init__(name, bases, dict)
        self.instance = None

    def __call__(self, *args, **kw):
        if self.instance is None:
            self.instance = super(Singleton, self).__call__(*args, **kw)
        return self.instance


class MyClass(object, metaclass=Singleton):
    pass


if __name__ == '__main__':
    a = MyClass()
    b = MyClass()

    print(id(a), id(b))
    print(a is b)
