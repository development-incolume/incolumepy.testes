class Singleton:
    __shared_state = {}

    def __init__(self):
        self.__dict__ = self.__shared_state


if __name__ == '__main__':
    a = Singleton()
    b = Singleton()

    print(id(a), id(b))
    print(a is b)
