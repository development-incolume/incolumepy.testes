#https://stackoverflow.com/questions/14694482/converting-html-to-text-with-python

#!/usr/bin/env python
from urllib.request import urlopen
import html2text
from bs4 import BeautifulSoup
import requests
# !/usr/bin/env python
from urllib.request import urlopen

import html2text
import requests
from bs4 import BeautifulSoup


def sof01a(filename='http://example.com/page.html'):
    soup = BeautifulSoup(urlopen(filename).read())
    txt = soup.find('div', {'class' : 'body'})
    print(html2text.html2text(txt))


def sof01b(filename='http://example.com/page.html'):
    soup = BeautifulSoup(urlopen(filename))
    print(soup.get_text())

def sof02a(filename = "http://3.bp.blogspot.com/-osCDhqTKe0w/VVYFVTHJ_yI/AAAAAAAAFNc/qxswVVWU4i0/s400/DonRamon.jpg"):
    '''https://stackoverflow.com/questions/14114729/save-a-large-file-using-the-python-requests-library'''
    proxies = {
        "http":"http://10.1.101.101:8080",
        "https":"http://10.1.101.101:8080",
        "ftp":"http://10.1.101.101:8080",
                }
    response = requests.get(filename, proxies=proxies, stream=True)
    # Throw an error for bad status codes
    response.raise_for_status()
    with open('output.jpg', 'wb') as handle:
        for block in response.iter_content(1024):
            handle.write(block)


def mine01(url = 'http://example.com/page.html'):

    """https://stackoverflow.com/questions/8287628/proxies-with-python-requests-module"""
    proxies = {
        "http":"http://10.1.101.101:8080",
        "https":"http://10.1.101.101:8080",
        "ftp":"http://10.1.101.101:8080",
                }
    page = requests.get(url, proxies=proxies)
    print(type(page))
    soup = BeautifulSoup(page.text, 'lxml')
    print(type(soup))
    print(soup)
    print(soup.title)
    print(soup.title.text)

    print('-' * 20)
    print(soup.body.text.strip())

def mine02(url = 'http://www.planalto.gov.br/ccivil_03/_ato2015-2018/2018/decreto/D9288.htm',
           file1 = 'output1.html', file2 = 'output2.html'):
    proxies = {
        "http":"http://10.1.101.101:8080",
        "https":"http://10.1.101.101:8080",
        "ftp":"http://10.1.101.101:8080",
                }

    page = requests.get(url, proxies=proxies)
    print(page.headers)
    with open(file1, 'w') as output1:
        print(page.text)
        output1.write(page.text)

    soup = BeautifulSoup(page.content, 'lxml')
    with open(file2, 'w')as output2:
        #print(soup.prettify())
        output2.write(soup.prettify())

    return True




if __name__ == '__main__':
    #sof01a()
    #sof01b()
    #mine01()
    #mine02()
    sof02a()
    mine02()
