from bs4 import BeautifulSoup
from os.path import isfile, basename
from inspect import stack

def filename(input):
    count = 0
    while True:
        try:
            if count <= 0:
                filename = '../fxml/{}_{}.xml'.format(basename(input).split('.')[0], stack()[0][3], count)
            else:
                filename = '../fxml/{}_{}_{:0>2}.xml'.format(basename(input).split('.')[0], stack()[0][3], count)
            if isfile(filename):
                raise IOError('Arquivo existente: {}'.format(filename))
            break
        except IOError as e:
            print(e)
        except:
            raise
        finally:
            count += 1


def html2xml02a(filein='../fhtml/D9255.htm'):
    with open(filein) as file:
        content = file.read()
    soup = BeautifulSoup(content, 'html.parser')
    #print(soup, type(soup), soup.body.p)
    print(soup.body.p.img)
    print(soup.find_all('p', {'class': 'epigrafe'}))
    p_tag = soup.find_all('p')
    print(type(p_tag))
    print(len(p_tag))
    for i in p_tag:
        print('>>>>>', i)

    print(soup.find_all('p', {'class':'a'}))
    print(soup.body.p.img)

if __name__ == '__main__':
    html2xml02a()

