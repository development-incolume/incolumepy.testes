import urllib2


def webPageToText(url):
    response = urllib2.urlopen(url)
    html = response.read()
    text = stripTags(html).lower()
    return text



if __name__ == '__main__':
    #ImportError: No module named 'urllib2'
    webPageToText('http://brito.blog.incolume.com.br/')
