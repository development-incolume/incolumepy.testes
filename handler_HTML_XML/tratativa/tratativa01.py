from stripogram import html2text

strhtml = r'''
        <html>
            <body>
                <b>Project:</b> DeHTML<br>
                <b>Description</b>:<br>
                This small script is intended to allow conversion from HTML markup to
                plain text.
            </body>
        </html>'''

def f01():
    text = html2text(strhtml)
    print(text)


if __name__ == '__main__':
    #ImportError: No module named 'html2safehtml'
    f01()
