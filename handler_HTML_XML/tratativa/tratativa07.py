import lxml.html as lh
from lxml.html.clean import clean_html

str_html = '''
<html>
<header>
    <title> Lorem Ipsum </title>
    <meta name="description" content="Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,consectetur, adipisci velit...." />
</header>
<body>
<h1>Lorem Ipsum</h1>
<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,consectetur, adipisci velit</p>
<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,consectetur, adipisci velit</p>
<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,consectetur, adipisci velit</p>
<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,consectetur, adipisci velit</p>
<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,consectetur, adipisci velit</p>
<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,consectetur, adipisci velit</p>
</body>
</html>
'''

def lxml_to_text(html):
    doc = lh.fromstring(html)
    doc = clean_html(doc)
    return doc.text_content()

def f01(filehtml='../../../data/ipsum.html', filetxt='../../../data/ipsum.txt'):
    try:
        page = open(filehtml,encoding='utf-8').read()
        #print(page)
        doc = lh.fragment_fromstring(page)
        doc = clean_html(doc)
        with open(filetxt, 'w+') as file:
            file.write(doc)
        return True
    except Exception as e:
        print(e)
        raise
        return False
    pass

if __name__ == '__main__':
    print(lxml_to_text(str_html))
    f01()
    pass