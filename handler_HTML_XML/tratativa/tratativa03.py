import urllib.request
import urllib.response


def webPageToText(url):
    response = urllib.request.urlopen(url)
    html = urllib.response.read()
    text = urllib.request.stripTags(html).lower()
    return text



if __name__ == '__main__':
    #ImportError: No module named 'urllib2'
    webPageToText('http://brito.blog.incolume.com.br/')
