import requests
from bs4 import BeautifulSoup

url = 'http://brito.blog.incolume.com.br'

page = requests.get(url, proxies=None, timeout=10)
soup = BeautifulSoup(page.content, 'html.parser')
a = soup.find_all('a')

for link in a:
    print('{1}: {0}'.format(link.get('href'), link.get('text')))

print(type(a))
# print(soup.prettify())
# todo isto aqui
