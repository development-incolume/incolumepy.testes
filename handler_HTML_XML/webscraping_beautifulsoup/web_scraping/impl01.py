from collections import OrderedDict

import requests
from bs4 import BeautifulSoup


def i001(url='http://www.geonames.org/countries/', proxy=None):
    page = requests.get(url, proxies=proxy)
    print(page)
    print(page.url)
    print(page.request)
    print(page.content)
    print(page.text)
    print(page.apparent_encoding)
    print(page.headers)
    print(page.iter_content())


def i002(url='http://www.geonames.org/countries/', proxy=''):
    page = requests.get(url, proxies=proxy)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries')
    table1 = soup.find(id='countries').text
    table2 = soup.find(id='countries').get_text(separator='; ')
    # print(table, type(table))
    # print()
    # print(table1, type(table1))
    print('>>>>')
    print(table2, type(table2))


def i003(url='http://www.geonames.org/countries/', proxy=''):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries').get_text(separator='  ')
    # for line in table:
    #    print(line.strip())
    print(len(table))


def i004(url='http://www.geonames.org/countries/', proxy=''):
    page = requests.get(url, proxies=proxy)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries')
    print(type(table))
    print(table.children)
    print()
    for item in table.children:
        print(type(item))
        print(item)
        print('-' * 20)


def i005(url='http://www.geonames.org/countries/', proxy=''):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    print(type(soup))
    print(soup.get('html.body'))
    table0 = soup.find_all(id="countries")[0]
    table1 = soup.find(id='countries')
    print(type(table0), type(table1))


def i006(url='http://www.geonames.org/countries/', proxy=''):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries')
    # pprint(table)
    for line in table:
        print(line)


def i007(url='http://www.geonames.org/countries/', proxy=''):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries')

    for line in table:
        for elem in line:
            print(elem)


def i008(url='http://www.geonames.org/countries/', proxies=''):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries')
    result = []
    for line in table:
        if isinstance(line, str):
            continue
        for elem in line:
            print(elem.get_text(' '))


def i009(url='http://www.geonames.org/countries/', proxies=''):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries')
    result = []
    header = None
    for line in table:
        if isinstance(line, str):
            continue
        if not header:
            header = [cell.get_text(' ') for cell in line]
        result.append([cell.get_text(' ') for cell in line])
    print(header, result)


def i010(url='http://www.geonames.org/countries/', proxies=''):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries')
    result = []
    header = None
    for line in table:
        if isinstance(line, str):
            continue
        if not header:
            header = [cell.get_text(' ') for cell in line]
            continue
        result.append([cell.get_text(' ') for cell in line])
    print(header, result)


def i011(url='http://www.geonames.org/countries/', proxies=''):
    '''fail'''
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries')
    result = []
    header = None
    for line in table:
        if isinstance(line, str):
            continue
        if not header:
            header = [cell.get_text(' ') for cell in line]
            continue
        result.append([cell.get_text(' ') for cell in line])
    result = OrderedDict(pair for pair in zip(header, result))
    print(header, result)


def i012(url='http://www.geonames.org/countries/', proxies=''):
    '''fail'''
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries')
    result = []
    header = None
    for line in table:
        if isinstance(line, str):
            continue
        if not header:
            header = [cell.get_text(' ') for cell in line]
            continue
        result.append([cell.get_text(' ') for cell in line])
    result = OrderedDict(pair for pair in zip(header, result))
    print(header, result)


def i013(url='http://www.geonames.org/countries/', proxies=''):
    page = requests.get(url, proxies=proxies)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries')
    result = []
    header = None
    for line in table:
        if isinstance(line, str):
            continue
        if not header:
            header = [cell.get_text(' ') for cell in line]
            continue
        tupla = [cell.get_text(' ') for cell in line]
        result.append({x: y for x, y in zip(header, tupla)})
    print(result)
    return result


i013()
for i in i013():
    print(i)
