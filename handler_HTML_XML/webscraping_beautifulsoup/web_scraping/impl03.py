def importa(url='http://www.geonames.org/countries/', tmout=2):
    '''https://pt.stackoverflow.com/questions/225691/web-scraping-converter-tabela-html-em-dict-python/225818#225818'''
    import requests
    from bs4 import BeautifulSoup

    page = requests.get(url=url, timeout=tmout)
    soup = BeautifulSoup(page.content, 'html.parser')
    # print('\nsoup >>>', soup)
    table = soup.find(id="countries")

    result = []

    headers = None
    for i, row in enumerate(table):
        # print(type(row))
        # input()
        # Pule as linhas que não contém tags html
        if isinstance(row, str):
            continue
        # Assume que a primeira linha com conteúdo são os cabeçalhos
        if not headers:
            # cria uma lista com o conteúdo de texto de cada tag na linha:
            headers = [cell.get_text(' ') for cell in row]
            continue

        row_contents = [cell.get_text() for cell in row]
        # data_dict = OrderedDict(pair for pair in zip(headers, row_contents))
        # data_dict = defaultdict(None, {x: y for x, y in zip(headers, row_contents)})
        data_dict = {x: y for x, y in zip(headers, row_contents)}
        result.append(data_dict)

        # print(i, row)
        print(headers)
        # print(row_contents)
        # print(data_dict)

    return result


from pprint import pprint

# importa()
pprint('')
# pprint(importa())
for item in importa():
    print(item)
    pass
