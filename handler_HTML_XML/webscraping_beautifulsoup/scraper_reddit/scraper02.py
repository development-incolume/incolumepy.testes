import requests
from bs4 import BeautifulSoup


def get_links_from(subreddit):
    links = []
    r = requests.get('http://www.reddit.com/r/%s/' % (subreddit))
    if r.status_code != 200:
        return None

    soup = BeautifulSoup(r.content)

    for a in soup.findAll('a', attrs={'class': 'title '}):
        links.append((a.text, a.get('href')))
    return links


for link_name, link_url in get_links_from('programming'):
    print("%s - %s" % (link_name, link_url))

for link_name, link_url in get_links_from('python'):
    print("%s - %s" % (link_name, link_url))
