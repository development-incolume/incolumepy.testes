import requests
from bs4 import BeautifulSoup

r = requests.get('http://www.reddit.com/r/programming/')
soup = BeautifulSoup(r.content, 'html.parser')

print(soup)
for a in soup.findAll('a', attrs={'class': 'title '}):
    print("%s - %s" % (a.text, a.get('href')))
