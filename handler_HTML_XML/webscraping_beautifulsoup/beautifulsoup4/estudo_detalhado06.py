import inspect
import os

from bs4 import BeautifulSoup

__author__ = '@britodfbr'
'''https://www.crummy.com/software/BeautifulSoup/bs4/doc/'''

data = '''<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />
    <meta name='author' content='@britodfbr' />
    <meta name='date' content='20180311' />
    <meta name='timestamp' content='Dom Mar 11 10:12:17 -03 2018' />
    <meta name='epoch' content = '1520773937' />
   <title>
    Dormouse
   </title>
  </head>
  <body>
   <p class="title">
    <b>
     The Dormouse's story
    </b>
   </p>
   <p class="story">
    Once upon a time there were three little sisters; and their names were
    <a class="sister" href="http://example.com/elsie" id="link1"> Elsie </a>,
    <a class="sister" href="http://example.com/lacie" id="link2"> Lacie </a> and
    <a class="sister" href="http://example.com/tillie" id="link3"> Tillie </a>; 
    and they lived at the bottom of a well.
   </p>
   <p class="story">
    ...
   </p>
   <p style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:1.0cm"><b><span style="color: black">O PRESIDENTE DA
REPÚBLICA</span></b><span style="color: black">, no uso da atribuição que lhe
confere o art. 84, <b>caput</b>, inciso IV, da Constituição, e tendo em vista o
disposto no art. 2<s>º</s> da Lei n<s>º</s> 13.152, de 29 de julho de 2015,</span></p>
<p style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:1.0cm"><span style="color: black">&nbsp;</span><b><span style="color: black">DECRETA</span></b><span style="color: black">:</span></p>
<p style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:1.0cm"><span style="color: black">&nbsp;<a name="art1"></a>Art. 1<s>º</s>&nbsp; A partir
de 1<s>º</s> de janeiro de 2018, o salário mínimo será de R$ 954,00 (novecentos
e cinquenta e quatro reais).</span></p>
<p style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:1.0cm"><span style="color: black">&nbsp;Parágrafo único.&nbsp; Em
virtude do disposto no <b>caput</b>, o valor diário do salário mínimo
corresponderá a R$ 31,80 (trinta e um reais e oitenta centavos) e o valor
horário, a R$ 4,34 (quatro reais e trinta e quatro centavos).</span></p>
<p style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:1.0cm"><span style="color: black">&nbsp;<a name="art2"></a>Art. 2<s>º</s>&nbsp; Este
Decreto entra em vigor em 1<s>º</s> de janeiro de 2018.</span></p>
<p style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:1.0cm"><span style="color: black">&nbsp;Brasília, 29 de dezembro
de 2017; 196<s>º</s> da Independência e 129<s>º</s> da República.</span></p>
   <footer>
   
   </footer>
  </body>
 </html>'''


def estudo_detalhado06a(filename=None, pathname='../../fhtml'):
    count = 0
    os.makedirs(pathname, mode=0o755, exist_ok=True)
    soup = BeautifulSoup(data, 'html5lib')

    if not filename:
        filename = os.path.join(pathname, '{}_{}.xhtml'.format(inspect.stack()[0][3], soup.title.text.strip().lower()))
    while True:
        try:
            if os.path.exists(filename):
                filename = os.path.join(pathname, '{}_{}_{:0>2}.xhtml'.format(inspect.stack()[0][3],
                                                                              soup.title.text.strip().lower(), count))
                raise IOError('Arquivo existente: {}'.format(filename))
            else:
                break

        except IOError as e:
            print(e)
        except Exception:
            raise
        finally:
            count += 1

    # editando TAG
    print(soup.span)
    print(soup.span.name)
    del soup.span['style']
    soup.span.name = 'b'
    print(soup)

    with open(filename, 'w') as file:
        file.write(soup.prettify())
        pass


if __name__ == '__main__':
    estudo_detalhado06a()
