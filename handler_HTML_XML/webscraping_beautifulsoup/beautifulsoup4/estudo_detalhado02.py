__author__ = '@britodfbr'
'''https://www.crummy.com/software/BeautifulSoup/bs4/doc/'''

from bs4 import BeautifulSoup

soup = BeautifulSoup(open('../../fhtml/D9255.htm'), 'html.parser')

print(soup.title)
print(soup.title.name)
print(soup.title.string)
print(soup.title.parent.name)  # possui um erro no html tag meta não fechada
print(soup.p)
print(soup.p['align'])
print(soup.p['style'])
print(soup.a)
print(soup.a['href'])
print('>>>', soup.a.text.strip())

print(soup.find_all('a'))
print('*' * 50)
print(soup.find(id="view"))
