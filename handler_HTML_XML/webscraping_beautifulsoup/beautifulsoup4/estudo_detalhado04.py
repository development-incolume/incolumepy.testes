import os

from bs4 import BeautifulSoup

__author__ = '@britodfbr'
'''https://www.crummy.com/software/BeautifulSoup/bs4/doc/'''

data = '''<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />
    <meta name='author' content='@britodfbr' />
    <meta name='date' content='20180311' />
    <meta name='timestamp' content='Dom Mar 11 10:12:17 -03 2018' />
    <meta name='epoch' content = '1520773937' />
   <title>
    Dormouse
   </title>
  </head>
  <body>
   <p class="title">
    <b>
     The Dormouse's story
    </b>
   </p>
   <p class="story">
    Once upon a time there were three little sisters; and their names were
    <a class="sister" href="http://example.com/elsie" id="link1"> Elsie </a>,
    <a class="sister" href="http://example.com/lacie" id="link2"> Lacie </a> and
    <a class="sister" href="http://example.com/tillie" id="link3"> Tillie </a>; 
    and they lived at the bottom of a well.
   </p>
   <p class="story">
    ...
   </p>
   <footer>
   
   </footer>
  </body>
 </html>'''


def estudo_detalhado04a(filename=None, pathname='../../fhtml'):
    count = 0
    os.makedirs(pathname, mode=0o755, exist_ok=True)
    soup = BeautifulSoup(data, 'html5lib')
    # print(soup)
    # print(soup.get_text)
    # print(soup.title.text)
    # print(soup.name)
    # print(soup.contents)
    if not filename:
        filename = os.path.join(pathname, '{}.xhtml'.format(soup.title.text.strip().lower()))
    while True:
        try:
            if os.path.exists(filename):
                filename = os.path.join(pathname, '{}_{:0>2}.xhtml'.format(soup.title.text.strip().lower(), count))
                raise IOError('Arquivo existente: {}'.format(filename))
            else:
                break

        except IOError as e:
            print(e)
        except Exception:
            raise
        finally:
            count += 1

        #           print(filename)
    with open(filename, 'w') as file:
        file.write(soup.prettify())
        pass


if __name__ == '__main__':
    estudo_detalhado04a()
