import requests
from bs4 import BeautifulSoup


def getUrl(url):
    r = requests.get("http://" + url)
    data = r.text
    soup = BeautifulSoup(data, 'lxml')

    for link in soup.find_all('a'):
        print(link.get('href'))


def main():
    url = input("Enter a website to extract the URL's from: ")
    getUrl(url)


if __name__ == '__main__':
    # getUrl('brito.blog.incolume.com.br')
    getUrl('www.pythonforbeginners.com')
    # main()
