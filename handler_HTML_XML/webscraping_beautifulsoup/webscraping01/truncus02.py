'''https://pt.stackoverflow.com/questions/42667/decodificar-entidades-html-num-string-em-python?rq=1'''

import html
import inspect

def truncus02a(s='orienta&ccedil;&atilde;o-a-objetos'):
    result = html.unescape(s)
    print('{}: {}'.format(inspect.stack()[0][3], result))


def truncus02b(s='orienta&ccedil;&atilde;o-a-objetos'):
    #PARA PYTHON2
    import html.parser
    h = html.parser.HTMLParser()
    print('{}: {}'.format(inspect.stack()[0][3], h.unescape(s)))



def truncus02c(s='orienta&ccedil;&atilde;o-a-objetos'):
    from bs4 import BeautifulSoup
    t = BeautifulSoup(s, 'html.parser')
    print('{}: {}'.format(inspect.stack()[0][3], t.get_text()))
    pass


def truncus02d(s='orienta&ccedil;&atilde;o-a-objetos'):
    from bs4 import BeautifulSoup
    t = BeautifulSoup(s, 'lxml')
    print('{}: {}'.format(inspect.stack()[0][3], t.get_text()))


def truncus02e(s='orienta&ccedil;&atilde;o-a-objetos'):
    from bs4 import BeautifulSoup
    t = BeautifulSoup(s, 'html5lib')
    print('{}: {}'.format(inspect.stack()[0][3], t.get_text()))


def truncus02f(s='orienta&ccedil;&atilde;o-a-objetos'):
    from bs4 import BeautifulSoup
    t = BeautifulSoup(s, 'xml')
    print('{}: {}'.format(inspect.stack()[0][3], t.get_text()))


def truncus02g(s='orienta&ccedil;&atilde;o-a-objetos'):
    from bs4 import BeautifulSoup
    t = BeautifulSoup(s, 'lxml-xml')
    print('{}: {}'.format(inspect.stack()[0][3], t.get_text()))


if __name__ == '__main__':
    truncus02a()
    truncus02b()
    truncus02c()
    truncus02d()
    truncus02e()
    truncus02f()
    truncus02g()