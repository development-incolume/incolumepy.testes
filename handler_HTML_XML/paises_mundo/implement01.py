# http://www.geonames.org/countries/
import csv
from pprint import pprint

import requests
from bs4 import BeautifulSoup


def impl01():
    pass


def impl02(url='http://www.geonames.org/countries/', tout=2):
    page = requests.get(url=url, timeout=tout)
    soup = BeautifulSoup(page.text, 'html.parser')
    print(soup.get_text())


def impl03(url='http://www.geonames.org/countries/', tout=2):
    page = requests.get(url)
    pprint(page.content)


def impl04(url='http://www.geonames.org/countries/', tout=2):
    page = requests.get(url, timeout=tout)
    soup = BeautifulSoup(page.content, 'html.parser')
    print(soup.prettify())


def impl05(url='http://www.geonames.org/countries/', tout=2):
    page = requests.get(url, timeout=tout)
    soup = BeautifulSoup(page.content, 'html.parser')
    print(soup.get_text)


def impl06(url='http://www.geonames.org/countries/', tout=2):
    page = requests.get(url, timeout=tout)
    soup = BeautifulSoup(page.text, 'html.parser')
    print(soup.text, sep=';')


def impl07(url='http://www.geonames.org/countries/', tmout=2):
    page = requests.get(url, timeout=tmout)
    # print(page)
    soup = BeautifulSoup(page.text, 'html.parser')
    # print(soup)
    for item in soup.find_all('tr'):
        print(item)


def impl08(url='http://www.geonames.org/countries/', tmout=2):
    page = requests.get(url=url, timeout=tmout)
    # print(page)
    soup = BeautifulSoup(page.text, 'html.parser')
    print(soup)


def impl08(url='http://www.geonames.org/countries/', tmout=2):
    page = requests.get(url=url, timeout=tmout)
    soup = BeautifulSoup(page.text, 'html.parser')
    print(soup.find_all(id="countries"))


def impl09(url='http://www.geonames.org/countries/', tmout=2):
    page = requests.get(url=url, timeout=tmout)
    soup = BeautifulSoup(page.text, 'html.parser')
    print(soup.children)


def impl10(url='http://www.geonames.org/countries/', tmout=2):
    page = requests.get(url=url, timeout=tmout)
    soup = BeautifulSoup(page.text, 'html.parser')
    print(list(soup.children))
    print(len(list(soup.children)))


def impl11(url='http://www.geonames.org/countries/', tmout=2):
    page = requests.get(url=url, timeout=tmout)
    soup = BeautifulSoup(page.text, 'html.parser')
    table = soup.find_all(id="countries")
    print(table)


def impl12(url='http://www.geonames.org/countries/', tmout=2):
    page = requests.get(url, timeout=tmout)
    soup = BeautifulSoup(page.content, 'html.parser')
    print(type(soup.find_all(id='countries')[0]))
    print(soup.find_all(id="countries")[0].get_text(separator='; '))


def impl12(url='http://www.geonames.org/countries/', tmout=2):
    page = requests.get(url=url, timeout=tmout)
    soup = BeautifulSoup(page.content, 'html.parser')
    # print('\nsoup >>>', soup)
    data = soup.find_all(id="countries")[0].get_text(separator='; ')

    # print('\ndata >>>', data, type(data))
    data = data.split('; ')

    content = csv.DictReader(data)
    for linha in content:
        pass
        print(linha)


if __name__ == '__main__':
    impl12()
