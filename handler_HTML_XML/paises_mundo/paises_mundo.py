import json
import re

import requests
from bs4 import BeautifulSoup


def paises_lst(url='http://www.geonames.org/countries/', proxies=None):
    page = requests.get(url=url, proxies=proxies)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries')
    header = None
    result = []
    for linha in table:
        if isinstance(linha, str):
            continue
        if not header:
            header = [campo.get_text(' ') for campo in linha]
            continue
        tupla = [campo.get_text(' ') for campo in linha]
        result.append({x: y for x, y in zip(header, tupla)})
    print(result)
    return result


def paises_str_json(url='http://www.geonames.org/countries/', proxies=None):
    page = requests.get(url=url, proxies=proxies)
    soap = BeautifulSoup(page.content, 'html.parser')
    header = soap.find('h3').get_text()
    table = soap.find(id='countries')
    headers = None
    result = {}
    for linha in table:
        if isinstance(linha, str):
            continue
        if not headers:
            headers = [campo.get_text(' ') for campo in linha]
            continue
        tupla = [campo.get_text(' ') for campo in linha]
        row = {x: y for x, y in zip(headers, tupla)}
        result[row['ISO-3166 alpha2']] = row
        result[row['ISO-3166 alpha3']] = row
        result[row['ISO-3166 numeric']] = row


        # print(row)
    result["_comment"] = "get at {}".format(page.url)
    print(result)
    return (result)


def paises_json0(url='http://www.geonames.org/countries/', proxies=None):
    page = requests.get(url=url, proxies=proxies)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries')
    result = {}
    filename = '{}.json'.format(soup.find('h3').get_text().replace(' ', ''))
    filename = '{}.json'.format(*(re.findall('\w+', soup.find('h3').get_text())))
    filename = '{}.json'.format(''.join(re.findall('\w+', soup.find('h3').get_text())))
    headers = None
    for linha in table:
        if isinstance(linha, str):
            continue
        if not headers:
            headers = [campo.get_text(' ') for campo in linha]
            continue
        tupla = [campo.get_text(' ') for campo in linha]
        row = {x: y for x, y in zip(headers, tupla)}
        result[row['ISO-3166 numeric']] = row

    result["_comment"] = "get at {}".format(page.url)

    for i in result.items():
        print(i)

    with open(filename, 'w') as fileout:
        fileout.write(json.dumps(result))
        json.dump(result, fileout, sort_keys=True, indent=4, ensure_ascii=False)

    return True


def paises_json1(filename='paises.json', url='http://www.geonames.org/countries/', proxies=None):
    result = paises_str_json(url=url, proxies=proxies)
    with open(filename, 'w') as fileout:
        fileout.write(json.dumps(result))
        json.dump(result, fileout, sort_keys=True, indent=4, ensure_ascii=False)
    return True


def paises_json(filename='paises.json', url='http://www.geonames.org/countries/', proxies=None):
    page = requests.get(url=url, proxies=proxies)
    soup = BeautifulSoup(page.content, 'html.parser')
    title = soup.find('title').get_text()
    result = None
    table = soup.find(id='countries')

    for line in table:
        print(line)

    # print(table)
    return result


# paises_json()
# #print(type(paises_json()), paises_json())
# pprint(paises_json())
print(paises_json())
