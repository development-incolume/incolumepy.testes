from __future__ import print_function
from lxml import etree, html
from io import StringIO
from os.path import basename, isfile
from inspect import stack

def caterva01(filename='../fhtml/D9255.htm', encoding='utf-8'):
    try:
        with open(filename, encoding=encoding) as file:
            content = file.read()

        parser = html.HTMLParser()
        print('parser: ',parser)
        tree = html.parse(StringIO(content), parser)
        #print('tree: ', tree)

        for element in tree.xpath('/html/body/font/p'):
            print('>>>>>{}\n'.format(element.text_content().strip()))
        #epigrafe = tree.xpath('/html/body/blockquote/p/font/small/strong/a/font')
        epigrafe = tree.xpath('//p[@class="ementa"]//*')
        print(type(epigrafe))
        print(len(epigrafe))
        for i in epigrafe:
            print(i.text_content())

        tei = etree.Element('TEI', xmlns='http://www.tei-c.org/ns/1.0')
        tei_header = etree.SubElement(tei, 'teiHeader')
        filedesc = etree.SubElement(tei_header, 'fileDesc')
        file_titlestmt = etree.SubElement(filedesc, 'titleStmt')
        titlemain = etree.SubElement(file_titlestmt, 'title', type='main')

        titlemain.text = tree.find('//head/title').text_content()
        fxml = '../fxml/{}_{}.xml'.format(basename(filename.split('.')[-2]), stack()[0][3])
        count = 0
        while True:
            try:
                if count <= 0:
                    fxml = '../fxml/{}_{}.xml'.format(basename(filename.split('.')[-2]), stack()[0][3])
                else:
                    fxml = '../fxml/{}_{}_{:0>2}.xml'.format(basename(filename.split('.')[-2]), stack()[0][3], count)
                if isfile(fxml): raise IOError('Arquivo existente: {}'.format(fxml))
                break
            except IOError as e:
                print(e)
            except:
                raise
            finally:
                count += 1

        print(etree.tostring(tree, pretty_print=True, xml_declaration=True, encoding='UTF-8'))
        with open(fxml, 'wb') as file:
            file.write(etree.tostring(tree, pretty_print=True, xml_declaration=True, encoding='UTF-8'))

    except IOError as e:
        print('ERROR:{}'.format(e))
        #raise
    except etree.XMLSyntaxError as e:
        print('Error: parser {}'.format(e))
    except etree.XPathEvalError as e:
        print('ERROR: XPath expression', e)
    except:
        raise
    #print(content)



if __name__ == '__main__':
    caterva01()