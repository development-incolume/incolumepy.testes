import scrapy

'''http://pythonclub.com.br/material-do-tutorial-web-scraping-na-nuvem.html'''


def first(sel, xpath):
    return sel.xpath(xpath).extract_first()


class YoutubeChannelLister(scrapy.Spider):
    '''
    scrapy runspider youtube_spider.py -o portadosfundos.csv
    scrapy runspider youtube_spider.py -o portadosfundos.json
    scrapy runspider youtube_spider.py -o portadosfundos.jl
    '''

    name = 'channel-lister'
    youtube_channel = 'portadosfundos'
    start_urls = ['https://www.youtube.com/user/%s/videos' % youtube_channel]

    def parse(self, response):
        for sel in response.css("ul#channels-browse-content-grid > li"):
            yield {
                'link': response.urljoin(first(sel, './/h3/a/@href')),
                'title': first(sel, './/h3/a/text()'),
                'views': first(sel, ".//ul/li[1]/text()"),
            }
