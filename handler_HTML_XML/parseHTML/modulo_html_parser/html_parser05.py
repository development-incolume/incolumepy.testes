import os

os.environ['PYTHONPATH'] = '{}:{}'.format(os.environ.get('PYTHONPATH'),os.path.abspath('.'))
os.environ['PYTHONPATH'] = '{}:{}'.format(os.environ.get('PYTHONPATH'), os.path.abspath('../modulo_html_parser'))

from html_parser02 import MyHTMLParser
from modulo_html_parser import html_parser01


if __name__ == '__main__':
    parser = MyHTMLParser()
    parser.feed('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN http://www.w3.org/TR/html4/strict.dtd">')

    parser.feed('<img src="python-logo.png" alt="The Python logo">')

    parser.feed('<h1>Python</h1>')
    parser.feed('<style type="text/css">#python { color: green }</style>')

    parser.feed('<script type="text/javascript">'
                'alert("<strong>hello!</strong>");</script>')

    parser.feed('<!-- a comment -->'
                '<!--[if IE 9]>IE-specific content<![endif]-->')

    parser.feed('&gt;&#62;&#x3E;')

    for chunk in ['<sp', 'an>buff', 'ered ', 'text</s', 'pan>']:
        parser.feed(chunk)

    parser.feed('<p><a class=link href=#main>tag soup</p ></a>')

    aa = html_parser01.MyHTMLParser()
    aa.feed('<html><head><title>Test</title></head>'
                '<body><h1>Parse me!</h1></body></html>')
