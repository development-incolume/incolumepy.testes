from html.parser import HTMLParser
from html.entities import name2codepoint

class MyHTMLParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        print("Start tag:", tag)
        for attr in attrs:
            print("     attr:", attr)

    def handle_endtag(self, tag):
        print("End tag  :", tag)

    def handle_data(self, data):
        print("Data     :", data)

    def handle_comment(self, data):
        print("Comment  :", data)

    def handle_entityref(self, name):
        c = chr(name2codepoint[name])
        print("Named ent:", c)

    def handle_charref(self, name):
        if name.startswith('x'):
            c = chr(int(name[1:], 16))
        else:
            c = chr(int(name))
        print("Num ent  :", c)

    def handle_decl(self, data):
        print("Decl     :", data)

if __name__ == '__main__':
    parser = MyHTMLParser()
    parser.feed('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN http://www.w3.org/TR/html4/strict.dtd">')

    parser.feed('<img src="python-logo.png" alt="The Python logo">')

    parser.feed('<h1>Python</h1>')
    parser.feed('<style type="text/css">#python { color: green }</style>')

    parser.feed('<script type="text/javascript">'
                'alert("<strong>hello!</strong>");</script>')

    parser.feed('<!-- a comment -->'
                '<!--[if IE 9]>IE-specific content<![endif]-->')

    parser.feed('&gt;&#62;&#x3E;')

    for chunk in ['<sp', 'an>buff', 'ered ', 'text</s', 'pan>']:
        parser.feed(chunk)

    parser.feed('<p><a class=link href=#main>tag soup</p ></a>')