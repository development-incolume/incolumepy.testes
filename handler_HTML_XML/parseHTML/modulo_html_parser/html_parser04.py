import sys
import os

sys.path.append('.')
os.environ['PYTHONPATH'] = '{}:{}'.format(os.environ.get('PYTHONPATH'),os.path.abspath('.'))

from html_parser02 import MyHTMLParser


if __name__ == '__main__':
    parser = MyHTMLParser()
    parser.feed('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN http://www.w3.org/TR/html4/strict.dtd">')

    parser.feed('<img src="python-logo.png" alt="The Python logo">')

    parser.feed('<h1>Python</h1>')
    parser.feed('<style type="text/css">#python { color: green }</style>')

    parser.feed('<script type="text/javascript">'
                'alert("<strong>hello!</strong>");</script>')

    parser.feed('<!-- a comment -->'
                '<!--[if IE 9]>IE-specific content<![endif]-->')

    parser.feed('&gt;&#62;&#x3E;')

    for chunk in ['<sp', 'an>buff', 'ered ', 'text</s', 'pan>']:
        parser.feed(chunk)

    parser.feed('<p><a class=link href=#main>tag soup</p ></a>')
    print(os.environ['PYTHONPATH'])
    print(os.environ.get('PYTHONPATH'))
    print(sys.path)
    print(os.path.abspath('.'))
    os.getenv('PYTHONPATH','{}:{}'.format(os.environ.get('PYTHONPATH'),os.path.abspath('.')))
    print(os.environ.get('PYTHONPATH'))

    for i, item in os.environ.items():
        print(i, item)

    os.environ['PYTHONPATH'] = '{}:{}'.format(os.environ.get('PYTHONPATH'),os.path.abspath('.'))
    print(os.environ.get('PYTHONPATH'))