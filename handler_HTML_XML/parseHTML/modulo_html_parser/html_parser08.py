from html.entities import name2codepoint
from html.parser import HTMLParser


class MyHTMLParser(HTMLParser):
    result = ''

    def handle_starttag(self, tag, attrs):
        print("Start tag:", tag)
        self.result += 'start tag: {}\n'.format(tag)
        for attr in attrs:
            print("     attr:", attr)
            self.result += '     attr: {}\n'.format(attr)

    def handle_endtag(self, tag):
        print("End tag  :", tag)
        self.result += 'End tag: {}\n'.format(tag)

    def handle_data(self, data):
        print("Data     :", data)
        self.result += "Data     : {}\n".format(data)

    def handle_comment(self, data):
        print("Comment  :", data)
        self.result += "Comment  : {}\n".format(data)

    def handle_entityref(self, name):
        c = chr(name2codepoint[name])
        print("Named ent:", c)
        self.result += "Named ent : {}\n".format(c)

    def handle_charref(self, name):
        if name.startswith('x'):
            c = chr(int(name[1:], 16))
        else:
            c = chr(int(name))
        print("Num ent  :", c)
        self.result += "Named ent : {}\n".format(c)

    def handle_decl(self, data):
        print("Decl     :", data)
        self.result += "Decl     : {}\n".format(data)


if __name__ == '__main__':
    parser = MyHTMLParser()
    parser.feed('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN http://www.w3.org/TR/html4/strict.dtd">')

    parser.feed('<img src="python-logo.png" alt="The Python logo">')

    parser.feed('<h1>Python</h1>')
    parser.feed('<style type="text/css">#python { color: green }</style>')

    parser.feed('<script type="text/javascript">'
                'alert("<strong>hello!</strong>");</script>')

    parser.feed('<!-- a comment -->'
                '<!--[if IE 9]>IE-specific content<![endif]-->')

    parser.feed('&gt;&#62;&#x3E;')

    for chunk in ['<sp', 'an>buff', 'ered ', 'text</s', 'pan>']:
        parser.feed(chunk)

    parser.feed('<p><a class=link href=#main>tag soup</p ></a>')
