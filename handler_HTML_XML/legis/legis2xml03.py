__author__ = '@britodfbr'
import inspect
import os
import platform
import re
from os.path import join
from pprint import pprint

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

if platform.system() == 'Windows':
    chromebin = r'C:\Users\ricardobn\AppData\Local\Programs\Python\Python36-32\selenium\webdriver\chromedriver.exe'
    firefoxbin = r'C:\Users\ricardobn\AppData\Local\Programs\Python\Python36-32\selenium\webdriver\geckodriver.exe'
    encode = 'iso8859-1'
elif platform.system() == 'Linux':
    chromebin = '../../../drivers/chromedriver'
    firefoxbin = '../../../drivers/geckodriver'
    encode = 'utf-8'
else:
    raise OSError('Sistema operacional não identificado.')


def realfilename(filebase, extout='xml', basedir=''):
    count = 0

    while True:
        try:
            if basedir:
                os.makedirs(basedir, mode=0o755, exist_ok=True)

            if count <= 0:
                filename = join(basedir, '{}_{}.{}'.format(filebase, inspect.stack()[0][3], extout))
            else:
                filename = join(basedir, '{}_{}{:0>2}.{}'.format(filebase, inspect.stack()[0][3], count, extout))
            if os.path.isfile(filename): raise IOError('Arquivo existente: {}'.format(filename))

            return filename
        except FileExistsError:
            print('Path *{}* não pode ser utilizado.'.format(basedir))
            break
        except IOError as e:
            print(e)
        except:
            raise
        finally:
            count += 1


def gen_url(filein='../data/links_decretos_normativos.txt'):
    try:
        with open(filein) as f:
            for linha in f:
                yield linha
    except FileNotFoundError as e:
        print('filein informado *{}* inválido'.format(e.filename))


def define_basename(url):
    result = None
    print(url)
    if re.match(r'.*planalto.*', url):
        #        print('planalto')
        result = (url.split('/')[-1]).split('.')[0]
    elif re.match(r'.*senado.*', url):
        #        print('senado')
        d = []
        for i in url.split('?')[-1].split('&'):
            d.append(i.split('='))
        d = dict(d)
        print(d)
        result = '{}{:0>5}'.format(d['tipo_norma'], d['numero'])

    return result


def legis2xml02a(url):
    print(url)
    data = requests.get(url, verify=False)
    print(data.content)
    soup = BeautifulSoup(data.content, 'html5lib')
    print(soup)


def legis2xml02b(url):
    print(url)
    chrome = webdriver.Chrome(chromebin)
    chrome.get(url)
    print(chrome.title)
    # print(chrome.page_source)
    print(chrome.current_url)
    print(chrome.application_cache)
    print(chrome.desired_capabilities)
    print(chrome.name)
    print(chrome.current_window_handle)


def legis2xml02c(url):
    header = '''<!DOCTYPE html>
        <html lang="pt-br"  xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset={}"/>
        <link rel="stylesheet" href="../view/legis_3.css">
        <title> {} </title>
        </head>
        <body>
        <header>
        <h1>Presidência da República</h1>
        <h2>Casa Civil</h2>
        <h3>Subchefia para Assuntos Jurídicos</h3>
        </header>'''
    footer = '<p class="dou">{}</p></body></html>'
    title = conteudo = message = ''
    filebase = define_basename(url)
    chrome = webdriver.Chrome(chromebin)
    chrome.get(url)
    if re.match(r'.*senado.*', chrome.current_url):
        print('>>>> senado <<<<')
        filebase = define_basename(url)
        chrome.get(chrome.find_element_by_partial_link_text('Publicação Original').get_attribute('href'))
        page = re.sub(r' style="[^"]+"', '', chrome.page_source)
        page = re.sub(r'<span>|</span>', '', page)
        page = re.sub(r'EpgrafeAlt1|Epgrafe', 'epigrafe', page)
        page = re.sub(r'EmentaAlt2|Ementa', 'ementa', page)
        page = re.sub(r'Assinatura1Alt7|Assinatura1', 'presidente', page)
        page = re.sub(r'Assinatura2Alt8|Assinatura2', 'ministro', page)
        soup = BeautifulSoup(page, 'html.parser').find_all(id='conteudoPrincipal')
        container = soup[0].div.find_all('div')
        conteudo = container[2]
        message = container[1].text
        title = ''
        # print(conteudo, message)
    elif re.match(r'.*planalto.*', chrome.current_url):
        print('>>>> planalto <<<<')
        void = []
        void.append(r'<big>|</big>')
        void.append(r'<small>|</small>')
        void.append(r'<strong>|</strong>')
        void.append(r' ?color="#\d+"')
        void.append(r' size="2"')
        void.append(r' face="Arial"')
        void.append(r' ?text-align: justify;?')
        void.append(r' ?text-indent: 35px;?')
        void.append(r' ?align="justify"')
        void.append(r' ?margin-\w+: 0;?')
        void.append(r' ?class="MsoNormal"')
        void.append(r' ?line-height: normal;?')
        void.append(r' ?text-align: center;?')
        void.append(r' ?align="\w+"')
        void.append(r' ?color="#FF0000"')
        void.append(r'font-family: Arial;? ')
        void.append(r' ?class="[pt]\d+"')
        #        print(chrome.page_source)
        page = re.sub('|'.join(void), '', chrome.page_source)
        page = re.sub(' style=""', '', page)
        page = re.sub('<span>|</span>', '', page)
        page = re.sub('<font>|</font>', '', page)
        page = re.sub('<i>|</i>', '', page)
        soup = BeautifulSoup(page, 'html5lib')
        print(soup.find_all('p'))
        conteudo = soup.find_all('p')[0]

    # print(chrome.find_element('meta',{'http-equiv', 'Content-Type'}))
    # print(chrome.find_element('meta', 'name'))
    # result = header.format(chrome.find_elements_by_tag_name('meta'))
    # print(type(conteudo))
    result = header.format(encode, title) + str(conteudo) + footer.format(message)
    # print(result, filebase)
    with open(realfilename(filebase, extout='xhtml', basedir='../fxhtml')) as file:
        file.write(pprint(result))


if __name__ == '__main__':
    get_url = gen_url()
    print(realfilename('teste', basedir='L8112cons.htm'))
    for i in range(2):
        #        legis2xml02a(next(get_url))
        #        legis2xml02b(next(get_url))
        legis2xml02c(next(get_url))
