'''https://stackoverflow.com/questions/3115448/best-way-to-convert-the-this-html-file-into-an-xml-file-using-python'''
from lxml import html
from xml.etree import ElementTree

def html2xml01():
    doc = html.fromstring(open('L8112cons0.htm', encoding='iso-8859-1').read())
    out = open('a.xhtml', 'wb')
    out.write(ElementTree.tostring(doc))

def html2xml():
    doc = html.fromstring(open('L8112cons.htm', encoding='iso-8859-1').read())
    with open('a.xhtml', 'wb') as out:
        out.write(ElementTree.tostring(doc))

if __name__ == '__main__':
    html2xml()
