import os
import re
from bs4 import BeautifulSoup
from copy import copy

def limpar_constituicao():
    #print(os.path.abspath('../data/Constituicao.htm'))
    with open(os.path.abspath(r'../data/Constituicao.htm'), encoding='iso8859-1') as filein, open(
            os.path.abspath(r'../fxhtml/constituicao.xhtml'), 'wb') as fileout:
        conteudo = filein.read()
        conteudo = re.sub('\n', ' ', conteudo)
        conteudo = re.sub(r'<p<a', r'<a', conteudo)
        conteudo = re.sub(r'<p</a>', r'</a>', conteudo)

        soup = BeautifulSoup(conteudo, 'html.parser')

        #assinatura xhml
        soup.html['xmlns']= "http://www.w3.org/1999/xhtml"

        #id body
        soup.body['id']='view'

        #header PR
        header = soup.new_tag('header')
        header.append(soup.new_tag('h1'))
        header.h1.string = 'Presidência da República'

        header.append(soup.new_tag('h2'))
        header.h2.string = 'Casa Cívil'

        header.append(soup.new_tag('h3'))
        header.h3.string = 'Subchefia de Assuntos Juridicos'

        soup.body.insert(0,header)

        #remover link texto compilado original
        i = soup.find_all(string=re.compile('Texto compilado'))[0]
        #print(i.parent.parent.parent.parent)
        container = i.parent.parent.parent.parent
        del(container['align'])
        container.b.decompose()
        #print(container)



        header.append(soup.new_tag('p'))
        tag = soup.new_tag('a')
        tag['class'] = 'hide-action'
        tag['href'] = '#view'
        tag.string = 'Texto Compilado'
        container.append(tag)

        tag = soup.new_tag('a')
        tag['class'] = 'show-action'
        tag['href'] = '#'
        tag.string = 'Texto Completo'
        container.append(tag)

        #print(container)

        #encode
        soup.head.insert(0, soup.new_tag('meta', charset='iso8859-1'))

        #matadatos base
        soup.head.insert(0, soup.new_tag('base', href='http://www.planalto.gov.br/ccivil_03/', target="_blank"))

        #link CSS
        soup.head.append(soup.new_tag('link', type="text/css", rel="stylesheet",
                     href="http://www.planalto.gov.br/ccivil_03/css/legis_3.css"))
        soup.head.append(soup.new_tag('link', type="text/css", rel="stylesheet", href='css/legis_3.css'))

        #acervo constitucional
        container = soup.new_tag('nav')
        container.append(soup.new_tag('ul'))
        for i, tag in enumerate(soup.select('table a')):
            #print(i, tag)
            a = soup.new_tag('li')
            a.append(tag)
            container.ul.append(a)
        #print(container)
        soup.table.insert_after(container)

        #remove tabela de acervo constitucional
        soup.table.decompose()

       #attrs bgcolor
        container = soup.select('[bgcolor]')
        for i in container:
            del i['bgcolor']

        #attrs style
        container = soup.select('[style]')
        #print(len(container))
        for i in container:
            del i['style']

        #attrs align
        container = soup.select('[align]')
        for i in container:
            del i['align']
        container = soup.select('[ALIGN]')
        for i in container:
            del i['ALIGN']

        #remover tabela brasão
        soup.table.decompose()

        #remover tags style, meta generator
        container = soup.select('style')
        container += soup.select('meta[name="GENERATOR"]')
        container += soup.select('link[rel*="STYLESHEET"]')
        for i in container:
            i.decompose()

        # tags font, span, s, div
        container = soup.select('font')
        container += soup.select('span')
        container += soup.select('s')
        container += soup.select('div#art')
        container += soup.select('blockquote')
        container += soup.select('center')

        #print(container)
        for i in container:
            i.unwrap()

        #MS class
        container = soup.select('[class*="Ms"]')
        #print(len(container))
        for i in container:
            del i['class']

        #acrescentando class no strike
        container = soup.select('p > strike')
        #print(len(container))
        for i in container:
            i.parent['class']='mostrar revogado'
            pass

        #class para capitulos
        container = soup.find_all(string=re.compile('CAPÍTULO'))
        for j, i in enumerate(container):
            if i.parent.name == 'p':
                result = i.parent
                i.parent['class'] = 'capitulo'
            #elif i.parent.parent.name == 'p': pass
            else:
                result = i.parent.parent
                i.parent.parent['class'] = 'capitulo'
            #print(j, result)


        #class para titulo
        container = soup.find_all(string=re.compile('TÍTULO'))
        #print(len(container))
        for j, i in enumerate(container):
            #print(type(i))
            i.parent.parent['class'] = 'titulo'
            #print(i.parent.parent)
            pass

        #paragrafos vazios
        container = soup.find_all('p', ' ')
        for i, j in enumerate(container):
            #print(i, j.parent)
            pass

        #assinatura parlamentares
        container = soup.select('#view > i')
        for i, j in enumerate(container):
            print(i, j)
            pass

        fileout.write(soup.prettify(encoding='iso8859-1'))
        print(True)
        return True
    pass

if __name__ == '__main__':
    limpar_constituicao()
