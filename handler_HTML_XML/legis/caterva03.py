import re

def gen_url(filein='../data/links_decretos_normativos.txt'):
    with open(filein) as f:
        for linha in f:
            yield linha

def define_basename(url):
    result = None
    print(url)
    if re.match(r'.*planalto.*', url):
        print('planalto')
        result = (url.split('/')[-1]).split('.')[0]
    elif re.match(r'.*senado.*', url):
        print('senado')
        d= []
        for i in url.split('?')[-1].split('&'):
            d.append(i.split('='))
        d = dict(d)
        print(d)
        result = '{}{:0>5}'.format(d['tipo_norma'], d['numero'])

    print(result)
    return result

if __name__ == '__main__':
    nurl = gen_url()
    define_basename(next(nurl))
    define_basename(next(nurl))
    for i, j in enumerate(nurl):
        pass
#        print(i, j)





