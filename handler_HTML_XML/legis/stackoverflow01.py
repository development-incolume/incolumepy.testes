'''https://stackoverflow.com/questions/3115448/best-way-to-convert-the-this-html-file-into-an-xml-file-using-python'''
from bs4 import BeautifulSoup

def normalizado():
    f = open('L8112cons0.htm')
    soup = BeautifulSoup(f, "lxml")
    f.close()
    g = open('a.xml', 'w')
    #g.write(soup)
    print(g, soup.prettify())
    g.close()

def default0():
    f = open('L8112cons0.htm', encoding='utf-8')
    soup = BeautifulSoup(f, "lxml")
    f.close()
    g = open('a.xml', 'w')
    #g.write(soup.)
    print(g, soup.prettify())
    g.close()

def default1():
    with open('L8112cons.htm', encoding='iso-8859-1') as f:
        soup = BeautifulSoup(f, "lxml")
    with open('a.xml', 'w') as g:
        print(g,"---\n", soup.prettify())

def default2():
    with open('L8112cons.htm', encoding='iso-8859-1') as f:
        soup = BeautifulSoup(f, "lxml")
    with open('a.xml', 'w') as g:
        print(soup.prettify())

def default(fileoutput='a.xml'):
    with open('L8112cons.htm', encoding='iso-8859-1') as f:
        soup = BeautifulSoup(f, "lxml")
    with open(fileoutput, 'w') as g:
        print(soup.prettify())
        g.write(soup.prettify())

if __name__ == '__main__':
    default()
