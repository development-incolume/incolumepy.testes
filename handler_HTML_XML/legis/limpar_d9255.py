import os
import re

from bs4 import BeautifulSoup

with open(os.path.abspath('../data/D9255.htm')) as filein, \
        open(os.path.abspath('../fhtml/D9255.html'), 'wb') as fileout:
    conteudo = filein.read()
    conteudo = re.sub('\n', ' ', conteudo)
    soup = BeautifulSoup(conteudo, 'html.parser')

    container = soup.select('[style]')
    # print(len(container))
    for i in container:
        del i['style']

    # remove elemento style
    soup.select('style')[0].decompose()
    # remove elemento meta generator
    soup.select('meta[name$="GENERATOR"]')[0].decompose()
    # cria nova tag link
    meta = soup.new_tag('link', type="text/css", rel="stylesheet",
                        href="http://www.planalto.gov.br/ccivil_03/css/legis_3.css")
    # acrescenta nova tag no head
    soup.head.append(meta)


    container = soup.select('span')
    container += soup.select('s')
    container += soup.select('font')
    # print(len(container))
    for i in container:
        i.unwrap()

    container = soup.select('title')
    container += soup.select('p')
    for i, j in enumerate(container):
        print('{:0>2} {}:= {}'.format(i, j.name, j.text))

    fileout.write(soup.prettify(encoding='iso8859-1'))
    pass
