import inspect
import os
import platform
import re
from bs4 import BeautifulSoup

def elements_planalto(setor='css'):
    '''
    cria os elementos necessário para montagem do cabeção, metainformação e importação do CSS
    :param setor:
    :return:
    '''
    soup = BeautifulSoup('', 'html.parser')


    metalist = []
    csslist = []
    csslist.append(soup.new_tag('link', type="text/css", rel="stylesheet", href="http://www.planalto.gov.br/ccivil_03/css/legis_3.css"))
    csslist.append(soup.new_tag('link', type="text/css", rel="stylesheet", href='css/legis_3.css'))
    print(len(csslist), csslist)

    metalist.append(soup.new_tag('meta', id = "numero", content = "823"))
    metalist.append(soup.new_tag('meta', id = "tipo", content = "dec"))
    metalist.append(soup.new_tag('meta', id = "ano", content = "2018"))
    metalist.append(soup.new_tag('meta', id = "situacao", content = "vigente|revogado"))
    metalist.append(soup.new_tag('meta', id = "origem", content = "executivo"))
    metalist.append(soup.new_tag('meta', id = "chefegoverno", content = "Michel Temer"))
    metalist.append(soup.new_tag('meta', id = "alteracao", content = " "))
    metalist.append(soup.new_tag('meta', id = "referenda", content = "pessoal"))
    metalist.append(soup.new_tag('meta', id = "correlacao", content = " "))
    metalist.append(soup.new_tag('meta', id = "veto", content = " "))
    metalist.append(soup.new_tag('meta', id = "fonte", content = "DOU, seção I pagina 15 de 12/03/2018"))
    metalist.append(soup.new_tag('meta', id = "dataassinatura", content = "2018/03/11"))
    print(len(metalist), metalist)

    header = soup.new_tag('header')

    header.append(soup.new_tag('h1'))
    header.h1.string = 'Presidência da República'

    header.append(soup.new_tag('h2'))
    header.h2.string='Casa Cívil'

    header.append(soup.new_tag('h3'))
    header.h3.string='Subchefia de Assuntos Juridicos'

    header.append(soup.new_tag('p'))
    tag = soup.new_tag('a')
    tag['class'] = 'hide-action'
    tag['href'] = '#view'
    tag.string = 'Texto Compilado'
    header.p.append(tag)

    tag = soup.new_tag('a')
    tag['class'] = 'show-action'
    tag['href'] = '#'
    tag.string = 'Texto Completo'
    header.p.append(tag)

    print(len(header), header)


    if setor == 'meta':
        return metalist
    elif setor == 'header':
        return header
    return csslist


def select_plataforma(path=None):
    result = {}
    osys = platform.system()

    if path:
        path = os.path.abspath(path)
    elif osys == 'Windows':
        path = r'C:\Users\ricardobn\AppData\Local\Programs\Python\Python36-32\selenium\webdriver'
    elif osys == 'Linux':
        path = '../drivers/'

    if osys == 'Windows':
        result['chromebin'] = os.path.join(os.path.abspath(path), 'chromedriver.exe')
        result['firefoxbin'] = os.path.join(os.path.abspath(path), 'geckodriver.exe')
        result['encode'] = 'iso8859-1'
    elif osys == 'Linux':
        result['chromebin'] = os.path.join(os.path.abspath(path), 'chromedriver')
        result['firefoxbin'] = os.path.join(os.path.abspath(path), 'geckodriver')
        result['encode'] = 'utf-8'
    else:
        raise OSError('Sistema operacional não identificado.')
    return result


def gen_url(filein='../data/links_decretos_normativos.txt'):
    try:
        with open(filein) as f:
            for linha in f:
                yield linha
    except FileNotFoundError as e:
        print('filein informado *{}* inválido'.format(e.filename))


def define_basename(url):
    result = None
    print(url)
    if re.match(r'.*planalto.*', url):
        #        print('planalto')
        result = (url.split('/')[-1]).split('.')[0]
    elif re.match(r'.*senado.*', url):
        #        print('senado')
        d = []
        for i in url.split('?')[-1].split('&'):
            d.append(i.split('='))
        d = dict(d)
        print(d)
        result = '{}{:0>5}'.format(d['tipo_norma'], d['numero'])

    return result


def realfilename(filebase, extout='xml', basedir=''):
    count = 0

    while True:
        try:
            if basedir:
                os.makedirs(basedir, mode=0o755, exist_ok=True)

            if count <= 0:
                filename = os.path.join(basedir,
                                        '{}_{}.{}'.format(filebase, inspect.stack()[0][3], extout))
            else:
                filename = os.path.join(basedir,
                                        '{}_{}{:0>2}.{}'.format(filebase, inspect.stack()[0][3],
                                                                count, extout))
            if os.path.isfile(filename):
                raise IOError('Arquivo existente: {}'.format(filename))

            return filename
        except FileExistsError:
            print('Path *{}* não pode ser utilizado.'.format(basedir))
            break
        except IOError as e:
            print(e)
        except:
            raise
        finally:
            count += 1


if __name__ == '__main__':
    pass
    print(select_plataforma())
    elements_planalto()
