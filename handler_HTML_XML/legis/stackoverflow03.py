'''https://stackoverflow.com/questions/3115448/best-way-to-convert-the-this-html-file-into-an-xml-file-using-python'''
from lxml import html
from lxml.html.clean import clean_html
from xml.etree import ElementTree

def html2xml00():
    doc = html.fromstring(open('L8112cons.htm', encoding='iso-8859-1').read())
    with open('a.xhtml', 'wb') as out:
        out.write(ElementTree.tostring(doc))
def html2xml01():
    doc = html.fromstring(open('L8112cons.htm', encoding='iso-8859-1').read())
    with open('a3.xhtml', 'wb') as out:
        out.write(ElementTree.tostring(clean_html(doc)))
def html2xml(fileoutput='a3.xhtml'):
    doc = html.fromstring(open('L8112cons.htm', encoding='iso-8859-1').read())
    with open(fileoutput, 'wb') as out:
        out.write(ElementTree.tostring((clean_html(doc))))

if __name__ == '__main__':
    html2xml()
