__author__ = '@britodfbr'
import inspect
import os
import re
import platform

from os.path import join
from bs4 import BeautifulSoup
from selenium import webdriver

if platform.system() == 'Windows':
    chromebin = r'C:\Users\ricardobn\AppData\Local\Programs\Python\Python36-32\selenium\webdriver\chromedriver.exe'
    firefoxbin = r'C:\Users\ricardobn\AppData\Local\Programs\Python\Python36-32\selenium\webdriver\geckodriver.exe'
    encode = 'iso8859-1'
elif platform.system() == 'Linux':
    chromebin = '../../../drivers/chromedriver'
    firefoxbin = '../../../drivers/geckodriver'
    encode = 'utf-8'
else:
    raise OSError('Sistema operacional não identificado.')


def realfilename(filebase, extout='xml', basedir=None):
    count = 0
    if not basedir: basedir = ''

    while True:
        try:
            if count <= 0:
                filename = join(basedir,'{}_{}.{}'.format(filebase, inspect.stack()[0][3], extout))
            else:
                filename = join(basedir, '{}_{}{:0>2}.{}'.format(filebase, inspect.stack()[0][3], count, extout))
            if os.path.isfile(filename): raise IOError('Arquivo existente: {}'.format(filename))

            return filename
        except IOError as e:
            print(e)
        except:
            raise
        finally:
            count += 1


def gen_url(filein='../data/links_decretos_normativos.txt'):
    with open(filein) as f:
        for linha in f:
            yield linha


def legis2xml01a():
    print(next(get_url))
    for i, j in enumerate(get_url):
        print(i, j)


def legis2xml01b():
    chrome = webdriver.Chrome(chromebin)
    data = chrome.get(next(get_url))
    print(type(data))


def legis2xml01c(url):
    chrome = webdriver.Chrome(chromebin)
    print(url)
    data = chrome.get(url)
    print(type(data))
    chrome.close()


if __name__ == '__main__':
    get_url = gen_url()
#    print(realfilename('teste', basedir='../fxml', extout='xhtml'))
#    legis2xml01a()
#    legis2xml01b()
    next(get_url)
    legis2xml01c(next(get_url))