import os
from os.path import join, getsize

def lookup00():
    print(list(os.walk('/home/brito')))
    return os.walk('.')

def lookup():
    for root, dirs, files in os.walk('/home/brito'):
        print(root, "consumes", end="")
        print(sum([getsize(join(root, name)) for name in files]), end="")
        print("bytes in", len(files), "non-directory files")
        if 'CVS' in dirs:
            dirs.remove('CVS')  # don't visit CVS directories

if __name__ == '__main__':
    pass
    lookup00()