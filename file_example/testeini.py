from configparser import ConfigParser

config = ConfigParser()
try:
    config.read('test.ini')
#['test.ini']
    config._sections
    #{'db': {'dbname': 'testdb', 'host': 'localhost', 'dbuser': 'test_user', '__name__': 'db', 'password': 'abc123', 'port': '3306'}}
    connection_string = "dbname='%(dbname)s' user='%(dbuser)s' host='%(host)s' password='%(password)s' port='%(port)s'"
    connection_string % (config._sections['db'])
    #"dbname='testdb' user='test_user' host='localhost' password='abc123' po
except Exception as e:
    raise e
    print(e)