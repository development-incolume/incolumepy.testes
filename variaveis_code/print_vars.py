dia = 2
mes = 'jan'
ano = 2018

print('#1 Forma')
print('dia ' + str(dia) + ' de ' + str(mes) + ' de ' + str(ano) + '.')


print('\n#2 Forma')
print('dia', dia, 'de', mes, 'de', ano,'.')


print('\n#3 Forma')
print('dia %s de %s de %s.' % (dia, mes, ano))

print('\n#4 Forma')
print('dia %d de %s de %d.' % (dia, mes, ano))


print('\n#5 Forma')
print('dia {} de {} de {}.'.format(dia, mes, ano))

print('\n#6 Forma')
print('dia {:02d} de {} de {:d}.'.format(dia, mes, ano))


lista = [ dia, mes, ano ]
#print(lista)

print('\n#7 Forma')
print('dia {:02d} de {} de {:d}.'.format(*lista))


print('\n#8 Forma')
dict1 = {'dia' : 2, 'mes': 'jan', 'ano': 2018}
print('dia {dia} de {mes} de {ano}.'.format(*dict1, **dict1))

print('\n#9 Forma')
print('{0} {dia} {1} {mes} {2} {ano}.'.format(*dict1, **dict1))
