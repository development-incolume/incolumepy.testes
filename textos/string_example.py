import string

alfabeto = string.ascii_letters
alfabeto_list = [x for x in string.ascii_letters if x.islower()]
alfabeto_ca = ''.join([x for x in string.ascii_letters if x.isupper()])
alfabeto_cb = string.ascii_lowercase
alfabeto_cb2 = string.ascii_uppercase
sc = string.capwords('ricardo brito do nascimento')
numbers = string.digits
hexadecimal = string.hexdigits

pontuação = [x for x in string.punctuation]

if __name__ == '__main__':
    print(alfabeto)
    print(alfabeto_list)
    print(alfabeto_ca)
    print(alfabeto_cb)
    print(alfabeto_cb2)
    print(sc)
    print(numbers)
    print(hexadecimal)
    print(type(hexadecimal))
