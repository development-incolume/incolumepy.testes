import os
from collections import Counter
from string import punctuation


def count_words(filename=os.path.join(os.path.dirname(__file__), '..', 'hino_a_bandeira.txt')):
    '''
    collections.Counter + string.punctuation
    :param filename:
    :return:
    '''
    # print(filename)
    with open(filename) as f:
        content = f.read()
    # print(content)

    # print(punctuation)
    # remove a pontuação
    for char in punctuation:
        content = content.replace(char, '')

    # print(content)

    # separa as palavras e elementos de lista e
    # converte caracteres para minusculos
    words = content.lower().split()

    # print(words)
    dic = Counter(words)

    # retorna dicionário com as palavras ordenado pela quantidade.
    print(dic)
    return dic


if __name__ == '__main__':
    count_words()
