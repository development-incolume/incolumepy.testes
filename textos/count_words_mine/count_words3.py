import os
import re
from collections import defaultdict


def count_words(filename=os.path.join(os.path.dirname(__file__), '..', 'hino_a_bandeira.txt')):
    '''
    return words from arquive order by count.
    use collections.defaultdict + re
    :param filename: string
    :return: dict
    '''
    ddict = defaultdict(list)
    with open(filename) as file:
        words = re.findall('\w+', file.read().lower())
    for word in words:
        if word in ddict:
            ddict[word] += 1
        else:
            ddict[word] = 1
    # ordenar pelas chaves do dicionario, resultado lista
    ddict = sorted(ddict, key=lambda key: ddict[key], reverse=True)
    return ddict


if __name__ == '__main__':
    print(count_words())
