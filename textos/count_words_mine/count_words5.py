import os
import re
from collections import Counter


def count_words(filename=os.path.join(os.path.dirname(__file__), '..', 'hino_a_bandeira.txt')):
    '''
    return words from arquive order by count.
    use collections.Counter + re
    :param filename: string
    :return: dict
    '''
    with open(filename) as f:
        words = Counter(re.findall('\w+', f.read().lower()))

    ddict = dict(words)
    # ordenar pelas chaves do dicionario, resultado lista
    ddict = sorted(ddict, key=lambda key: ddict[key], reverse=True)
    return ddict


if __name__ == '__main__':
    print(count_words())
