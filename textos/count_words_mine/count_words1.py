import os
import re
from collections import Counter


def count_words(filename=os.path.join(os.path.dirname(__file__), '..', 'hino_a_bandeira.txt')):
    '''
    return words from arquive order by count.
    use collections.Counter + re
    :param filename: string
    :return: dict
    '''
    with open(filename) as f:
        return Counter(re.findall('\w+', f.read().lower()))


if __name__ == '__main__':
    print(count_words())
