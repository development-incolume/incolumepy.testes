import os
import re
from collections import Counter, defaultdict


def count_words(filename=os.path.join(os.path.dirname(__file__), '..', 'hino_a_bandeira.txt')):
    '''
    return words from arquive order by count.
    use collections.defaultdict + re
    :param filename: string
    :return: dict
    '''
    cdict = defaultdict(list)
    with open(filename) as file:
        palavras = re.findall('\w+', file.read().lower())

        for palavra in palavras:
            if palavra in cdict:
                cdict[palavra] += 1
            else:
                cdict[palavra] = 1
        print(len(palavras), palavras)
        print(len(cdict), dict(cdict))
        print(len(cdict), list(cdict))
        print(len(cdict), Counter(cdict))
        print()
    return cdict


if __name__ == '__main__':
    print(count_words())
