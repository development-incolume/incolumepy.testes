import os
import re
from collections import Counter, defaultdict


def count_words(filename=os.path.join(os.path.dirname(__file__), '..', 'hino_a_bandeira.txt')):
    '''
    return words from arquive order by count.
    use collections.Counter + re
    :param filename: string
    :return: dict
    '''
    ddict = defaultdict(list)
    with open(filename) as f:
        words = Counter(re.findall('\w+', f.read().lower()))

    # acrescentando dicionário ao defaultdict
    ddict.update(words)
    # ordenar pelas chaves do dicionario, resultado lista
    # ddict = sorted(ddict, key=lambda key:ddict[key], reverse=True)
    ddict = sorted(ddict.items(), key=lambda x: x[1], reverse=True)
    return ddict


if __name__ == '__main__':
    print(count_words())
