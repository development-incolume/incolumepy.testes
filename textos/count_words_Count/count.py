import os
import re
from collections import Counter

from incolumepy.testes.textos.count_words.count_words import texto


def count0(file):
    content = ''
    with open(file) as file:
        content = file.read()
        print(content)

    print()
    print(content.lower())

    print()
    print('re', re.findall('\w+', 'Casa, teste, azul'))
    print('re', re.findall('\w+', content.lower()))
    words = re.findall('\w+', content.lower())
    print(words)
    c = Counter(words)
    print(len(c.most_common()))
    print(c.most_common())
    print(c.most_common(10))


def count1(file):
    words = re.findall('\w+', open(file).read().lower())
    c = Counter(words)
    print(words)
    print(c)
    print('Total de palavras: {}, \n'
          'Contador: {}, \n'
          'top 10: {}'.format(len(words), c, c.most_common(10)))


def count2(file, top=10):
    print(Counter(re.findall('\w+', open(file).read().strip().lower())).most_common(top))


def count(input_file, top=10):
    try:
        print(Counter(re.findall('\w+', open(input_file).read().
                                 strip().lower())).most_common(top))
    except OSError as e:
        if e.errno == 36:
            print(Counter(re.findall('\w+', input_file.strip().lower())).most_common(top))
        else:
            raise
    except:
        raise

if __name__ == '__main__':
    print('>>>>')
    print(os.path.dirname(__file__))
    count0('loremipsum.txt')
    print()
    count('loremipsum.txt', 3)
    print()
    # count2(texto)
    count(texto, 3)
    count(os.path.join(os.path.dirname(__file__), '..', 'hino_a_bandeira.txt'))
