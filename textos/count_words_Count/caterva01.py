import re
from collections import Counter


def count(input_file, top=10):
    try:
        print(Counter(re.findall('\w+', open(input_file).read().
                                 strip().lower())).most_common(top))
    except OSError as e:
        if e.errno == 36:
            print(Counter(re.findall('\w+', input_file.strip().lower())).most_common(top))
        else:
            raise
    except:
        raise


def inanis0(input_file):
    # a pontuação distingue a palavra
    return Counter(open(input_file).read().strip().lower().split())


def inanis1(input_file):
    return Counter(re.findall('\w+', open(input_file).read().strip().lower()))


def inanis2(input_file):
    file = open(input_file)
    data = file.read()
    content = data.strip()
    content_lower = content.lower()
    lista = re.findall('\w+', content_lower)
    quantia = Counter(lista)
    return quantia


def inanis(input_file='loremipsum.txt', top=None):
    return Counter(re.findall('\w+', open(input_file).read().strip().lower())).most_common(top)


count('loremipsum.txt')
print(inanis('loremipsum.txt'))
