import doctest


def isVogal(letra):
    '''
    >>> isVogal('a')
    True
    >>> isVogal('r')
    False
    >>> isVogal('f')
    False
    >>> isVogal(';')
    False
    '''
    vogais = 'aeiou'
    return letra.lower() in vogais


def vogal():
    print(isVogal(input("Digite uma letra: ")))


if __name__ == '__main__':
    doctest.testmod()
    vogal()
