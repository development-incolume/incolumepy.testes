import unicodedata
from collections import defaultdict, Counter
from os import path
from string import punctuation, ascii_lowercase

# texto exemplo: Hino à Bandeira
text0 = """\
Salve, lindo pendão da esperança,
Salve, símbolo augusto da paz!
Tua nobre presença à lembrança
A grandeza da Pátria nos traz.

Recebe o afeto que se encerra
Em nosso peito juvenil,
Querido símbolo da terra,
Da amada terra do Brasil!

Em teu seio formoso retratas
Este céu de puríssimo azul,
A verdura sem par destas matas,
E o esplendor do Cruzeiro do Sul.

Recebe o afeto que se encerra
Em nosso peito juvenil,
Querido símbolo da terra,
Da amada terra do Brasil!

Contemplando o teu vulto sagrado,
Compreendemos o nosso dever;
E o Brasil, por seus filhos amado,
Poderoso e feliz há de ser.

Recebe o afeto que se encerra
Em nosso peito juvenil,
Querido símbolo da terra,
Da amada terra do Brasil!

Sobre a imensa Nação Brasileira,
Nos momentos de festa ou de dor,
Paira sempre, sagrada bandeira,
Pavilhão da Justiça e do Amor!

Recebe o afeto que se encerra
Em nosso peito juvenil,
Querido símbolo da terra,
Da amada terra do Brasil!
"""
texto = open(path.join(path.dirname(__file__), 'hino_a_bandeira.txt')).read()


def leterscount(input_texto):
    # converte string para bytes
    btexto = unicodedata.normalize('NFKD', input_texto).encode('ascii',
                                                               'ignore').lower()
    # print(type(input_texto), type(btexto))

    resultados = {'vogais': 0, 'consoantes': 0,
                  'punctuation': 0, 'outros': 0}
    dic = defaultdict(list)
    result = ''
    try:
        for letra in btexto:
            # print('{:3}:{}'. format(letra, chr(letra)))
            if chr(letra) in dic:
                dic[chr(letra)] += 1
            else:
                dic[chr(letra)] = 1

            if chr(letra) in 'aeiou':
                resultados['vogais'] += 1
            elif chr(letra) in [x for x in
                                ascii_lowercase if x not in 'aeiou']:
                resultados['consoantes'] += 1
            elif chr(letra) in punctuation:
                resultados['punctuation'] += 1
            else:
                resultados['outros'] += 1

        rank = Counter(dic)
        result = str(resultados)

        result = 'Caracteres: {a:5},\n' \
                 'Vogais:     {b[vogais]:5},\n' \
                 'Consoantes: {b[consoantes]:5},\n' \
                 'Pontuação:  {b[punctuation]:5},\n' \
                 'Outros:     {b[outros]:5}\n' \
                 'Metainformação: {c}\n' \
                 'Rank: {d}\n' \
                 ''.format(a=len(input_texto), b=resultados, c=dict(dic), d=rank)


    except Exception as e:
        result = e

    return result


if __name__ == '__main__':
    print(leterscount(text0))
    print(leterscount(texto))
