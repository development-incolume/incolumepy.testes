# http://www.programcreek.com/python/example/1084/difflib.Differ
def print_diff(s1, s2):
    import difflib
    differ = difflib.Differ()
    result = list(differ.compare(s1.splitlines(), s2.splitlines()))
    print('\n'.join(result))


if __name__ == '__main__':
    from os import path

    file1 = open(path.join(path.dirname(__file__), 'file1.txt')).read()
    file2 = open(path.join(path.dirname(__file__), 'file2.txt')).read()

    print_diff(file1, file2)
