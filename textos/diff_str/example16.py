# http://www.programcreek.com/python/example/1084/difflib.Differ

def showdiff(old, new):
    import difflib
    d = difflib.Differ()
    lines = d.compare(old.lines(), new.lines())
    realdiff = False
    for l in lines:
        print(l, end=',')
        if not realdiff and not l[0].isspace():
            realdiff = True
    return realdiff


if __name__ == '__main__':
    from os import path

    file1 = open(path.join(path.dirname(__file__), 'file1.txt'))
    file2 = open(path.join(path.dirname(__file__), 'file2.txt'))
    print(file1)
    showdiff(file1, file2)
