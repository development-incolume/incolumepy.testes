# https://docs.python.org/2/library/difflib.html
from difflib import ndiff

diff = ndiff('one\ntwo\nthree\n'.splitlines(1),
             'ore\ntree\nemu\n'.splitlines(1))

print(''.join(diff))
'''
- one
?  ^
+ ore
?  ^
- two
- three
?  -
+ tree
+ emu
'''
