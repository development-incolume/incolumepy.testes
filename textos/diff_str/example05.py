# https://docs.python.org/2/library/difflib.html
import sys
from difflib import unified_diff

s1 = ['bacon\n', 'eggs\n', 'ham\n', 'guido\n']
s2 = ['python\n', 'eggy\n', 'hamster\n', 'guido\n']

for line in unified_diff(s1, s2, fromfile='before.py', tofile='after.py'):
    sys.stdout.write(line)

'''
--- before.py
+++ after.py
@@ -1,4 +1,4 @@
-bacon
-eggs
-ham
+python
+eggy
+hamster
 guido
 '''
