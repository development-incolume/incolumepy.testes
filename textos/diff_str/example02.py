# https://docs.python.org/2/library/difflib.html
import keyword
from difflib import get_close_matches

print(get_close_matches('appel', ['ape', 'apple', 'peach', 'puppy']))
# ['apple', 'ape']

print(keyword.kwlist)

print(get_close_matches('wheel', keyword.kwlist))
# ['while']

print(get_close_matches('apple', keyword.kwlist))
# []

print(get_close_matches('accept', keyword.kwlist))
# ['except']
