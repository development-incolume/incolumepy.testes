# https://docs.python.org/2/library/difflib.html
from difflib import SequenceMatcher


def implement01():
    s = SequenceMatcher(None, " abcd", "abcd abcd")
    print(s.find_longest_match(0, 5, 0, 9))
    print()


def implement02():
    s = SequenceMatcher(lambda x: x == " ", " abcd", "abcd abcd")
    print(s.find_longest_match(0, 5, 0, 9))
    print()


def implement03():
    s = SequenceMatcher(None, "abxcd", "abcd")
    print(s.get_matching_blocks())
    print()


def implement04():
    '''analisa a diferença entre as strings considerando cada caracter'''
    a = "qabxcd"
    b = "abycdf"
    s = SequenceMatcher(None, a, b)
    for tag, i1, i2, j1, j2 in s.get_opcodes():
        print("%7s a[%d:%d] (%s) b[%d:%d] (%s)" %
              (tag, i1, i2, a[i1:i2], j1, j2, b[j1:j2]))
    print()


def implement05():
    s = SequenceMatcher(None, "abcd", "bcde")
    print("'ratio': {}, 'quick ratio': {}, 'real ratio': {}".format(
        s.ratio(), s.quick_ratio(), s.real_quick_ratio()))
    print()


def implement06():
    f1 = "private Thread currentThread;"
    f2 = "private volatile Thread currentThread;"
    s = SequenceMatcher(lambda x: x == " ",
                        f1,
                        f2)
    print('Valores maiores que .6, indicam similaridade')
    print(round(s.ratio(), 3))
    print()

    print('apresenta os blocos alterados')
    for block in s.get_matching_blocks():
        print("a[%d] and b[%d] match for %d elements" % block)

    print('\n apresenta caracteres inseridos, removidos e iguais')
    for opcode in s.get_opcodes():
        print("%6s a[%d:%d] b[%d:%d]" % opcode)

    print()


implement01()
implement02()
implement03()
implement04()
implement05()
implement06()
