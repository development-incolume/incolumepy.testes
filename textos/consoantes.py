from string import ascii_lowercase

print([x for x in ascii_lowercase if x not in 'aeiou'])
