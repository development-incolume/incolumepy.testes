''' Gera o hino_a_bandeira.txt'''

# texto exemplo: Hino à Bandeira
texto = """\
Salve, lindo pendão da esperança,
Salve, símbolo augusto da paz!
Tua nobre presença à lembrança
A grandeza da Pátria nos traz.

Recebe o afeto que se encerra
Em nosso peito juvenil,
Querido símbolo da terra,
Da amada terra do Brasil!

Em teu seio formoso retratas
Este céu de puríssimo azul,
A verdura sem par destas matas,
E o esplendor do Cruzeiro do Sul.

Recebe o afeto que se encerra
Em nosso peito juvenil,
Querido símbolo da terra,
Da amada terra do Brasil!

Contemplando o teu vulto sagrado,
Compreendemos o nosso dever;
E o Brasil, por seus filhos amado,
Poderoso e feliz há de ser.

Recebe o afeto que se encerra
Em nosso peito juvenil,
Querido símbolo da terra,
Da amada terra do Brasil!

Sobre a imensa Nação Brasileira,
Nos momentos de festa ou de dor,
Paira sempre, sagrada bandeira,
Pavilhão da Justiça e do Amor!

Recebe o afeto que se encerra
Em nosso peito juvenil,
Querido símbolo da terra,
Da amada terra do Brasil!

"""


def genHino(filename='hino_a_bandeira.txt'):
    with open(filename, 'w') as file:
        file.write(texto)


if __name__ == '__main__':
    genHino()
