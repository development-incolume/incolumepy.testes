def over_two(lst):
    lst1 = [x for x in lst if x > 2]
    return lst1


print('\nMaior que 2')
print(over_two([1, 2, 3, 4]))

print(list(filter(lambda x: x > 2, [1, 2, 3, 4])))

print('\nImpares')
print(list(filter(lambda x: (x % 2), [x for x in range(10)])))
