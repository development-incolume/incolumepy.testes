import sys

import incolumepy.testes.handler_error.errosPersonalizados
from incolumepy.testes.handler_error.errosPersonalizados import getNum


def testes():
    sys.path.append('.')
    from errosPersonalizados import getNum

    print(sys.path)
    getNum(10)
    incolumepy.testes.handler_error.errosPersonalizados.getNum(4)


def get3Num():
    nums = list()
    while len(nums) < 3:
        nums.append(getNum(input('Digite %sº Número: ' % (len(nums) + 1))))
    return nums


if __name__ == '__main__':
    testes()
    get3Num()
