
def e_handler01():
    try:
        raise TypeError('Error TypeError')
    except Exception as e:
        print(e)

def e_handler02():
     N1 = 25
     N2 = 'asd'
     try:
         N1 + N2
     except:
         print('erro ao tentar realizar a soma')

def e_handler03():
     N1 = 25
     N2 = 'asd'
     try:
         N1 + N2
     except TypeError as e:
         print('erro ao tentar realizar a soma')
     except:
         raise


if __name__ == '__main__':
    e_handler01()
    e_handler02()
    e_handler03()
