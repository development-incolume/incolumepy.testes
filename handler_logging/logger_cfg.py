import logging


def forma1():
    logging.basicConfig(filename='sample.log', level=logging.DEBUG,
                        format='%(asctime)s:%(name)s:%(message)s')


def forma2():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s:%(name)s:%(message)s')

    file_handler = logging.FileHandler('info.log')
    file_handler.setLevel(logging.ERROR)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
