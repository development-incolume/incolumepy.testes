import logging.handlers
import os
import zlib

def namer(name):
    return name + ".gz"

def rotator(source, dest):
    with open(source, "rb") as sf:
        data = sf.read()
        compressed = zlib.compress(data, 9)
        with open(dest, "wb") as df:
            df.write(compressed)
    os.remove(source)

if __name__ == '__main__':
    file = os.path.abspath(os.path.join('..', 'tmp',
        '{}.log'.format(os.path.basename(__file__).split('.')[0])))
    rh = logging.handlers.RotatingFileHandler(
        file, maxBytes=10, backupCount=10)
    rh.rotator = rotator
    rh.namer = namer
    rh.setLevel(logging.DEBUG)

    for i in range(101):
        logging.debug('starting...')
        logging.debug(i)
        logging.debug('..end')