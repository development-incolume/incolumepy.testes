class BraceMessage(object):
    def __init__(self, fmt, *args, **kwargs):
        self.fmt = fmt
        self.args = args
        self.kwargs = kwargs

    def __str__(self):
        return self.fmt.format(*self.args, **self.kwargs)

class DollarMessage(object):
    def __init__(self, fmt, **kwargs):
        self.fmt = fmt
        self.kwargs = kwargs

    def __str__(self):
        from string import Template
        return Template(self.fmt).substitute(**self.kwargs)


if __name__ == '__main__':
    __ = BraceMessage
    print(__('Message with {0} {1}', 2, 'placeholders'))
    class Point: pass
    p = Point()
    p.x = 0.5
    p.y = 0.5
    print(__('Message with coordinates: ({point.x:.2f}, {point.y:.2f})', point=p))
