from brace import BraceMessage as __
print(__('Message with {0} {name}', 2, name='placeholders'))

class Point: pass
p = Point()
p.x = 0.5
p.y = 0.5
print(__('Message with coordinates: ({point.x:.2f}, {point.y:.2f})',
         point=p))

from brace import DollarMessage as __
print(__('Message with $num $what', num=2, what='placeholders'))
