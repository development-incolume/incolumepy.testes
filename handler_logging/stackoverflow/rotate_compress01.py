import gzip
import os
import logging
import logging.handlers

class GZipRotator:
    def __call__(self, source, dest):
        os.rename(source, dest)
        with open(dest, 'rb') as f_in, gzip.open('{}.gz'.format(dest), 'wb') as f_out:
            f_out.writelines(f_in.read())
        os.remove(dest)

logformatter = logging.Formatter('%(asctime)s;%(levelname)s;%(message)s')
#logformatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

filelog = os.path.abspath(os.path.join('../', 'tmp',
        '{}.log'.format(os.path.basename(__file__).split('.')[0])))

log = logging.handlers.TimedRotatingFileHandler(
    filelog, 'midnight', 1, backupCount=5)

log.setLevel(logging.DEBUG)
log.setFormatter(logformatter)
log.rotator = GZipRotator()

logger = logging.getLogger('main')
logger.addHandler(log)
logger.setLevel(logging.DEBUG)


if __name__ == '__main__':
    for i in range(100):
        logger.debug(i)
    pass