import gzip
import os
import logging
import logging.handlers
import names

#regra para compress
class GZipRotator:
    def __call__(self, source, dest):
        with open(dest, 'rb') as f_in, gzip.open('{}.gz'.format(dest), 'wb') as f_out:
            compressed = gzip.compress(f_in.read(), 9)
            f_out.write(compressed)
        os.remove(source)

class Namer:
    def __call__(self, name):
        return name + ".gz"

#formatos de logs
logformatter = logging.Formatter('%(asctime)s;%(levelname)s;%(message)s')
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

#nome dos arquivos
filelog = os.path.abspath(os.path.join('../', 'tmp',
        '__{}.log'.format(os.path.basename(__file__).split('.')[0])))

filelog1 = os.path.abspath(os.path.join('../', 'tmp',
        '_{}.log'.format(os.path.basename(__file__).split('.')[0])))

#rotaciona por tamanho
log1 = logging.handlers.RotatingFileHandler(
        filelog1, maxBytes=10, backupCount=7)

#rotaciona por tempo
log = logging.handlers.TimedRotatingFileHandler(
    filelog, 'midnight', 1, backupCount=5)

log1.setFormatter(formatter)
log1.setLevel(logging.DEBUG)
log1.rotator = GZipRotator()
log1.namer = Namer

log.setLevel(logging.DEBUG)
log.setFormatter(logformatter)
log.rotator = GZipRotator()
log.namer = Namer

logger = logging.getLogger('main')
logger.addHandler(log)
#logger.addHandler(log1)
logger.setLevel(logging.DEBUG)


if __name__ == '__main__':
    for i in range(100):
        logger.debug('{}: {}'.format(i, names.get_first_name(gender='male')))
        logger.error('{}: {}'.format(i, names.get_first_name(gender='female')))
        logger.info('{}: {}'.format(i, names.get_full_name(gender='female')))
    pass