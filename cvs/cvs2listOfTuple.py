import csv

class Test:
    file_csv_in = '../../../data/cidades_brasil.csv'

    def test01(self):
        '''
        acesso ao cvs
        :return:
        '''
        with open(self.file_csv_in) as filein:
            #print(filein.read())
            print(filein.readline())
        pass

    def test02(self):
        '''
        exemplo disponivem em:
        http://stackoverflow.com/questions/18776370/converting-a-csv-file-into-a-list-of-tuples-with-python
        :return:
        '''
        with open(self.file_csv_in) as filein:
            data = [tuple(line) for line in csv.reader(filein)]
        print(data)




    @classmethod
    def Main(cls):
        Test().test01()
        Test().test02()

if __name__ == '__main__':
    Test.Main()
