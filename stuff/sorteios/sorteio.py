from random import choice
from collections import defaultdict

meses = {1: 'jan', 2: 'fev', 3: 'mar', 4: 'abr',
         5: 'mai', 6: 'jun', 7: 'jul', 8: 'ago',
         9: 'set', 10: 'out', 11: 'nov', 12: 'dev'}

lista = sorted(['Eliana', "Elaine", 'Elisangela', 'Elias Jr', 'Elias'])

def unico(participantes):
    participantes = participantes.copy()
    print(participantes)
    for i in [x for x in meses if x >= 9 ]:
        escolhido = choice(participantes)
        participantes.remove(escolhido)
        print('{}: {}'.format(meses[i], escolhido))
    print('Não sorteado: {}\n\n'.format(participantes))


def duplo0():
    participantes = lista.copy()
    mes = defaultdict(list)
    print(participantes)
    for i in [x for x in meses if x >= 9]:
        for j in range(2):
            if participantes:
                pass
            else:
                participantes = lista.copy()
            escolhido = choice(participantes)
            participantes.remove(escolhido)
            mes[meses[i]].append(escolhido)
    print(dict(mes.items()))

def duplo1(lista):
    alista = lista.copy()
    blista = lista.copy()
    l = alista
    sorteados = defaultdict(list)
    #print(id(alista), id(blista), id(l))
    print('participantes: {}'.format(', '.join(lista)))
    for i in range(2):
        for i in [x for x in meses if x >= 9]:
            #print(i, meses[i])
            if not l:
                l = blista
            escolhido = choice(l)
            l.remove(escolhido)
            sorteados[meses[i]].append(escolhido)
            #print(meses[i], escolhido)
    #print(sorteados)
    #print('', alista, blista)
    for e, i in sorteados.items():
        print(e, i)

    print()
    return sorteados

def duplo(lista):
    alista = lista.copy()
    blista = lista.copy()
    l = alista
    sorteados = defaultdict(list)
    #print(id(alista), id(blista), id(l))
    print('participantes: {}'.format(', '.join(lista)))
    for i in range(2):
        for i in [x for x in meses if x >= 9 and x < 12]:
            #print(i, meses[i])
            if not l:
                break
            escolhido = choice(l)
            l.remove(escolhido)
            sorteados[meses[i]].append(escolhido)
            #print(meses[i], escolhido)
    #print(sorteados)
    #print('', alista, blista)
    for e, i in sorteados.items():
        print(e, i)

    print()
    return sorteados

if __name__ == '__main__':
    #unico(lista)
    r=duplo(lista)
