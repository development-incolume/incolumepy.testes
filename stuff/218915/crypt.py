import base64
import os


def cifrar00():
    file_original = 'txt/k.txt'
    file_dest = 'txt/k_enc.txt'

    f = open(file_original, 'rb')
    dados = f.read()
    f.close()

    dados_enc = base64.b64encode(dados)
    f = open(file_dest, 'wb')
    f.write(dados_enc)
    f.close()


def cifrar01(fileorigem, filedestino):
    with open(fileorigem, 'rb') as forigem, open(filedestino, 'wb') as fdestino:
        conteudo = forigem.read()
        fdestino.write(base64.b64encode(conteudo))


def cifrarDir(dir='./txt'):
    arquivos = [arq for arq
                in [os.path.join(dir, file) for file
                    in os.listdir(dir)] if os.path.isfile(arq)]
    print(arquivos)
    for arquivo in arquivos:
        print(arquivo)
        with open(arquivo, 'rb') as forigem, open((arquivo + '.cript'), 'wb') as fdestino:
            conteudo = forigem.read()
            fdestino.write(base64.b64encode(conteudo))


if __name__ == '__main__':
    cifrar00()
    cifrar01('txt/k.txt', 'txt/kCript.txt')
    cifrarDir()
