import requests
import csv

def get_cvs(urlcsvfile='http://magonia.ibict.br/ckan/dataset/'
                    '1f1475d3-73a3-4972-871c-493f2100204f/'
                    'resource/5ebdde66-1229-4dfe-a0c6-6a6b3836679f/'
                    'download/saude.csv', proxies=None, timeout=None):

    data = requests.get(urlcsvfile, proxies=proxies, timeout=timeout)
    content = data.content.decode('utf-8')
    handlecsv = csv.reader(content.splitlines(), delimiter=';', quoting=csv.QUOTE_MINIMAL)
    with open('saude.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        for linha in handlecsv:
            writer.writerow(linha)
    return True

def cvs2dict(cvsfile='saude.csv'):
    result = {}
    with open(cvsfile) as csvfile:
        result = dict(csv.DictReader(csvfile))
    return result

if __name__ == '__main__':
    #get_cvs()
    print(cvs2dict())

