import calendar
import datetime
import locale

import requests
from bs4 import BeautifulSoup

url = ' http://www.bhaktiyogapura.com/2017/03/calendario-vaisnava-marco-de-2017/'
site = requests.get(url)

site_HTML = BeautifulSoup(site.content, 'html.parser')
dados_calendario = site_HTML.find(attrs={"class": "the_content_wrapper"})


def busca_data_do_evento(evento):
    ''' A datas estão dentro de uma tag strong. Assim sendo, retornamos a primeira
    tag p que possui strong antes do eventos'''
    data = evento.find_previous_siblings('p')
    for d in data:
        if d.find('strong'):
            return d


def busca_eventos(calendario):
    ''' Busca todos o eventos do calendário e retorna em uma lista. Enventos são inciados por '-' ou '–' '''
    eventos = [evento for evento in calendario.find_all('p') if
               evento.text.startswith('-') or evento.text.startswith('–')]
    return eventos


def trata_eventos(eventos):
    ''' Algumas data possuem eventos divididos em mais de uma tag p, vamos tratar
        estas situações verificando na página se existem dois eventos seguidos,
        se existirem agrupamos em um único item '''

    def is_evento(tag):
        if tag.text.startswith('-'):
            return True
        else:
            return False

    eventos_tratados = []
    for n, evento in enumerate(eventos):
        proxima_tag = evento.find_next_sibling('p')
        if is_evento(proxima_tag):
            eventos_tratados.append([evento, proxima_tag])
        else:
            foi_tratado = any([evento in evento_tratado for evento_tratado in eventos_tratados])
            if foi_tratado is False:
                eventos_tratados.append([evento])
    return eventos_tratados


eventos = busca_eventos(dados_calendario)
eventos = trata_eventos(eventos)
calendario = [[busca_data_do_evento(e[0]).text, e] for e in eventos]

''' Neste ponto temos o calendario com as datas e eventos, contudo os eventos ainda contem ainda estao como objetos Beautifulsoup.
Vamos deixar apenas o texto '''


def get_text(evento):
    return evento.text.strip()


for n, data in enumerate(calendario):
    eventos_data = data[1]
    text_eventos = [get_text(e) for e in eventos_data]
    # altera o calendario deixando apenas texto
    calendario[n][1] = text_eventos

locale.setlocale(locale.LC_ALL, 'Portuguese')


def trata_data(string_data_evento):
    data = string_data_evento.split('-')[0]
    dia, mes, ano = data.split(' ')
    meses = [mes.title() for mes in calendar.month_name]
    mes = str(meses.index(mes))
    data = ''.join([dia, mes, ano])
    return datetime.datetime.strptime(data, "%d%m%Y").date()


''' Agora vamos tratar as datas e tratar para o formato datetime.date e inseri-las na lista de eventos '''
for n, evento in enumerate(calendario):
    calendario[n].insert(0, trata_data(evento[0]))

''' Finalmente criamos um dicionário para facilitar a busca por data'''
calendario_vaisnava = {}
for evento in calendario:
    calendario_vaisnava[evento[0]] = evento[2][0]

dia = datetime.datetime.strptime('04032017', "%d%m%Y").date()
print(calendario_vaisnava[dia])
