def soma_lista0(lista=list()):
    '''

    :param lista:
    :return:
    >>> soma_lista([1,2,3])
    6
    >>> soma_lista([1,2,3,4])
    10
    >>> soma_lista([1,2,3,4,5,6])
    21
    '''
    try:
        return lista.pop() + soma_lista(lista)
    except IndexError:
        return 0


def soma_lista1(lista=list()):
    '''

    :param lista:
    :return:
    >>> soma_lista([1,2,3])
    6
    >>> soma_lista([1,2,3,4])
    10
    >>> soma_lista([1,2,3,4,5,6])
    21
    '''
    if len(lista) >= 1:
        return lista.pop() + soma_lista(lista)
    else:
        return 0


def soma_lista(lista):
    '''

    :param lista:
    :return:
    >>> soma_lista([1,2,3])
    6
    >>> soma_lista([1,2,3,4])
    10
    >>> soma_lista([1,2,3,4,5,6])
    21
    '''
    if len(lista) == 1:
        return lista[0]
    else:
        return lista[0] + soma_lista(lista[1:])


if __name__ == '__main__':
    import doctest

    doctest.testmod()
    print()
