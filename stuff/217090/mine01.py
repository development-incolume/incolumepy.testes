def int2hex(n):
    '''
    :param n: int
    :return: str
    >>> int2hex(10)
    'A'
    >>> int2hex(15)
    'F'
    >>> int2hex(32)
    '20'
    >>> int2hex(255)
    'FF'
    >>> int2hex(65535)
    'FFFF'
    '''

    x16 = '0 1 2 3 4 5 6 7 8 9 a b c d e f'.upper().split()
    result = []
    while n > 0:
        result.append(x16[(n % 16)])
        n = n // 16
    result.reverse()
    return ''.join(result)


if __name__ == '__main__':
    import doctest

    doctest.testmod()

    print(int2hex(64202))
