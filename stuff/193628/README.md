#Timestamp
## Introdução
Um timestamp é uma seqüência de caracteres ou 
informações codificadas que identificam quando 
ocorreu um determinado evento, geralmente dando 
data, hora e dia, às vezes com precisão em uma 
pequena fração de segundo. 

O termo deriva dos carimbos usados em escritórios 
para marcar a data atual com tinta 
em documentos de papel, registrando quando o 
documento foi recebido. 

Exemplos comuns deste tipo de carimbo de data / hora 
são um carimbo de data em uma carta ou os tempos de
"entrada" e "saída" em um cartão de ponto.

Nos tempos modernos, o uso do termo se expandiu 
para se referir a informações digitais de data 
e hora anexadas a dados digitais. Por exemplo, 
os arquivos de computador contêm timestamps que 
informam quando o arquivo foi modificado pela 
última vez, e as câmeras digitais adicionam 
timestamps às fotos , registrando a data e hora 
em que a imagem foi tirada.

## Formatos
Há diversos formatos para timestamp:

- ISO 8601 format, YYYY-MM-DDThh:mm:ss.sTZD 
(1994-11-05T08:15:30.13843-05:00)
onde:

     - YYYY = quatro dígitos para o ano
     - MM   = dois dígitos para o mês (01=Janeiro, etc.)
     - DD   = dois dígitos para o dia do mês (de 01 até 31)
     - hh   = dois dígitos para horas (de 00 até 23) (am/pm não é permitido)
     - mm   = dois dígitos para minutos (de 00 até 59)
     - ss   = dois dígitos para segundos (de 00 até 59)
     - s    = um ou mais digitos representando a fração decimal
      dos segundos;
     - TZD  = designa o fuso horário (Z ou +hh:mm ou -hh:mm)
- Norte Americano, M/DD/YY
(7/29/17)
- Europeu, DD/MM/YY 
(29/07/17)

- Unix epoch: Quantidade de segundos desde 1/1/1970.
(**1501335001** equivale a Sáb Jul 29 10:30:01 BRT 2017)

- Unix time: idem Unix epoch;

- POSIX time: idem Unix epoch;

- Unix timestamp: idem Unix epoch;

- big-endian, (YYYYMMDD| YYYY-MM-DD| YYYY/MM/DD): O 
numeros são ordenados por ordem de grandeza, Ano, mês e dia. ex:
 (20170729)(2017-07-29)(2017/07/29)

- rfc-2822 (aaa, DD MMM YYYY HH:mm:ss Z): ex: 
(Mon, 14 Aug 2006 02:34:56 -0600)

- rfc-3339 (YYYY-MM-DD HH:mm:ssZ) : ex
(2006-08-14 02:34:56-06:00)

- Personalizados: Ao gosto do freguês.
   - 29 07 2017
   - 29-07-2017
   - Sáb Jul 29 2017
   - Sáb Jul 29 2017 10:30:01
   - 29 Jul 2017, Sáb
   - Sáb Jul 29 10:30:01
   - 29 Jul 2017
   - Brasília, 29 de Jul de 2017
    

##Referências
- man date
- https://en.wikipedia.org/wiki/Timestamp
- https://www.w3.org/TR/NOTE-datetime
- https://pt.wikipedia.org/wiki/ISO_8601
- https://www.epochconverter.com/
- http://www.ehow.com.br/maneiras-escrever-data-diferentes-paises-info_283002/