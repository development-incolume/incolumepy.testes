import re

t = 'a, b; c. d\na: b'
print(t)

#replace with regex
s = re.sub(r'[\n:,.;]', r'', t)
print(s)