import netifaces

def get_ipaddr():
    localip = netifaces.ifaddresses('enp0s25')[netifaces.AF_INET][0]['addr']
    print(localip)

def ifaces():
    #print(netifaces.interfaces())
    return (netifaces.interfaces())

def get_ip_principal():
    for i in ifaces():
        if i == 'lo':
            (i)
        localip = netifaces.ifaddresses(i)[netifaces.AF_INET][0]['addr']
    return localip

def get_ip_array():
    localip = []
    for i in ifaces():
        localip.append(netifaces.ifaddresses(i)[netifaces.AF_INET][0]['addr'])
    return localip

def get_ip_dict():
    localip = {}
    for i in ifaces():
        localip[i] = netifaces.ifaddresses(i)[netifaces.AF_INET][0]['addr']
    return localip

if __name__ == '__main__':
    #get_ipaddr()
    #ifaces()
    print(get_ip_principal())
    print(get_ip_array())
    print(get_ip_dict())