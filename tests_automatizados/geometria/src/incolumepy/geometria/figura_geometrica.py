class FiguraGeometrica:
    def get_area(self):
        raise NotImplementedError(NotImplemented)

    def get_perimetro(self):
        raise NotImplementedError(NotImplemented)

    def __setattr__(self, key, value):
        if key not in self.__dict__:
            raise AttributeError('Atributo {} não permitido'.format(key))
        self.__dict__[key] = value

    def __getattr__(self, item):
        if item not in self.__dict__:
            raise AttributeError('Atributo {} inexistente'.format(item))
        return self.__dict__[item]


if __name__ == '__main__':
    f = FiguraGeometrica()
    print(f.get_area())
