import os
import sys

try:
    from incolumepy.geometria.figura_geometrica import FiguraGeometrica
except:
    sys.path.append(os.path.join(os.path.dirname(__file__), *'incolumepy.geometria'.split('.')))
    from figura_geometrica import FiguraGeometrica


class Quadrado(FiguraGeometrica):
    __dict__ = {'lado': None}

    def get_area(self):
        return self.lado ** 2

    def get_perimetro(self):
        return self.lado * 4
