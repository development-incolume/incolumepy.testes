import os
import sys

try:
    from incolumepy.geometria.figura_geometrica import FiguraGeometrica
except:
    sys.path.append(os.path.join(os.path.dirname(__file__), *'incolumepy.geometria'.split('.')))
    from figura_geometrica import FiguraGeometrica


class Triangulo(FiguraGeometrica):
    __dict__ = {'base': None, 'altura': None, 'lados': None}

    def __init__(self):
        super()

    def get_perimetro(self):
        return sum(self.lados)

    def get_area(self):
        return self.base * self.altura


if __name__ == '__main__':
    t = Triangulo()
