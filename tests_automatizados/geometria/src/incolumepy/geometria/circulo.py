import sys
import os
import math
try:
    from incolumepy.geometria.figura_geometrica import FiguraGeometrica
except:
    sys.path.append(os.path.join(os.path.dirname(__file__), *'incolumepy.geometria'.split('.')))
    from figura_geometrica import FiguraGeometrica


class Circulo(FiguraGeometrica):
    __dict__ = {'raio': None}

    def get_area(self):
        return math.pi * self.raio ** 2

    def get_perimetro(self):
        return 2 * math.pi * self.raio

