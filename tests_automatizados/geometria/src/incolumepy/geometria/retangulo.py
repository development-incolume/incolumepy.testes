import os
import sys

try:
    from incolumepy.geometria.figura_geometrica import FiguraGeometrica
except:
    sys.path.append(os.path.join(os.path.dirname(__file__), *'incolumepy.geometria'.split('.')))
    from figura_geometrica import FiguraGeometrica


class Retangulo(FiguraGeometrica):
    __dict__ = {'base': None, 'altura': None}

    def get_area(self):
        return self.base * self.altura

    def get_perimetro(self):
        return (self.base + self.altura) * 2
