import sys

import pytest

'''\
cd source ~/projetos/00-incolumepy/
source py35/bin/activate
pytest -v incolumepy/testes/tests_automatizados/testes_avulsos.py
'''


def test_myoutput(capsys):  # or use "capfd" for fd-level
    print("hello")
    sys.stderr.write("world\n")
    out, err = capsys.readouterr()
    assert out == "hello\n"
    assert err == "world\n"
    print("next")
    out, err = capsys.readouterr()
    assert out == "next\n"


# --------------
def funcao_sem_retorno(value=''):
    if value != '':
        print(value)


def test_funcao_sem_retorno0():
    assert funcao_sem_retorno() == None


def test_funcao_sem_retorno1():
    assert funcao_sem_retorno('ok') == None


def test_funcao_sem_retorno2(capsys):
    funcao_sem_retorno('ok')
    out, err = capsys.readouterr()
    assert out == 'ok\n'
    assert err == ''


def test_funcao_sem_retorno3(capsys):
    assert funcao_sem_retorno('ok') == None
    out, err = capsys.readouterr()
    assert out == 'ok\n'
    assert err == ''

# --------------
def makeSomething(*args):
    if not args:
        return None
    return args


def test_makeSomething0():
    something = makeSomething('ok')
    assert something is not None


def test_makeSomething1():
    something = makeSomething()
    assert something is None


# --------------
def notbool(b=bool()):
    return not b


def test_notbool0():
    assert notbool(True) == False


def test_notbool1():
    assert notbool(False) == True


# --------------
def test_zero_div():
    with pytest.raises(ZeroDivisionError):
        raise ZeroDivisionError()


# --------------
def func(id, name):
    return id, name


def test_func0():
    id, name = func(1, '1')
    assert type(id) is int, "id is not an integer: %r" % id
    assert type(name) is str, "name is not a string: %r" % name


def test_func1():
    id, name = func('1', 1)
    assert type(id) is not int, "id is not an integer: %r" % id
    assert type(name) is not str, "name is not a string: %r" % name


# --------------
def value_error():
    raise ValueError('could not convert string to float')


def test_value_error0():
    with pytest.raises(ValueError) as e:
        value_error()
        e.match(r'could not convert string to float')


# --------------
def float_fail(msg):
    raise AttributeError(msg)


def test_float_fail():
    with pytest.raises(AttributeError) as e:
        float_fail('')
        e.match(r'')


def test_float_fail1():
    with pytest.raises(AttributeError) as e:
        float_fail('Float fail')
        e.match(r'Float fail')


# ----------------
def myfunc():
    raise ValueError("Exception 123 raised")


def test_match():
    with pytest.raises(ValueError) as excinfo:
        myfunc()
    excinfo.match(r'.* 123 .*')


# ----
def test_set_comparison():
    set1 = set("1308")
    set2 = set("8031")
    assert (set1 != set2) == False


def test_set_comparison1():
    set1 = set("1308")
    set2 = set("8031")
    assert set1 not in set2


def test_set_comparison2():
    set1 = set("1308")
    set2 = set("8031")
    assert (set1 is not set2) == True


def test_set_comparison3():
    set1 = set("1308")
    set2 = set("8031")
    assert (set1 == set2) == True
