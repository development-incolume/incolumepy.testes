import os
import platform

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

if platform.system() == 'Windows':
    chromebin = r'C:\Users\ricardobn\AppData\Local\Programs\Python\Python36-32\selenium\webdriver\chromedriver.exe'
    firefoxbin = r'C:\Users\ricardobn\AppData\Local\Programs\Python\Python36-32\selenium\webdriver\geckodriver.exe'
    encode = 'iso8859-1'
elif platform.system() == 'Linux':
    chromebin = '../handler_HTML_XML/drivers/chromedriver'
    # firefoxbin = FirefoxBinary(os.path.abspath('../handler_HTML_XML/drivers/geckodriver'))
    firefoxbin = os.path.abspath('../handler_HTML_XML/drivers/geckodriver')
    encode = 'utf-8'
else:
    raise OSError('Sistema operacional não identificado.')

print(os.path.isfile(firefoxbin))

driver = webdriver.Firefox(executable_path=firefoxbin)
driver.get("http://www.python.org")
assert "Python" in driver.title
elem = driver.find_element_by_name("q")
elem.clear()
elem.send_keys("pycon")
elem.send_keys(Keys.RETURN)
assert "No results found." not in driver.page_source
driver.close()
