import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


firefoxbin = os.path.abspath('../handler_HTML_XML/drivers/geckodriver')
firefox = webdriver.Firefox(executable_path=firefoxbin)
firefox.get('http://pypi.python.org')
wait = WebDriverWait(firefox, 10)
sign_in = wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "Log in")))
sign_in.click()