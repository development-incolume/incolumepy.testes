import os
from collections import namedtuple

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait

Atos = namedtuple('Ato', 'nome numero ano origem situacao texto')

class Scrapy_camara:
    def __init__(self, ano=1900, driver='../handler_HTML_XML/drivers/geckodriver'):
        options = webdriver.FirefoxOptions()
        options.add_argument('headless')
        options.add_argument('window-size=800x600')
        self.options = options
        self.firefoxbin = os.path.abspath(driver)
        self.firefox = webdriver.Firefox(executable_path=self.firefoxbin)
        self.ano = ano
        self.l_atos = []
        self.queryid = None
        self.queryreq = []


    def query_form(self):
        '''
        busca de leis
        :return: return and write a json file
        '''
        self.queryid = self.firefox.window_handles
        self.firefox.get('http://www2.camara.leg.br/atividade-legislativa/legislacao/pesquisa/avancada')
        container = self.firefox.find_element_by_id('expressao')
        container.send_keys('leis')
        container = self.firefox.find_element_by_id('ano')
        container.send_keys(self.ano)
        self.firefox.find_element_by_id('apelido').click()
        self.firefox.find_element_by_id('Ilei').click()
        self.firefox.find_element_by_id('Ileicom').click()
        self.firefox.find_element_by_id('Ideclei').click()
        self.firefox.find_element_by_id('Idecret').click()

        # É preciso passar o elemento para a classe
        origem = Select(self.firefox.find_element_by_name('origensF'))
        # Selecionar a opção pelo texto
        origem.select_by_visible_text('(Todas)')

        # É preciso passar o elemento para a classe
        situacao = Select(self.firefox.find_element_by_id('situacao'))
        # Selecionar a opção pelo valor
        situacao.select_by_value('')

        frame = container.send_keys(Keys.ENTER)
        self.firefox.save_screenshot('tela.png')

        print('frame: {}'.format(frame))
        print('title: {}'.format(self.firefox.title))
        print('url: '.format(self.firefox.current_url))
        print('id: {}'.format(self.queryid))

        # self.firefox.forward()
        print('confirmaçao id: {}'.format(self.firefox.window_handles))
        print('frame: {}'.format(self.firefox.switch_to_frame(frame_reference=frame)))

        body = self.firefox.find_element_by_tag_name("body")
        body.send_keys(Keys.CONTROL + 't')

        wait = WebDriverWait(self.firefox, 10)
        wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "Próxima")))

        #assert "busca" in self.firefox.title
        atos = []
        #atos = self.firefox.find_element_by_name('li')
        atos = self.firefox.find_element_by_class_name('titulo')
        #atos = self.firefox.find_element(By.CSS_SELECTOR, 'li.content')

        for ato in atos:
            d = {}
            d['title'] = ato.text
            d['link'] = ato.find_element_by_name('a').get_attribute('href')
            self.l_atos.append(d)


        print(atos)

        for ato in atos:
            print(ato)

        assert "No results found." not in self.firefox.page_source
        self.firefox.close()

    @staticmethod
    def run():
        a = Scrapy_camara()
        a.query_form()

if __name__ == '__main__':
    Scrapy_camara.run()
    pass
