import json
import locale
import logging
import logging.handlers
import os
import platform
import re
from collections import namedtuple
from copy import copy
from datetime import datetime
from incolumepy.utils.files import realfilename
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait

Atos = namedtuple('Ato', 'nome numero ano date origem situacao relacionada link_elem_txt elem_txt')


class Scrapy_camara:
    def __init__(self, ano=1900, driver='../handler_HTML_XML/drivers/geckodriver', filelog=None, filejson=None):
        self.firefoxbin = os.path.abspath(driver)
        self.firefox = webdriver.Firefox(executable_path=self.firefoxbin)
        self.ano = ano
        self.l_atos = []
        self.collection_atos = []
        self.collection_json = {}
        self.filelog = filelog
        self.filejson = '{}.json'.format(os.path.basename(__file__).split('.')[0])
        if filejson:
            self.filejson = filejson

    def atos2list(self, container_atos):
        for ato in container_atos:
            d = {}
            d['title'] = ato.text
            d['link'] = ato.find_element_by_tag_name('a').get_attribute('href')
            self.l_atos.append(d)

    def query_form(self):
        '''
        busca de leis
        :return: return and write a json file
        '''
        self.queryid = self.firefox.window_handles
        self.firefox.get('http://www2.camara.leg.br/atividade-legislativa/legislacao/pesquisa/avancada')
        logging.debug(self.firefox.current_url)
        container = self.firefox.find_element_by_id('expressao')
        container.send_keys('leis')
        container = self.firefox.find_element_by_id('ano')
        container.send_keys(self.ano)
        self.firefox.find_element_by_id('apelido').click()
        self.firefox.find_element_by_id('Ilei').click()
        self.firefox.find_element_by_id('Ileicom').click()
        self.firefox.find_element_by_id('Ideclei').click()
        self.firefox.find_element_by_id('Idecret').click()

        # É preciso passar o elemento para a classe
        origem = Select(self.firefox.find_element_by_name('origensF'))
        # Selecionar a opção pelo texto
        origem.select_by_visible_text('(Todas)')

        # É preciso passar o elemento para a classe
        situacao = Select(self.firefox.find_element_by_id('situacao'))
        # Selecionar a opção pelo valor
        situacao.select_by_value('')
        container.send_keys(Keys.ENTER)
        logging.debug('consultas selecionadas')

        print('title: {}'.format(self.firefox.title))
        logging.debug('title: {}'.format(self.firefox.title))
        logging.debug('url: '.format(self.firefox.current_url))
        logging.debug('id: {}'.format(self.queryid))

        # self.firefox.forward()
        print('confirmaçao id: {}'.format(self.firefox.window_handles))

        body = self.firefox.find_element_by_tag_name("body")
        body.send_keys(Keys.CONTROL + 't')

        wait = WebDriverWait(self.firefox, 10)
        wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "Próxima")))

        print(self.firefox.title)
        # assert "busca" in self.firefox.title
        atos = self.firefox.find_elements_by_class_name('titulo')
        self.atos2list(atos)
        while True:
            try:
                self.firefox.find_element_by_link_text('Próxima').click()
                self.atos2list(self.firefox.find_elements_by_class_name('titulo'))
            except:
                break

        print('l_atos', len(self.l_atos))
        logging.debug('l_atos', len(self.l_atos))
        self.collection_json['result_query'] = copy(self.l_atos)
        print('>>>', len(self.collection_json['result_query']))
        print('collection_json', (self.collection_json))
        print('')
        for i in self.l_atos:
            print(i)
            logging.debug(i)

        for j, o in enumerate(self.collection_json['result_query']):
            print(j, o)

        assert "No results found." not in self.firefox.page_source
        # self.firefox.close()

    def date_conv(self, date):
        '''
                convert date to timestamp
                :param date: ' de 8 de Janeiro de 1900'
                :return: 1900/1/8
        '''
        if platform.system() == 'Linux':
            locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
        else:
            locale.setlocale(locale.LC_ALL, 'pt_BR')

        try:
            return datetime.strptime(date, ' de %d de %B de %Y').strftime('%Y/%m/%d')
        except ValueError:
            return datetime.strptime(date, ' de %dº de %B de %Y').strftime('%Y/%m/%d')

    def decode_obj(self, obj_ato):
        '''

        :return:
        '''
        head = 'nome numero ano date origem situacao relacionada link_elem_txt elem_txt'.split()
        ato = {}
        l_atos = []
        for i, j in zip(head, tuple(obj_ato)):
            print(i, j)
            ato[i] = j
            l_atos.append(ato)
            self.collection_json['atos'] = l_atos
        print(l_atos)
        return l_atos

    def fill_Atos(self, onfile=True):
        o = Atos
        et = {}
        if onfile:
            with open(self.filejson) as file:
                query = json.load(file)['result_query']
        else:
            query = self.l_atos

        for n, elem in enumerate(query):
            print('#',n, elem)
            o.name = elem['title'].split(',')[0].split()[0].lower()
            o.numero = '{:0>5}'.format(''.join([i for i in elem['title'].split(',')[0] if i.isdigit()]))
            o.ano = elem['title'].split()[-1]
            o.date = self.date_conv(elem['title'].split(',')[1])
            o.situacao = {}
            url = elem['link'].strip()
            logging.debug('\n', url)
            self.firefox.get(url)
            container = self.firefox.find_elements_by_class_name('sessao')
            for i in container:
                elem = i.text
                print('elem', elem)
                if re.match('TEXTO - PUBLICAÇÃO ORIGINAL', elem, flags=re.I):
                    logging.debug('@', i.tag_name)
                    logging.debug('@', i.find_elements_by_tag_name('a'))
                    logging.debug('@', i.find_elements_by_tag_name('a')[0].get_attribute('href'))
                    et['link'] = i.find_elements_by_tag_name('a')[0].get_attribute('href')

                if re.match('origem', elem, flags=re.I):
                    o.origem = elem.split(': ')[-1]
                if re.match('situação', elem, flags=re.I):
                    o.situacao['status'] = elem.split(': ')[-1]

            container = self.firefox.find_element_by_partial_link_text('Texto - Publicação Original')
            o.link_elem_txt = container.get_attribute('href')
            et['link'] = container.get_attribute('href')
            print('-' * 20)
            container = self.firefox.find_elements_by_class_name('listaMarcada')
            # desconsidera a #1 ocorrencia .listaMarcada
            elem = []
            for i in container[1:]:
                d = {}
                print(i.tag_name, i.text)
                try:
                    d['title'] = re.split('\(|\)', i.text)[0].strip()
                    d['link'] = i.find_elements_by_tag_name('a')[0].get_attribute('href')
                    elem.append(d)
                except:
                    print('l.160')
                    pass
            logging.debug(elem)
            o.situacao['relacionada'] = elem

            logging.debug('Ato nome {}, numero {}, data {}, '
                          'origem {}, situação {}, relacionamento {}, link_elemento {}, elem. texto {}'.format(
                o.name, o.numero, o.date, o.origem, o.situacao,
                o.relacionada, o.link_elem_txt, o.elem_txt))


            print('\nAto \nnome: {}, \nnumero: {}, \ndata: {}, \n'
                  'origem: {}, \nsituação: {}, \nrelacionamento: {}, '
                  '\nelem. texto: {}\n link elem: {}\n'.format(
                o.name, o.numero, o.date, o.origem, o.situacao,
                o.relacionada, o.link_elem_txt, o.elem_txt))

            self.decode_obj(o)

            self.collection_atos.append(o)
        print(len(self.collection_atos))

    def collection2jsonfile(self, fileout=None):
        if not fileout:
            #fileout = '{}.json'.format(os.path.basename(__file__).split('.')[0])
            fileout = self.filejson
        with open(realfilename(fileout), 'w') as f:
            json.dump(self.collection_json, f, sort_keys=True, indent=4, ensure_ascii=False)
            pass

    def fill_content(self, collection=None):
        if not collection:
            collection = self.collection_atos

        for i, ato in enumerate(collection):
            print(i, (ato))
            print(tuple(ato))
        pass

    @staticmethod
    def run():
        a = Scrapy_camara()
        a.filejson='selenium_multaccess03a.json'
        try:
            logging.debug('starting...')
            #a.query_form()
            a.fill_Atos()
            # a.fill_content()
            a.collection2jsonfile()
        except (AttributeError) as e:
            logging.error(e)
            raise
            print(e)
        finally:
            a.firefox.quit()
            pass


if __name__ == '__main__':
    Scrapy_camara.run()
    # Scrapy_camara.wlogs()
    pass
