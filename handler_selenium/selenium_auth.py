from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time, os
'''https://imasters.com.br/desenvolvimento/autenticacao-em-um-website-usando-selenium-python-exemplo-de-selenium-python/?trace=1519021197&source=single'''

firefoxbin = os.path.abspath('../handler_HTML_XML/drivers/geckodriver')


browser = webdriver.Firefox(executable_path=firefoxbin)
browser.get("https://lbjcrs.ust.hk/primo/authen.php")
time.sleep(10)

username = browser.find_element_by_id("extpatid")
password = browser.find_element_by_id("extpatpw")
username.send_keys("username")
password.send_keys("password")

login_attempt = browser.find_element_by_xpath("//*[@type='submit']")
login_attempt.submit()
