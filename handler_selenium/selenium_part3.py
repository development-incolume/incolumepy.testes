import os

from selenium import webdriver
from selenium.common.exceptions import WebDriverException


def js_event():
    firefoxbin = os.path.abspath('../handler_HTML_XML/drivers/geckodriver')
    firefox = webdriver.Firefox(executable_path=firefoxbin)
    firefox.get('http://google.com.br/')
    try:
        firefox.execute_script('alert("código javascript executando")')
        print('exec 1')
        firefox.execute_async_script('alert("código javascript sendo executado")')
        print('exec 2')
    except WebDriverException as e:
        print(e)

if __name__ == '__main__':
    pass
    js_event()
