# http://pythonclub.com.br/selenium-parte-1.html#instalacao
__authon__ = '@britodfbr'
import os
import platform

from selenium import webdriver

if platform.system() == 'Windows':
    chromebin = r'C:\Users\ricardobn\AppData\Local\Programs\Python\Python36-32\selenium\webdriver\chromedriver.exe'
    firefoxbin = r'C:\Users\ricardobn\AppData\Local\Programs\Python\Python36-32\selenium\webdriver\geckodriver.exe'
    encode = 'iso8859-1'
elif platform.system() == 'Linux':
    chromebin = '../handler_HTML_XML/drivers/chromedriver'
    firefoxbin = os.path.abspath('../handler_HTML_XML/drivers/geckodriver')
    encode = 'utf-8'
else:
    raise OSError('Sistema operacional não identificado.')

firefox = webdriver.Firefox(executable_path=firefoxbin)

# Abrir google
firefox.get('http://google.com.br')

# Abrir o site da Python Brasil
firefox.get('http://python.org.br/')

# Abrir o site do meu blog
firefox.get('http://brito.blog.incolume.com.br')

# Abrir o site da camara.gov.br
firefox.get('http://www2.camara.leg.br/atividade-legislativa/legislacao/pesquisa/avancada')

# Encontrar elemento pelo ID
#  Se o elemento não for encontrado uma exception é gerada
firefox.find_element_by_id('ano')

# Encontrar elemento pelo atributo name
firefox.find_element_by_name('expressao')

# Encontrar elemento pelo texto do link
firefox.find_element_by_link_text('Selecionar todos')

# Encontrar elemento pelo seu seletor css
# firefox.find_element_by_css_selector('<css_selector>')

# Encontrar elementos pelo nome da tag
firefox.find_elements_by_tag_name('form')

# Encontrar elementos pela classe
firefox.find_elements_by_class_name('btn btn-primary')
