import os

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select


def event_click():
    firefoxbin = os.path.abspath('../handler_HTML_XML/drivers/geckodriver')
    firefox = webdriver.Firefox(executable_path=firefoxbin)
    firefox.get('http://google.com.br/')

    # pegar o campo de busca onde podemos digitar algum termo
    campo_busca = firefox.find_element_by_name('q')

    # Digitar "Python Club" no campo de busca
    campo_busca.send_keys('Python Club')

    # Simular que o enter seja precisonado
    campo_busca.send_keys(Keys.ENTER)


def event_select():
    firefoxbin = os.path.abspath('../handler_HTML_XML/drivers/geckodriver')
    firefox = webdriver.Firefox(executable_path=firefoxbin)
    firefox.get('http://www2.camara.leg.br/atividade-legislativa/legislacao/pesquisa/avancada')

    # É preciso passar o elemento para a classe
    situacao = Select(firefox.find_element_by_name('situacao'))

    # Selecionar a opção pelo texto
    situacao.select_by_visible_text('(Todas)')

    # Selecionar a opção pelo valor
    situacao.select_by_value('Revogada')


def event_button():
    firefoxbin = os.path.abspath('../handler_HTML_XML/drivers/geckodriver')
    firefox = webdriver.Firefox(executable_path=firefoxbin)
    firefox.get('http://www2.camara.leg.br/atividade-legislativa/legislacao/pesquisa/avancada')
    situacao = Select(firefox.find_element_by_name('situacao'))
    situacao.select_by_value('Revogada')

    # firefox.find_element_by_id('<botao_id>')
    # Precisamos antes encontrar o frame
    frame = firefox.find_element_by_id('FormLegin')
    # frame = firefox.find_elements_by_class_name('btn btn-primary')

    # Vamos alterar para ele
    firefox.switch_to_frame(frame)
    # firefox.switch_to.frame()

    # Agora podemos encontrar o elemento
    botao = firefox.find_element_by_xpath('/html/body/div[3]/div[2]/div/div[2]/div[3]/form/div[3]/input[1]')

    # E podemos manipulá-lo :)
    botao.click()

def event_click01():
    firefoxbin = os.path.abspath('../handler_HTML_XML/drivers/geckodriver')
    firefox = webdriver.Firefox(executable_path=firefoxbin)
    firefox.get('http://www.camara.leg.br')

    campo_busca = firefox.find_element_by_id('input-ProjetoAnoLeg')
    campo_busca.send_keys('1900')
    campo_busca.send_keys(Keys.ENTER)
    print(firefox.window_handles)


def change_window():
    firefoxbin = os.path.abspath('../handler_HTML_XML/drivers/geckodriver')
    firefox = webdriver.Firefox(executable_path=firefoxbin)
    firefox.get('http://brito.blog.incolume.com.br')
    firefox.get('http://google.com')

    print(firefox.window_handles)
    # [u'{7fd12d82-4fb3-48a4-a8b9-e1e460c00236}', u'{2ce4de19-0902-48e3-a1cc-50f6378afd79}']

    # Título da janela atual
    print(firefox.title)
    # u'Google'

    # Trocar para a última janela da lista
    firefox.switch_to_window_handles(firefox.window_handles[-1])
    print(firefox.title)
    # u'PythonClub //'

    # Fechar a janela atual
    firefox.close()

    # Voltar para a janela do Google
    firefox.switch_to_window_handles(firefox.window_handles[-1])
    print(firefox.title)

if __name__ == '__main__':
    event_select()
    event_click01()
    # change_window() not ok
    #event_button()
