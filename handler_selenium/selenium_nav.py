import os
import json
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import WebDriverException


def fill_form():
    firefoxbin = os.path.abspath('../handler_HTML_XML/drivers/geckodriver')
    firefox = webdriver.Firefox(executable_path=firefoxbin)
    firefox.get('http://www2.camara.leg.br/atividade-legislativa/legislacao/pesquisa/avancada')

    container = firefox.find_element_by_id('expressao')
    container.send_keys('leis')
    container = firefox.find_element_by_id('ano')
    container.send_keys('1900')
    firefox.find_element_by_id('apelido').click()
    firefox.find_element_by_id('Ilei').click()
    firefox.find_element_by_id('Ileicom').click()
    firefox.find_element_by_id('Ideclei').click()
    firefox.find_element_by_id('Idecret').click()
    container.send_keys(Keys.ENTER)

    atos = firefox.find_elements_by_class_name('titulo')
    print(atos)


if __name__ == '__main__':
    fill_form()
    pass