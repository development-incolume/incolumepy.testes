import json
import os

from selenium import webdriver

firefoxbin = os.path.abspath('../handler_HTML_XML/drivers/geckodriver')


def pythonclub():
    # Criar instância do navegador
    firefox = webdriver.Firefox(executable_path=firefoxbin)

    # Abrir a página do Python Club
    firefox.get('http://pythonclub.com.br/')

    # Seleciono todos os elementos que possuem a class post
    posts = firefox.find_elements_by_class_name('post')

    # Para cada post printar as informações
    for post in posts:
        # O elemento `a` com a class `post-title`
        # contém todas as informações que queremos mostrar
        post_title = post.find_element_by_class_name('post-title')

        # `get_attribute` serve para extrair qualquer atributo do elemento
        post_link = post_title.get_attribute('href')

        # printar informações
        print("Títutlo: {titulo}, \nLink: {link}".format(
            titulo=post_title.text,
            link=post_link
        ))

        # Fechar navegador
        firefox.quit()


def desafio01():
    '''Modificar o exemplo para mostrar o nome do autor do post.'''
    firefox = webdriver.Firefox(executable_path=firefoxbin)
    firefox.get('http://pythonclub.com.br/')
    posts = firefox.find_elements_by_class_name('post')
    for post in posts:
        post_title = post.find_element_by_class_name('post-title').text
        post_link = post.find_element_by_class_name('post-title').get_attribute('href')
        post_autor = post.find_element_by_class_name('avatar').get_attribute('alt')
        print('Título: {} \nAutor: {} \nLink: {} \n'.format(post_title, post_autor, post_link))
    firefox.close()


def desafio02():
    '''Modificar o exemplo 01 para salvar os dados(titulo, link, autor) em um arquivo json.'''
    firefox = webdriver.Firefox(executable_path=firefoxbin)
    firefox.get('http://pythonclub.com.br/')
    result = []
    posts = firefox.find_elements_by_class_name('post')
    for post in posts:
        d = {}
        post_title = post.find_element_by_class_name('post-title').text
        post_link = post.find_element_by_class_name('post-title').get_attribute('href')
        post_autor = post.find_element_by_class_name('avatar').get_attribute('alt')
        d['title'] = post_title
        d['autor'] = post_autor
        d['link'] = post_link
        result.append(d)
    firefox.close()
    with open('desafio02.json', 'w') as f:
        json.dump(result, f, sort_keys=True, indent=2, ensure_ascii=False)


if __name__ == '__main__':
    pass
    # pythonclub()
    desafio01()
    desafio02()
