import datetime
import json
import os
from collections import namedtuple

Atos = namedtuple('Ato', 'nome numero ano date origem situacao relacionada link_elem_txt elem_txt')

class Class:
    def __init__(self):
        self.fjson = '{}.json'.format(os.path.basename(__file__).split('.')[0])
        self.l_obj = []
        self.l_atos = []

    def cria_obj(self):
        for i in range(20):
            o = Atos(
                'decreto',
                i + 100,
                datetime.date.today().strftime('%Y'),
                datetime.date.today().strftime('%Y/%m/%d'),
                'executivo',
                'revogada',
                {'link': '@{}'.format(i), 'title': '#@{}'.format(i)},
                '',
                {'link': 'url{}'.format(i), 'content': 'conteudo{}'.format(i)}
            )
            self.l_obj.append(o)
            print(tuple(o))

    def decode_obj(self):
        head = 'nome numero ano date origem situacao relacionada link_elem_txt elem_txt'.split()
        l_atos = []
        for o in self.l_obj:
            ato = {}
            for i, j in zip(head, tuple(o)):
                print(i, j)
                ato[i] = j
            l_atos.append(ato)
        print(l_atos)
        self.l_atos = l_atos

    def writejson(self):
        filename = '{}.json'.format(os.path.basename(__file__).split('.')[0])
        atos = {}
        atos['atos'] = self.l_atos
        with open(filename, 'w') as fout:
            json.dump(atos, fout, sort_keys=True, indent=2, ensure_ascii=False)

    def readjson(self):
        filename = '{}.json'.format(os.path.basename(__file__).split('.')[0])
        with open(filename) as file:
            atos = json.load(file)
            print('\n\n\n\n\n')
            print(atos)
            for i, ato in enumerate(atos['atos']):
                print(i, ato)




    def fill_Atos(self, fjson=None):
        if fjson:
            self.fjson = fjson
        print(self.fjson)
        self.cria_obj()
        self.decode_obj()
        self.writejson()
        self.readjson()
        pass

if __name__ == '__main__':
    a = Class()
    a.fill_Atos(fjson='queryresult2json.json')
    b = Class()
    b.fill_Atos()
