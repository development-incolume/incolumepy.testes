import json
import os
import re
from pprint import pprint as pp

from bs4 import BeautifulSoup as bs
from selenium import webdriver

from incolumepy.utils.files import realfilename

with open('selenium_multaccess03c.json') as file:
    fjson = json.load(file)

atos = fjson['atos']
print(atos)
l = []
for ato in atos:
    try:
        print(ato)
        print('{} {}'.format(ato['nome'], ato['numero']))
        print(ato['elem_txt']['url'])
        firefoxbin = '../handler_HTML_XML/drivers/geckodriver'
        firefox = webdriver.Firefox(executable_path=firefoxbin)
        firefox.get(ato['elem_txt']['url'])
        container = firefox.find_elements_by_id('content')
        print(len(container))
        source = firefox.page_source.replace('\n', '')
        for j in range(220):
            source = re.sub('\t', ' ', source, flags=re.I)
            source = source.replace('   ', ' ')
            source = source.replace('  ', ' ')

        soup = bs(source, 'lxml')
        print(soup.find_all(id='content'))
        ato['elem_txt']['content'] = soup.find_all(id='content')
        l.append(ato)

    finally:
        firefox.quit()

fjson['atos'] = l
pp(fjson)
with open(realfilename(os.path.basename(__file__), ext='json'), 'w') as file:
    json.dump(fjson, file, ensure_ascii=False, indent=2, sort_keys=True)
