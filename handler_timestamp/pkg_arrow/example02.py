from datetime import datetime

import arrow
from dateutil import tz

# http://arrow.readthedocs.io/en/latest/


print('get now time')
print(arrow.utcnow())
# <Arrow [2013-05-07T04:20:39.369271+00:00]>

print(arrow.now())
# <Arrow [2013-05-06T21:20:40.841085-07:00]>

print(arrow.now('US/Pacific'))
# <Arrow [2013-05-06T21:20:44.761511-07:00]>


print('\n# Create from timestamps (ints or floats, or strings that convert to a float):')

print('arrow.get(1367900664):', arrow.get(1367900664))
# <Arrow [2013-05-07T04:24:24+00:00]>

print("arrow.get('1367900664'):", arrow.get('1367900664'))
# <Arrow [2013-05-07T04:24:24+00:00]>

print('arrow.get(1367900664.152325):', arrow.get(1367900664.152325))
# <Arrow [2013-05-07T04:24:24.152325+00:00]>

print("arrow.get('1367900664.152325'):", arrow.get('1367900664.152325'))
# <Arrow [2013-05-07T04:24:24.152325+00:00]>


print('\nUse a naive or timezone-aware datetime, or flexibly specify a timezone:')

print('arrow.get(datetime.utcnow()): ', arrow.get(datetime.utcnow()))
# <Arrow [2013-05-07T04:24:24.152325+00:00]>

print("arrow.get(datetime(2013, 5, 5), 'US/Pacific'): ",
      arrow.get(datetime(2013, 5, 5), 'US/Pacific'))
# <Arrow [2013-05-05T00:00:00-07:00]>

print("arrow.get(datetime(2013, 5, 5), tz.gettz('US/Pacific')): ",
      arrow.get(datetime(2013, 5, 5), tz.gettz('US/Pacific')))
# <Arrow [2013-05-05T00:00:00-07:00]>

print("arrow.get(datetime.now(tz.gettz('US/Pacific'))): ",
      arrow.get(datetime.now(tz.gettz('US/Pacific'))))
# <Arrow [2013-05-06T21:24:49.552236-07:00]>
