# https://stackoverflow.com/questions/415511/how-to-get-current-time-in-python
import datetime
import time


def execute01():
    return datetime.datetime.now()
    '2017-07-27 11:14:03.280206'


def execute02():
    return datetime.datetime(2009, 1, 6, 15, 8, 24, 78915)


def execute03():
    return datetime.datetime.time(datetime.datetime.now())


def execute04():
    return datetime.time(15, 8, 24, 78915)


def execute05():
    return datetime.datetime.now().time()


def execute06():
    return time.gmtime()


def execute07():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())


def execute08():
    return datetime.datetime.now()


def execute09():
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


def execute10():
    ''':return epoch'''
    return time.time()


def execute11():
    return time.ctime()


def execute12():
    return (time.ctime(1424233311.771502), time.ctime(1501165624.0438251))


def execute13():
    pass


def execute14():
    pass


def execute15():
    pass


def execute16():
    pass


if __name__ == '__main__':
    print(execute01())
    print(execute02())
    print(execute03())
    print(execute04())
    print(execute05())
    print(execute06())
    print(execute07())
    print(execute08())
    print(execute09())
    print(execute10())
    print(execute11())
    print(execute12())
