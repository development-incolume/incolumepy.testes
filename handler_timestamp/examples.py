import datetime
import time


def gen_timestamp01():
    '''2017-07-11 21:57:00'''
    while 1:
        yield datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')


def gen_timestamp02():
    '''2017-07-12 00:57:00.918545'''
    while 1:
        yield datetime.datetime.utcnow()


def gen_timestamp03():
    '''2017-07-11 21:57:00.918561'''
    while 1:
        yield datetime.datetime.now()


def gen_timestamp04():
    '''2017-07-11T21:57:00.918574'''
    while 1:
        yield datetime.datetime.now().isoformat()


def gen_timestamp05():
    '''2017-07-12 00:57:00.918585'''
    while 1:
        yield datetime.datetime.utcfromtimestamp(time.time())


if __name__ == '__main__':
    a = gen_timestamp01()
    b = gen_timestamp02()
    c = gen_timestamp03()
    d = gen_timestamp04()
    e = gen_timestamp05()

    print(next(a))
    print(next(b))
    print(next(c))
    print(next(d))
    print(next(e))
