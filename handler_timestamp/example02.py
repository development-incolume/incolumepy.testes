import datetime
import time


def execute01():
    str_date = 'Thu Jul 27 13:54:22 2017'
    return datetime.datetime.strptime(str_date, "%a %b %d %H:%M:%S %Y").strftime("%d/%m/%Y")


def execute02():
    import time
    from_date = "Mon Feb 15 2010"
    conv = time.strptime(from_date, "%a %b %d %Y")
    return time.strftime("%d/%m/%Y", conv)


def execute03():
    import time
    a = time.strptime('Thu Jul 27 13:54:22 2017', "%a %b %d %H:%M:%S %Y")
    return time.strftime('%Y/%m/%d', a)


def execute04():
    '''converte string para timestamp'''
    return time.strftime('%Y/%m/%d', time.strptime(
        'Thu Jul 27 13:54:22 2017', "%a %b %d %H:%M:%S %Y"))


if __name__ == '__main__':
    print(execute01())
    print(execute02())
    print(execute03())
    print(execute04())
