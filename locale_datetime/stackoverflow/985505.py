import datetime
import locale
import platform
import sys

if sys.platform == 'win32':
    locale.setlocale(locale.LC_ALL, 'pt_BR')
else:
    locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')

if platform.system() == 'Linux':
    locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
else:
    locale.setlocale(locale.LC_ALL, 'pt_BR')

print(datetime.date.today().strftime("%B %Y"))
