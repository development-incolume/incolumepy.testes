lista = [1, 11]
tupla = (1, 11)
dic = {'a': 1, 'b': 11}

print([x ** 2 for x in range(*lista)])
print([x ** 2 for x in range(*tupla)])
print([x ** 2 for x in range(*dic.values())])
# print(list(range(**dic)))
