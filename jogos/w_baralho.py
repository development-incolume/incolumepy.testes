#https://gist.githubusercontent.com/valdergallo/5166917/raw/b48e832e1587f4589f01fa2ced6e8b176e7309a7/baralho.py

from random import shuffle

class Carta(object):

    def __init__(self, valor, naipe):
        self.valor = valor
        self.naipe = naipe

    def __repr__(self):
        return '<%s de %s>' % (self.valor, self.naipe)



class Baralho(object):
    naipes = 'paus copas espadas ouros'.split()
    valores = 'A K J Q 10 9 8 7 6 5 4 3 2'.split(' ')

    def __init__(self):
        self.cartas = [Carta(v,n) for n in self.naipes for v in self.valores]

    def __len__(self):
        return len(self.cartas)

    def __getitem__(self, pos):
        return self.cartas[pos]

    def __setitem__(self, pos, valor):
        self.cartas[pos] = valor

    def embaralhar(self):
        shuffle(self.cartas)

if __name__ == '__main__':
    c = Baralho()
    print(c.cartas)
    print(len(c))