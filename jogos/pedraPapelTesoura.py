import random

print("\n=== Jogo Pedra, Papel e Tesoura ===\n")

OPCOES = ['pedra', 'papel', 'tesoura']

NUM_OPCOES = len(OPCOES)


def nome_num(nome):
    try:
        return OPCOES.index(nome.lower())
    except:
        return None


def num_nome(numero):
    try:
        return OPCOES[int(numero)]
    except:
        return None


def imprimir_resultado(escolhajogador, escolhapc):
    print("O jogador escolheu :", escolhajogador)
    print("O computador escolheu: ", escolhapc)


def ppt(opcao):
    # converte o texto em numero
    numjogador = nome_num(opcao)
    numcomputador = random.randrange(0, NUM_OPCOES)
    diferenca = (numjogador - numcomputador) % NUM_OPCOES

    imprimir_resultado(opcao, num_nome(numcomputador))

    # diferencas

    if (diferenca == 0):
        print("EMPATE")
    elif diferenca == 1:
        print("Parabens, voce venceu")
    else:
        print("O computador venceu")

    print()


def main():
    ppt(input('pedra, papel ou tesoura? '))

if __name__ == '__main__':
    DEBUG = 0
    if DEBUG: print(NUM_OPCOES)
    if DEBUG: print(nome_num('PAPEL'))
    if DEBUG: print(nome_num('Tesoura'))
    if DEBUG: print(num_nome('0'))

    ppt("tesoura")
    ppt("tesoura")
    ppt("tesoura")
    ppt("tesoura")
    ppt("papel")
    ppt("pedra")
    main()
