#https://gist.githubusercontent.com/valdergallo/5166917/raw/b48e832e1587f4589f01fa2ced6e8b176e7309a7/baralho.py

from random import shuffle

class Carta():
    naipes = 'Paus Ouros Copas Espadas'.split()
    valores = 'A 2 3 4 5 6 7 8 9 10 J Q K'.split()

    def __init__(self, valor, naipe):
        self.valor = valor
        self.naipe = naipe

    def __repr__(self):
        return '<%s de %s>' % (self.valor, self.naipe)

    def __cmp__(self, other):
        if self.valores.index(self.valor) < self.valores.index(other.valor): return -1
        if self.valores.index(self.valor) > self.valores.index(other.valor): return 1
        if self.naipes.index(self.naipe) < self.naipes.index(other.naipe): return -1
        if self.naipes.index(self.naipe) > self.naipes.index(other.naipe): return 1
        return 0

    def __lt__(self, other):
        if self.__cmp__(other) == -1:
            return True
        return False

    def __eq__(self, other):
        if self.__cmp__(other) == 0:
            return True
        return False


class Baralho():
    def __init__(self):
        self.cartas = [Carta(v,n) for n in Carta.naipes for v in Carta.valores]

    def __len__(self):
        return len(self.cartas)

    def __getitem__(self, pos):
        return self.cartas[pos]

    def __setitem__(self, pos, valor):
        self.cartas[pos] = valor

    def embaralhar(self):
        shuffle(self.cartas)

    def sort(self):
        sorted(self.cartas, key=lambda x: (x[1], x[0]))

if __name__ == '__main__':
    c = Baralho()
    print(c.cartas)
    print(len(c))

    c.embaralhar()
    print(c.cartas)
    print(sorted(c.cartas))
    print(c.sort())
