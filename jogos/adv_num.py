import random

DEBUG = 0


def main():
    print('Adivinhe o número entre 0 e 100')
    num = random.randrange(0, 101)
    if DEBUG:
        print(num)
    qtentativas = 0
    while True:
        chute = int(input('Digite a tua escolha: '))
        qtentativas += 1
        if chute == num:
            print('Parabéns!! você acertou com {} tentativas.'.format(qtentativas))
            break
        elif chute < num:
            print('Número é maior que o chute.')
        elif chute > num:
            print('Número é menor que o chute.')
        else:
            print('ops...')


if __name__ == '__main__':
    main()
