print("=== Jogo do par ou impar ===")
import random

OPCOES = ['par', 'impar']
NUM_OPCOES = len(OPCOES)


def nome_num(nome):
    try:
        return OPCOES.index(nome.lower())
    except:
        return None


def num_nome(numero):
    try:
        return OPCOES[int(numero)]
    except:
        return None


def imprimir_resultado(escolhajogador, valorjogador, escolhapc):
    print("O jogador escolheu :", escolhajogador)
    print("Valor do Jogador: ", valorjogador, " - Valor do computador: ", escolhapc)


def escolha_computador():
    return random.randrange(0, NUM_OPCOES)


def par_ou_impar(escolhajogador, valorjogador):
    escolhapc = escolha_computador()
    # qual a escolha do player
    num_comparacao = nome_num(escolhajogador)
    imprimir_resultado(escolhajogador, valorjogador, escolhapc)
    # checa se o resultado é do jogador
    if ((valorjogador + escolhapc) % 2 == num_comparacao):

        print("Parabens, voce venceu! O resultado foi ", num_nome(num_comparacao))
    else:

        print("Perdeu! O resultado foi ", num_nome((num_comparacao + 1) % 2))

    print()


def caterva():
    for n in range(1, 11):
        if n % 2:
            print(n)


def main():
    par_ou_impar(input('par ou impar: '), int(input('qual o numero?')))


if __name__ == '__main__':
    print(escolha_computador())
    print('-' * 20)
    caterva()
    print('-' * 20)
    par_ou_impar("par", 4)
    par_ou_impar("par", 4)
    par_ou_impar("par", 4)
    par_ou_impar("par", 4)
    par_ou_impar("par", 4)
    par_ou_impar("par", 5)
    par_ou_impar("impar", 4)
    par_ou_impar("impar", 3)
    main()
