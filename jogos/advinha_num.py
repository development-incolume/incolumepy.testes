import random

NUM_OPCOES = 4


def num_pensado():
    return random.randrange(0, NUM_OPCOES)


def imprime_resultado(escolhido, pensado):
    print("Numero escolhido: %s, Numero pensado: %s." % (escolhido, pensado))


def escolha_num(num_escolhido):
    pensado = num_pensado()
    imprime_resultado(num_escolhido, pensado)
    if num_escolhido == pensado:
        print("Parabéns!!!!")
    else:
        print("Não foi desta vez.")


if __name__ == '__main__':
    print(num_pensado())
    escolha_num(0)
    escolha_num(0)
    escolha_num(0)
