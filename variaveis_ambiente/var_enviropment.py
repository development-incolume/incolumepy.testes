import os
import sys
import platform

if platform.python_version() < '3.0':
    print('Use Python3 ou superior')
    exit(1)

class Var_eviropment:
    DEBUG = 0
    def venv(self):
        try:
            #raise StopIteration('ops')
            self.opsys = platform.system()
            if Var_eviropment.DEBUG: print(self.opsys)

            if platform.system() == 'Linux':
                self.user = os.environ.get('USER','Este Usuário')

            if Var_eviropment.DEBUG: print(self.user)
            return '%s usa %s' %(self.user, self.opsys)
            pass
        except:
            print("%s: %s" % (sys.exc_info()[0], sys.exc_info()[1]))

    def venv02(self):
        try:
            if Var_eviropment.DEBUG: print(sys.platform)
            self.opsys = sys.platform
            self.user = os.environ.get('USER', '')
            return 'O usuário %s usa %s' % (self.user, self.opsys)
        except:
            print(sys.exc_info())

    def metodo01(self):
        os.environ['DEBUSSY'] = 'None'
        print(os.environ.get('DEBUSSY'))
        print(os.environ.get('SESSION'))
        os.environ['SESSION'] = 'Windows'

def main():
    a = Var_eviropment()
    print(a.venv())
    print(a.venv02())
    Var_eviropment().metodo01()

if __name__ == '__main__':
    pass
    main()
