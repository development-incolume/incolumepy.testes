# Docstrings can be customized by making direct assignments to the __doc__ fields:
from collections import namedtuple

Book = namedtuple('Book', ['id', 'title', 'authors'])
Book.__doc__ += ': Hardcover book in active collection'
Book.id.__doc__ = '13-digit ISBN'
Book.title.__doc__ = 'Title of first printing'
Book.authors.__doc__ = 'List of authors sorted by last name'

print(Book.__doc__)
help(Book.title)
help(Book.authors)