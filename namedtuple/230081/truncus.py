from collections import namedtuple

Point = namedtuple('Point', 'x y')

Point3d = namedtuple('Point3d', Point._fields + ('z',))


print(Point3d._fields)

Points = namedtuple('Points', 'x y z')
Points.__new__.__defaults__ = (None, None, 0)

p = Points()
print(p)