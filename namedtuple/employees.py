from collections import namedtuple
import csv
import sqlite3

EmployeeRecord = namedtuple('EmployeeRecord', 'name, age, title, department, paygrade')


for emp in map(EmployeeRecord._make, csv.reader(open("employees.csv", "r"))):
    print('{}: {}'.format(emp.name, emp.title))


conn = sqlite3.connect('employees.dat')
cursor = conn.cursor()
cursor.execute('SELECT name, age, title, department, paygrade FROM employees')
for emp in map(EmployeeRecord._make, cursor.fetchall()):
    print(emp.name, emp.title)