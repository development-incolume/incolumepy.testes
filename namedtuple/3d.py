from collections import namedtuple
'''extender class namedtuple'''

Point = namedtuple('Point', 'x y')
Point3D = namedtuple('Point3D', Point._fields + ('z',))

p3d = Point3D(1, 2, 3)

print(p3d)