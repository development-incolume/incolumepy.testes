# https://gist.github.com/href/1319371

from collections import namedtuple


def convert(dictionary):
    return namedtuple('GenericClass', dictionary.keys())(**dictionary)


d = dict(a=1, b='b', c=[3])

named = convert(d)
print(named)

d2 = {'peso':85, 'name': 'brito', 'alt': 1.78 }
obj = convert(d2)
print(obj)
