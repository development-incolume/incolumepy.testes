'''
https://wiki.python.org.br/RemovedorDeAcentos
'''
import platform
from doctest import testmod
from unicodedata import normalize


class Tratamento_acentos:
    def remover_acentos(self, txt, codif='utf-8'):
        print(txt)
        if platform.python_version() < '3.0':
            return normalize('NFKD', txt.decode(codif)).encode('ASCII','ignore')
        return normalize('NFKD', txt).encode('ASCII','ignore').decode('ASCII')


    @classmethod
    def Main(cls, txt):
        Tratamento_acentos().remover_acentos(txt)
        pass

if __name__ == '__main__':
    testmod()
    print(Tratamento_acentos().remover_acentos('[ACENTUAÇÃO] ç: áàãâä! éèêë? íì&#297;îï, óòõôö; úù&#361;ûü. a'))

    pass


