import unicodedata

print(unicodedata.lookup('LEFT CURLY BRACKET'))
print(unicodedata.name(u'/'))
print(unicodedata.decimal(u'9'))
try:
    print(unicodedata.decimal(u'a'))
except ValueError as e:
    print(e)
except Exception as e:
    print(e)

print(unicodedata.category(u'A'))  # 'L'etter, 'u'ppercase
print(unicodedata.bidirectional(u'\u0660'))  # 'A'rabic, 'N'umber
print(unicodedata.bidirectional(u'疂'))  # 'A'rabic, 'N'umber
