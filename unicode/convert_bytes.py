class ConvertBytes:
    b = [chr(x) for x in range(256)]

    def convert001(self):
        for i in self.b:
            print(i)
        pass

    def convert002(self):
        print(''.join(self.b))

    def convert003(self):
        pass

    @classmethod
    def main(cls):
        cb = ConvertBytes()
        cb.convert001()
        cb.convert002()
        cb.convert003()
        pass


if __name__ == '__main__':
    ConvertBytes.main()
