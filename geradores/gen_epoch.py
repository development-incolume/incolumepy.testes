import time



def gen_epoch():
    '''
    é necessário loop para executar o proximo
    :return:
    '''
    while True:
        yield time.time()

def g_epoch(digit='default'):
    '''
    retorna o epoch numerico, com a precisão pré-definida
    :param digit:
    :return:
    '''
    digits={'0':0, 0:0, 'int':0, '1': 1, 1:1, 2:2, '2':2, 'float':None, 'default':0 }
    value = None
    while True:
        if digit in digits:
            value = digits[digit]

        if value == 0:
            yield int(time.time())
        elif value == 1:
            yield float('%.01f'% time.time())
        elif value == 2:
            yield float('%.02f'% time.time())
        else:
            yield time.time()


if __name__ == '__main__':
    print(time.time(), time.ctime())

    epoch = gen_epoch()

    print(epoch.__next__())
    print(epoch.__next__())
    print(epoch.__next__())
    print(epoch.__next__())
    print()
    for i in range(3):
        print(epoch.__next__())

    print()
    a = g_epoch()
    print(type(a.__next__()))
    print('a', a.__next__())

    print()
    b = g_epoch(digit='1')
    print(type(b.__next__()))
    print('b', b.__next__())

    print()
    c = g_epoch(digit='2')
    print(type(c.__next__()))
    print('c', c.__next__())

    print()
    d = g_epoch(digit='3')
    print(type(d.__next__()))
    print('d', d.__next__())

    print()
    e = g_epoch(digit='int')
    print(type(e.__next__()))
    print('e', e.__next__())

    print()
    f= g_epoch(digit='float')
    print(type(f.__next__()))
    print('f', f.__next__())