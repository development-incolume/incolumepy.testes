import random

c = "abcdefghijklmnopqrstuvwxyz"
c1 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
c2 = '1234567890'
c3 = '!@#$%&*()_-§.'
char = c + c1 + c2 + c3

def password(num_caract=8, q_passwds=1):
    try:
        try:
            num_caract = int(num_caract)
        except:
            raise ValueError('Erro: (num_caract) quantidade de caracteres da senha, deve ser numerico!')
        if (num_caract < 0):
            raise ValueError("Erro: (num_caract) número negativo, deve ser um inteiro positivo!")
        elif (0 <= num_caract < 8):
            raise ValueError("Erro: (num_caract) Tem que ter pelo menos 8 caracteres, deve ser um inteiro positivo!.")
        else:
            passwds = []
            while len(passwds) < q_passwds:
                passwd = ""
                while len(passwd) != num_caract:
                    passwd = passwd + random.choice(char)
                passwds.append(passwd)

        return "Password(s): %s" % ', '.join(passwds)
    except (TypeError) as e:
        print('Erro: (num_caract) quantidade de caracteres da senha, deve ser um inteiro positivo!')
        return False
    except (ValueError) as e:
        print(e)
        return False
    except:
        raise


if __name__ == '__main__':
    print(password(10))
    print(password(10,7))
