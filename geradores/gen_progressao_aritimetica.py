class ProgressaoAritmetica():
    pa = []
    def __init__(self, *, numInicial, razao):
        self.pa.append(numInicial)
        self.razao = razao

    def __next__(self):
        self.pa.append(self.pa[-1] + self.razao)
        return self.pa.pop(0)

    def __iter__(self):
        return self


if __name__ == '__main__':
    pa = ProgressaoAritmetica(numInicial=1, razao=10)
    print(next(pa))

    for i in range(10):
        print(next(pa))

    print([next(pa) for x in range(10)])

    pa2 = ProgressaoAritmetica(numInicial=0, razao=3)
    print(next(pa2))

    for i in range(100):
        print(next(pa2))
