def minha_funcao():
    """Não faz nada, mas é documentada.
    Realmente ela não faz nada.
    """
    pass


print(minha_funcao.__doc__)
# Não faz nada, mas é documentada.
# Realmente ela não faz nada.
