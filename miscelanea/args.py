import sys

def all_args():
    print (sys.argv)

def var_args():
    nome_do_ficheiro = sys.argv[0]
    argumentos_passados = sys.argv[1:]
    
    if len(sys.argv) > 1:
        print ("O nome do ficheiro e: " + nome_do_ficheiro)
        print ("E os argumentos passados sao: " + str(argumentos_passados))


def var_fargs(farg=1, *args):
    print ("formal arg:", farg)
    for arg in args:
        print ("another arg:", arg)

def var_kwargs(farg=1, **kwargs):
    print ("formal arg:", farg)
    for key in kwargs:
        print ("another keyword arg: %s: %s" % (key, kwargs[key]))

def var_args_call(arg1, arg2, arg3, arg4=0):
    print ("arg1:", arg1)
    print ("arg2:", arg2)
    print ("arg3:", arg3)
    print ("arg4:", arg4)

def print_everything(*args):
    for count, thing in enumerate(args):
        print ('{0}. {1}'.format(count, thing))

def table_things(**kwargs):
    for name, value in kwargs.items():
        print ('{0} = {1}'.format(name, value))

if __name__ == "__main__":
    try:
        args = ("two", 3)
        kwargs = {"arg3": 3, "arg2": "two", 'arg4': 'quatro'}
        all_args()
        print( "-"*10)
        var_args()
        print("-" * 10)
        var_fargs(sys.argv[1], "two", 3)
        print("-" * 10)
        var_kwargs(farg=sys.argv[1], myarg2="two", myarg3=3)
        print("-" * 10)
        var_args_call(sys.argv[1], *args)
        print("-" * 10)
        var_args_call(sys.argv[1], **kwargs)
        print("-" * 10)
        print_everything('apple', 'banana', 'cabbage')
        print("-" * 10)
        print_everything(sys.argv)
        print("-" * 10)
        table_things(apple = 'fruit', cabbage = 'vegetable')
        print("-" * 10)
        table_things(**kwargs)
    except IndexError:
        print ('este programa necessita de argumentos para ser executado.')



