# Manipular o path e arquivos no sistemas de arquivos
import os
import sys


def get_filename():
    fargv = sys.argv[0]
    fdundle = __file__
    print(fargv == fdundle)
    print(fdundle)
    print(os.path.dirname(fdundle))
    print(os.path.basename(fdundle))
    print(os.path.split(fdundle))


get_filename()
# True
# /home/brito/projetos/00-incolumepy/incolumepy/testes/miscelanea/truncus0.py
# /home/brito/projetos/00-incolumepy/incolumepy/testes/miscelanea
# truncus0.py
# ('/home/brito/projetos/00-incolumepy/incolumepy/testes/miscelanea', 'truncus0.py')
