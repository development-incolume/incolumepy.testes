#! /usr/bin/env Python
#-*- coding: utf-8 -*-

import sys
def file_open01(file_name="myfile.txt"):
    try:
        with open(file_name) as f:
            print (f.readline())
        print('arquivo fechado')
    except :
        print (sys.exc_info())


def file_open02(file_name="myfile.txt"):

    with open(file_name) as f:
        for line in f:
            print (line,)
    print('arquivo fechado')



if __name__ == '__main__':
    file_open01('readline_file.txt')
    file_open02('readline_file.txt')
