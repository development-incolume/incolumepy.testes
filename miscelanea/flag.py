#!/usr/bin/env Python
# --*-- Coding: utf-8 --*--

import sys
import platform

if platform.python_version() >= '2.7':
    raise EnvironmentError('incompative Python version')

def commandline():
    '''
    There is a commandline
    execute:
        python incolumepy/testes/flag.py -n a b c 10 -f arq.log
    '''
    options = ['-h','--help','-f','--file','-n','--num','--number','-v','--verbose']
    flag={}
    flag['help'] = False
    flag['num'] = False
    flag['file'] = False
    flag['verbose'] = False
    if len(sys.argv) > 1:
        mylist = []
        myfile = ''
        # sys.argv[0] is the program filename, slice it off
        for element in sys.argv[1:]:
            if flag['num']:
                if not '-' in element:
                    mylist.append(element)
                if not mylist:
                    raise IndexError('numbers not informed')
            if flag['file']:
                if not '-' in element:
                    myfile=element
                if myfile=='':
                    raise NameError('file name not informed')
            if element in options[0:1]:
                flag['verbose'] = True 
            if element in options[2:3]:
                flag['num'] = False
                flag['file'] = True
            if element in options[3:7]:
                flag['num'] = True
                flag['file'] = False
            if element in options[7:9]:
                flag['num'] = False
                flag['file'] = False
                flag['verbose'] = True
    else:
        print ("usage %s %s" % (sys.argv[0], str(options)))
        sys.exit(1)
    # if the arguments were -n 1 2 3 4 -f abc.txt
    # mylist should be  ['1', '2', '3', '4']
    if flag['verbose']:
        return '-h|--help: help\n-f|--file: nome do arquivo\n-n|--num|--number: numeros\n-v|--verbose: verboso'
    return (mylist, myfile,flag['verbose'])

if __name__ == '__main__':
    try:
        print (commandline())
    except:
        sys.exc_info()
