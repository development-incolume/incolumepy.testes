#-*- coding: utf-8 -*-
class adder:
    def __init__(self, value=0):
        self.data = value

    def __add__(self, other):
        self.data += other

class addrepr(adder):
    def __repr__(self):
        return 'addrepr(%s)' % self.data

class addstr(adder):
    def __str__(self):
        return '[Value: %s]' % self.data

class addboth(adder):
    def __str__(self):
        return '[Value: %s]' % self.data # string amigavel para usuário

    def __repr__(self):
        return '========== addboth(%s)' % self.data # string como código

if __name__ == '__main__':
    x = addrepr(2) # executa __init__
    print (x + 1) #executa o __add__
    print (x) #executa o __repr__
    print (str(x), repr(x))


    y = addstr(3)
    print (y)
    print (str(y), repr(x))

    w = addboth(4)
    print (w)
    print (str(w), repr(w))
