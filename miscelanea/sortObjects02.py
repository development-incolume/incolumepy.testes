import operator
#import platform
from platform import python_version
from sys import exc_info

if python_version() >= '3.0' :
    xrange = range

class ThisObj(object):
    def __init__(self, cod, nome, ano):
        self.cod = cod
        self.nome = nome
        self.ano = ano


    def __repr__(self):
        return '(' + (', '.join(map(str,(self.cod,self.ano,self.nome)))) + ')'


if __name__ == "__main__":
    try:
        xarge = range
    except:
        print(exc_info())

    lista = []
    for i in xrange(10,0,-1):
        if i % 2: y=2000+i 
        else: y=2010+i
        lista.append(ThisObj(cod=i, nome='a'+str(i), ano=y))

    print ('normal', lista)

    lista.sort(key=operator.attrgetter('nome'))
    print ('nome', lista)

    lista.sort(key=operator.attrgetter('cod'))
    print ('Cod', lista)

    lista.sort(key=operator.attrgetter('ano'))
    print ('Ano', lista)

