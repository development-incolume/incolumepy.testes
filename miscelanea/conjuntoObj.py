# -*- coding: utf-8 -*-
'''
https://aprendendo-computacao-com-python.readthedocs.org/en/latest/capitulo_15.html
'''
from platform import python_version

if python_version() >= '3.0': xrange = range


class Carta(object):
    listaDeNaipes = ["Paus", "Ouros", "Copas", "Espadas"]
    listaDePosicoes = ["narf", "Ás", "2", "3", "4", "5", "6", "7",
                     "8", "9", "10", "Valete", "Rainha", "Rei"]
    '''
    Espadas -> 3
    Copas -> 2
    Ouros -> 1
    Paus -> 0

    Valete -> 11
    Rainha -> 12
    Rei -> 13
    '''

    def __init__(self, naipe=0, posicao=0):
        self.naipe = naipe
        self.posicao = posicao

    def __str__(self):
        return (self.listaDePosicoes[self.posicao] + " de " +
            self.listaDeNaipes[self.naipe])

    def __cmp__(self, other):
        # verificar os naipes
        if self.naipe > other.naipe: return 1
        if self.naipe < other.naipe: return -1
        # as cartas têm o mesmo naipe... verificar as posições
        if self.posicao > other.posicao: return 1
        if self.posicao < other.posicao: return -1
        # as posições são iguais... é um empate
        return 0

class Baralho:
    def __init__(self):
        self.cartas = []
        for naipe in xrange(4):
            for posicao in xrange(1,14):
                self.cartas.append(Carta(naipe,posicao))

    def imprimirBaralho(self):
        for carta in self.cartas:
            print (carta)

    def __str__(self):
        s = ""
        for i in xrange(len(self.cartas)):
            s += str(self.cartas[i]) + "\n"
        return s


    def embaralhar(self):
        import random
        nCartas = len(self.cartas)
        for i in xrange(nCartas):
            j = random.randrange(i, nCartas)
            self.cartas[i], self.cartas[j] = self.cartas[j], self.cartas[i]

    def removerCarta(self, carta):
        if carta in self.cartas:
          self.cartas.remove(carta)
          return 1
        else:
          return 0

    def distribuirCarta(self):
        return self.cartas.pop()

    def estahVazio(self):
        return (len(self.cartas) == 0)


class Mao(Baralho):
    def __init__(self, nome=''):
        self.cartas = []
        self.nome = nome
    def adicionarCarta(self,carta):
        self.cartas.append(carta)


if __name__ == '__main__':
    print(python_version())
    print('2.7.5' > '1.2')
    carta1 = Carta(1,11)
    print (carta1)
    print (carta1.listaDeNaipes[1])
    print (Carta(2,12))
    print ('*' * 5)
    print (Baralho())
    b = Baralho()
    b.imprimirBaralho()
    print ('*' * 5)
    b.embaralhar()
    b.imprimirBaralho()
    print ('*' * 5)
