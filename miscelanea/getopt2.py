def calc(x, y, op):
    try:
        x, y = int(x), int(y)
        return {
            '+': x + y,
            '-': x - y,
            '*': x * y,
            '/': x // y,
            '%': x % y,
        }.get(op, 'Error')
    except:
        raise


print(calc(1, 2, '+'))
