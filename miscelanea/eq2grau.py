def eq2grau(*, a: float, b: float, c: float):
    if not a:
        return 'Não é equação 2º grau.'
    d = (b ** 2 - 4 * a * c)
    x1 = (-b + d) / 2 * a
    x2 = (-b - d) / 2 * a
    print((x1, x2))
    return (x1, x2)


print(eq2grau(a=1, b=2, c=5))

eq2grau(a=1, b=-5, c=6)
eq2grau(a=1, b=1, c=1)
eq2grau(a=2, b=3, c=3)
print(eq2grau(a=0, b=3, c=3))
eq2grau(a=3, b=0, c=-3)
