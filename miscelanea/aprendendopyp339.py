# -*- coding:utf-8 -*-

class super:
    def hello(self):
        self.data1 = 'python'

class sub(super):
    def hola(self):
        self.data2 = 'eggs'



if __name__ == '__main__':
    a = sub()
    print (a.__dict__)
    print (a.__class__)
    print('-' * 80)
    print (sub.__bases__)
    print (super.__bases__)
    print ('-'* 80)
    y = sub()
    print (a.hello())
    print (a.__dict__)
    a.hola()
    print (a.__dict__)
    print (sub.__dict__)
    print (super.__dict__)
    print (sub.__dict__.keys(), super.__dict__.keys())
    print (y.__dict__)

