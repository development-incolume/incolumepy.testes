def greet(lang):
    if lang == 'pt':
        return 'Olá'
    elif lang == 'es':
        return 'Hola'
    elif lang == 'fr':
        return 'Bonjour'
    elif lang == 'it':
        return 'Ciao'
    elif lang == 'lt':
        return 'Salve'
    else:
        return 'Hello'


if __name__ == '__main__':

    print(greet(''), 'Ana')
    print(greet('es'), 'Ana')
    print(greet('pt'), 'Ana')
    print(greet('fr'), 'Michael')
    print(greet('it'), 'Ana')
    print(greet('lt'), 'Ana')