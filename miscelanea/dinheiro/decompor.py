from collections import defaultdict


def f228645a(N):
    N = int(N)
    notas = [100.00, 50.00, 20.00, 10.00, 5.00, 2.00, 1.00]
    print("R$ %d:" % (N))
    for x in notas:
        print("%i nota(s) de R$ %.2f" % ((N / x), x))
        N %= (x)

def f228645(N):
    N = int(N)
    notas = [100,50,20,10,5,2,1]
    print ("R$ %d:"%(N))
    for x in notas:
        print("%i nota(s) de R$ %d,00"%((N/x),x))
        N %= (x)


def decompor(valor):
    notas = [100, 50, 20, 10, 5, 2, 1]
    valor = int(valor)
    result = {}
    for nota in notas:
        if nota not in result:
            result[nota] = 0
        result[nota] += valor // nota
        print("{:1.0f} nota(s) de R$ {:2.2f}".format(result[nota], nota))
        valor %= nota
    return result

f228645(576)
print()
d = decompor(188.00)
print(d)
