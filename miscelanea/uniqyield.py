'''
http://stackoverflow.com/questions/12897374/get-unique-values-from-a-list-in-python
'''

from itertools import filterfalse

def unique_everseen(iterable, key=None):
    '''
    "List unique elements, preserving order. Remember all elements ever seen."
    # unique_everseen('AAAABBBCCDAABBB') --> A B C D
    # unique_everseen('ABBCcAD', str.lower) --> A B C D
    '''
    seen = set()
    seen_add = seen.add
    if key is None:
        for element in filterfalse(seen.__contains__, iterable):
            seen_add(element)
            yield element
    else:
        for element in iterable:
            k = key(element)
            if k not in seen:
                seen_add(k)
                yield element


if __name__ == '__main__':
    a = unique_everseen('AAAABBBCCDAABBB')
    b = unique_everseen('ABBCcAD', str.lower)

    for i in a:
        print(a.__next__())
