#!/usr/bin/env python

import ipaddr
import sys

try:
    if sys.argv:
        ip = ipaddr.ipaddr.IPAddress(sys.argv[1])
    else:
        print ('%s is a correct IP%s address.' % (ip, ip.version))
except ValueError:
    print ('address/netmask is invalid: %s' % sys.argv[1])
except:
    print ('Usage : %s  ip' % sys.argv[0])
