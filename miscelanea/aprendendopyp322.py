#-*- coding: utf-8 -*-
from builtins import print
from sys import exc_info


class Empty:
    def __getattr__(self, attrname):
        if attrname == 'age':
            return 40
        else:
            raise AttributeError("Atributo '%s' inexistente" % attrname)

class Access_control:
    def __setattr__(self, attr, value):
        if attr == 'age':
            self.__dict__[attr] = value
        else:
            raise AttributeError ('{} não permitido'.format(attr))

class New_access_control:
    def __init__(self, id):
        self.id = id

    def __setattr__(self, *args):
        if args[0] == 'idade':
            self.__dict__['idade'] = args[1]
        elif args[0] == 'id':
            self.__dict__['id'] = args[1]
        else:
            raise AttributeError ('{} não permitido'.format(args[0]))

class access_control003:
    def __init__(self):
        pass

    def __setattr__(self, key, value):
        if key == 'id':
            self.__dict__['id'] = value
        else:
            raise AttributeError('Atributo "{}" não permitido'.format(key))

    def __getattr__(self, item):
        if item in self.__dict__:
            return self.__dict__[item]
        else:
            raise AttributeError('Atribuito "%s" inexistente"' % item)

class access_control004:
    __dict__ = {'id':'', 'idade':'', 'nome':''}

    def __init__(self, id):
        self.id = id

    def __setattr__(self, key, value):
        if key in self.__dict__:
            self.__dict__[key] = value
        else:
            raise AttributeError('Atributo "{}" não permitido'.format(key))

    def __getattr__(self, item):
        if item in self.__dict__:
            return self.__dict__[item]
        else:
            raise AttributeError('Atribuito "%s" inexistente"' % item)

if __name__ == '__main__':
    DEBUB=0
    if DEBUB:
        try:
            X = Empty()
            print (X.age)
            print (X.name)
        except AttributeError as e:
            print (e)

        try:
            Y = Access_control()
            Y.age = 40
            print (Y.age)
            Y.name = 'mel'
        except AttributeError as e:
            print (e)

        a = New_access_control(id='a')
        try:
            a.age = '20'
            print(a.age)
            a.nome = 'teste'
        except AttributeError as e:
            print(e)

        try:
            a.idade = 38
            print(a.idade)
        except AttributeError as e:
            print(e)

        try:
            a.altura = 1.78
        except AttributeError as e:
            print(e)
        try:
            a.cep = '72316-042'
        except AttributeError as e:
            print(e)

        b = New_access_control('b')
        print(a.__dict__, b.__dict__)

    c = access_control003()
    c.id = 'c'

    print(c.__dict__)
    try:
        c.idade = 5
    except:
        print(exc_info())
    try:
        print(c.idade)
    except:
        print(exc_info())


    d = access_control004('d')
    d.idade = 5
    d.id = 'D'
    d.nome = 'classe D'
    print(d.__dict__)
    try:
        print(d.teste)
    except:
        print(exc_info())
